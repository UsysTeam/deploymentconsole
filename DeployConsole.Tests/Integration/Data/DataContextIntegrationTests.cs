﻿using System;
using System.Linq;
using System.Transactions;
using DeployConsole.Config;
using DeployConsole.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DeployConsole.Tests.Integration.Data
{
    /// <summary>
    /// Class for testing the <see cref="DeployConsole.Data.DataContext"/> class.
    /// </summary>
    [TestClass]
    public class DataContextIntegrationTests
    {
        /// <summary>
        /// Tests the configuration properties.
        /// </summary>
        [TestMethod]
        public void TestConfigProperties()
        {
            var context = new DataContext();
            using (new TransactionScope())
            {
                var prop1 = context.ConfigProperties.Add(context.ConfigProperties.Create());
                prop1.Environment = Environments.DEV;
                prop1.Id = Guid.NewGuid();
                prop1.ProjectType = ProjectTypes.MVC;
                prop1.Warehouse = Warehouses.PT01;
                prop1.PropertyName = "Property 1";
                prop1.PropertyValue = "Value 1";

                var prop2 = context.ConfigProperties.Add(context.ConfigProperties.Create());
                prop2.Environment = Environments.DEV;
                prop2.Id = Guid.NewGuid();
                prop2.ProjectType = ProjectTypes.WCFGeneral;
                prop2.Warehouse = Warehouses.PT01;
                prop2.PropertyName = "Property 2";
                prop2.PropertyValue = "Value 2";

                context.SaveChanges();

                var properties = context.ConfigProperties.ToList();

                Assert.AreEqual(2, properties.Count);
                
                var projectTypes = Enum.GetValues(typeof(ProjectTypes)).Cast<ProjectTypes>().Where(e => (ProjectTypes.WCFGeneral | ProjectTypes.MVC).HasFlag(e)).ToList();

                Assert.AreEqual(2, context.ConfigProperties.Count(c => projectTypes.Contains(c.ProjectType)));

                projectTypes = Enum.GetValues(typeof(ProjectTypes)).Cast<ProjectTypes>().Where(e => (ProjectTypes.WCFWarehouse | ProjectTypes.MVC).HasFlag(e)).ToList();

                Assert.AreEqual(1, context.ConfigProperties.Count(c => projectTypes.Contains(c.ProjectType)));
            }
        }
    }
}
