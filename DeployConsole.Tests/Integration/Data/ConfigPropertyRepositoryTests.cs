﻿using System;
using System.Linq;
using System.Transactions;
using DeployConsole.Config;
using DeployConsole.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DeployConsole.Tests.Integration.Data
{
    /// <summary>
    /// Class for testing the <see cref="DeployConsole.Data.ConfigPropertyRepository"/> class.
    /// </summary>
    [TestClass]
    public class ConfigPropertyRepositoryTests
    {
        /// <summary>
        /// Tests the get configuration properties method.
        /// </summary>
        [TestMethod]
        public void TestGetConfigProperties()
        {
            using (new TransactionScope())
            {
                DataContext context;

                //Test preparation
                using (context = new DataContext())
                {
                    var prop1 = context.ConfigProperties.Add(context.ConfigProperties.Create());
                    prop1.Environment = Environments.DEV;
                    prop1.Id = Guid.NewGuid();
                    prop1.ProjectType = ProjectTypes.MVC;
                    prop1.Warehouse = Warehouses.PT01;
                    prop1.PropertyName = "Property 1";
                    prop1.PropertyValue = "Value 1";

                    var prop2 = context.ConfigProperties.Add(context.ConfigProperties.Create());
                    prop2.Environment = Environments.DEV;
                    prop2.Id = Guid.NewGuid();
                    prop2.ProjectType = ProjectTypes.WCFGeneral;
                    prop2.Warehouse = Warehouses.PT01;
                    prop2.PropertyName = "Property 2";
                    prop2.PropertyValue = "Value 2";

                    context.SaveChanges();
                }

                using (context = new DataContext())
                {
                    var repository = new ConfigPropertyRepository(context);
                    Assert.AreEqual(2, repository.GetConfigProperties(Environments.DEV, ProjectTypes.WCFGeneral | ProjectTypes.MVC, Warehouses.PT01).Count);
                    Assert.AreEqual(1, repository.GetConfigProperties(Environments.DEV, ProjectTypes.WCFWarehouse | ProjectTypes.MVC, Warehouses.PT01).Count);
                    Assert.AreEqual(0, repository.GetConfigProperties(Environments.ACC, ProjectTypes.WCFWarehouse | ProjectTypes.MVC, Warehouses.PT01).Count);
                }
            }
        }
    }
}