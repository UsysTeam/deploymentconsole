﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DeployConsole.Config;
using DeployConsole.Helpers;
using DeployConsole.Logging;
using DeployConsole.Managers;

namespace DeployConsole
{
    public class Sql
    {
        private static readonly string Today = DateTime.Today.ToString("yyyyMMdd");
        const string DirTosearch = @"\\WS12-USYS69FL1\usys01s-bck$\temp\";
        private static readonly ConcurrentDictionary<string, Tuple<Environments, Warehouses, string>> Dictionary = new ConcurrentDictionary<string, Tuple<Environments, Warehouses, string>>();

        public static void RestoreBackupFromPrdToCpyPrd(Warehouses whs)
        {
            RestoreBackupPrivate(whs, Environments.PRD, Environments.CPYPRD);
        }
        private static void RestoreBackupPrivate(Warehouses whs, Environments envsFrom, Environments envTo)
        {
            try
            {
                if (EnumHelper.MaskToList<Environments>(envTo).Count() > 1)
                    throw new Exception("More then one 'to-env' specified");

                if (envTo.HasFlag(Environments.PRD))
                    throw new Exception("To-env may not be PRD");
            }
            catch (Exception e)
            {
                Logger.Log(new LoggingData
                {
                    Env = envsFrom,
                    Exception = e,
                    Group = "",
                    Message = $"Restorebackup failed: {e}"
                });
                return;
            }

            Parallel.ForEach(EnumHelper.MaskToList<Environments>(envsFrom), currentEnvFrom =>
            {
                var configPropertyManager = new ConfigPropertyManager();
                var keysToGet = new List<string> { "ManagementConnectionString" };
                var configProperties = configPropertyManager.GetSimpleConfigProperties(envTo, keysToGet);

                try
                {
                    if (!Directory.Exists(DirTosearch))
                    {
                        throw new Exception($"The directory {DirTosearch} doesn't exists, so can't restore backup");
                    }
                    if (configProperties == null
                        || configProperties.Count != keysToGet.Count
                        || string.IsNullOrWhiteSpace(configProperties.First().PropertyValue))
                    {
                        throw new Exception($"Error getting the ManagementConnectionString for env {envTo}");
                    }

                    Parallel.ForEach(EnumHelper.MaskToList<Warehouses>(whs).Where(wh => wh != Warehouses.None), currentWh =>
                    {
                        var logGroup = Guid.NewGuid().ToString();

                        var filesToCopy = Directory.GetFiles(DirTosearch, $"{currentEnvFrom}*{currentWh}*{Today}_TEMP.bak", SearchOption.TopDirectoryOnly);
                        if (filesToCopy == null || !filesToCopy.Any())
                        {
                            Logger.Log(new LoggingData
                            {
                                Env = currentEnvFrom,
                                Exception = null,
                                Group = logGroup,
                                Wh = currentWh,
                                Message = $"Restoring backup db from env {currentEnvFrom} to env {envTo} and wh {currentWh} failed, cause the file is not found."
                            });
                            return;
                        }

                        Logger.Log(new LoggingData
                        {
                            Env = currentEnvFrom,
                            Exception = null,
                            Group = logGroup,
                            Wh = currentWh,
                            Message = $"Restoring backup db from env {currentEnvFrom} to env {envTo} and wh {currentWh}"
                        });

                        using (var conn = new SqlConnection(configProperties.First().PropertyValue))
                        {
                            try
                            {
                                conn.Open();

                                if (conn.State == ConnectionState.Open)
                                {
                                    using (var cmd = new SqlCommand("CustomBackupRestore", conn) { CommandType = CommandType.StoredProcedure })
                                    {
                                        Dictionary[conn.ConnectionString] = new Tuple<Environments, Warehouses, string>(currentEnvFrom, currentWh, logGroup);
                                        conn.InfoMessage += connection_InfoMessage;
                                        cmd.CommandTimeout = 0;
                                        cmd.Parameters.Add(new SqlParameter("@WH", currentWh.ToString()));
                                        cmd.Parameters.Add(envTo.HasFlag(Environments.CPYPRD)
                                            ? new SqlParameter("@ToEnv", "CPY_PRD")
                                            : new SqlParameter("@ToEnv", envTo.ToString()));
                                        cmd.Parameters.Add(currentEnvFrom.HasFlag(Environments.CPYPRD)
                                            ? new SqlParameter("@FromEnv_DefaultPRD", "CPY_PRD")
                                            : new SqlParameter("@FromEnv_DefaultPRD", currentEnvFrom.ToString()));
                                        
                                        var result = cmd.ExecuteScalar();
                                        var ok = result as int?;

                                        conn.InfoMessage -= connection_InfoMessage;

                                        if (ok.HasValue && ok == 1)
                                        {
                                            Logger.Log(new LoggingData
                                            {
                                                Env = currentEnvFrom,
                                                Exception = null,
                                                Group = logGroup,
                                                Wh = currentWh,
                                                Message = $"Restoring backup db for env {currentEnvFrom} and wh {currentWh} succceeded"
                                            });
                                        }
                                        else
                                        {
                                            throw new Exception($"Restoring backup db for env {currentEnvFrom} and wh {currentWh} failed");
                                        }
                                    }
                                }
                                else
                                {
                                    throw new Exception($"Cannot open the usys management DB for restoring backup from {currentEnvFrom.ToString()}");
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Log(new LoggingData
                                {
                                    Env = currentEnvFrom,
                                    Exception = ex,
                                    Group = logGroup,
                                    Wh = currentWh,
                                    Message = $"Something happend when trying to restore the {currentEnvFrom.ToString()}-backup: {ex.Message} {Environment.NewLine} {ex.StackTrace}"
                                });
                            }
                            finally
                            {
                                conn.Close();
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                    Logger.Log(new LoggingData
                    {
                        Env = envTo,
                        Exception = e,
                        Group = "",
                        Wh = whs,
                        Message = $"Something happend when opening the managent db: {e.Message} {Environment.NewLine} {e.StackTrace}"
                    });
                }
            });
        }

        public static void CreateDbBackup(Environments envs, Warehouses whs)
        {
            Parallel.ForEach(EnumHelper.MaskToList<Environments>(envs), currentEnv =>
            {
                var configPropertyManager = new ConfigPropertyManager();
                var keysToGet = new List<string> {"ManagementConnectionString"};
                var configProperties = configPropertyManager.GetSimpleConfigProperties(currentEnv, keysToGet);

                try
                {
                    if (configProperties == null
                        || configProperties.Count != keysToGet.Count
                        || string.IsNullOrWhiteSpace(configProperties.First().PropertyValue))
                    {
                        throw new Exception($"Error getting the ManagementConnectionString for env {currentEnv}");
                    }

                    Parallel.ForEach(EnumHelper.MaskToList<Warehouses>(whs).Where(wh => wh != Warehouses.None), currentWh =>
                    {
                        var logGroup = Guid.NewGuid().ToString();
                        Logger.Log(new LoggingData
                        {
                            Env = currentEnv,
                            Exception = null,
                            Group = logGroup,
                            Wh = currentWh,
                            Message = $"Taking backup db for env {currentEnv} and wh {currentWh}"
                        });

                        using (var conn = new SqlConnection(configProperties.First().PropertyValue))
                        {
                            try
                            {
                                conn.Open();

                                if (conn.State == ConnectionState.Open)
                                {
                                    using (var cmd = new SqlCommand("CustomBackupCreate", conn) { CommandType = CommandType.StoredProcedure })
                                    {
                                        Dictionary[conn.ConnectionString] = new Tuple<Environments, Warehouses, string>(currentEnv, currentWh, logGroup);
                                        conn.InfoMessage += connection_InfoMessage;

                                        cmd.CommandTimeout = 0;
                                        cmd.Parameters.Add(new SqlParameter("@WHtoBackup", currentWh.ToString()));

                                        if (currentEnv != Environments.PRD)
                                            cmd.Parameters.Add(new SqlParameter("@EnvPrefix", currentEnv.ToString()));

                                        var result = cmd.ExecuteScalar();
                                        var ok = result as int?;

                                        conn.InfoMessage -= connection_InfoMessage;

                                        if (ok.HasValue && ok == 1)
                                        {
                                            Logger.Log(new LoggingData
                                            {
                                                Env = currentEnv,
                                                Exception = null,
                                                Group = logGroup,
                                                Wh = currentWh,
                                                Message = $"Taking backup db for env {currentEnv} and wh {currentWh} succceeded"
                                            });
                                        }
                                        else
                                        {
                                            throw new Exception($"Taking backup db for env {currentEnv} and wh {currentWh} failed");
                                        }
                                    }
                                }
                                else
                                {
                                    throw new Exception($"Cannot open the usys management DB for creating backup from {currentEnv.ToString()}");
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Log(new LoggingData
                                {
                                    Env = currentEnv,
                                    Exception = ex,
                                    Group = logGroup,
                                    Wh = currentWh,
                                    Message = $"Something happend when trying to create the {currentEnv.ToString()}-backup: {ex.Message} {Environment.NewLine} {ex.StackTrace}"
                                });
                            }
                            finally
                            {
                                conn.Close();
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                    Logger.Log(new LoggingData
                    {
                        Env = currentEnv,
                        Exception = e,
                        Group = "",
                        Wh = whs,
                        Message = $"Something happend when opening the managent db: {e.Message} {Environment.NewLine} {e.StackTrace}"
                    });
                }
            });
        }

        public static void CopyDbBackup(Environments envs, Warehouses whs)
        {
            Parallel.ForEach(EnumHelper.MaskToList<Environments>(envs), currentEnv =>
            {
                Parallel.ForEach(EnumHelper.MaskToList<Warehouses>(whs).Where(wh => wh != Warehouses.None), currentWh =>
                {
                    try
                    {
                        var dirForBackup = $@"\\WS12-USYS69FL1\usys01s-bck$\temp\{Today}_{currentEnv}_{currentWh}\";
                        var logGroup = new Guid().ToString();
 
                        if (Directory.Exists(DirTosearch))
                        {
                            var filesToCopy = Directory.GetFiles(DirTosearch, $"{currentEnv}*{currentWh}*{Today}_TEMP.bak", SearchOption.TopDirectoryOnly);
                            if (filesToCopy != null && filesToCopy.Any())
                            {
                                if (!Directory.Exists(dirForBackup))
                                    Directory.CreateDirectory(dirForBackup);
                            }
                            Parallel.ForEach(filesToCopy, fileToCopy =>
                            {
                                try
                                {
                                    var newFile = dirForBackup + Path.GetFileName(fileToCopy);
                                    Logger.Log(new LoggingData
                                    {
                                        Env = currentEnv,
                                        Exception = null,
                                        Group = logGroup,
                                        Wh = currentWh,
                                        Message = $"Copying db backup file from {fileToCopy} to {newFile}"
                                    });
                                    if (File.Exists(newFile))
                                        File.Delete(newFile);
                                    File.Copy(fileToCopy, newFile);
                                }
                                catch (Exception ex)
                                {
                                    Logger.Log(new LoggingData
                                    {
                                        Env = currentEnv,
                                        Exception = ex,
                                        Group = logGroup,
                                        Wh = currentWh,
                                        Message = $"Something happend when copying the db backupfile {fileToCopy}: {ex.Message} {Environment.NewLine} {ex.StackTrace}"
                                    });
                                }
                            });
                        }
                        else
                        {
                            throw new Exception($":{DirTosearch} doesn't exist");
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Log(new LoggingData
                        {
                            Env = currentEnv,
                            Exception = e,
                            Group = "",
                            Wh = currentWh,
                            Message = $"Something happend when copying the db backupfiles: {e.Message} {Environment.NewLine} {e.StackTrace}"
                        });
                    }
                });
            });
        }

        private static void connection_InfoMessage(object sender, SqlInfoMessageEventArgs e)
        {
            var sqlConnection = sender as SqlConnection;
            if (sqlConnection != null)
            {
                Tuple<Environments, Warehouses, string> valueForLogging;
                if (Dictionary.TryGetValue(sqlConnection.ConnectionString, out valueForLogging))
                {
                    Console.WriteLine($"Taking backup for {valueForLogging.Item1} {valueForLogging.Item2}: {e.Message}");
                    Logger.Log(new LoggingData
                    {
                        Env = valueForLogging.Item1,
                        Exception = null,
                        Group = valueForLogging.Item3,
                        Wh = valueForLogging.Item2,
                        Message = e.Message
                    });
                }
            }
        }
    }
}