﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeployConsole.Annotations;
using DeployConsole.Config;

namespace DeployConsole.Logging
{
    static class Logger
    {
        [UsedImplicitly]
        private static List<LoggingData> _logMessages;

        private static readonly object LockObject = new object();
        public static void Log(LoggingData logData)
        {
            if (_logMessages == null) _logMessages = new List<LoggingData>();
            lock(LockObject)
                _logMessages.Add(logData);
            
        }
        public static void Log(Environments env, ProjectTypes project, Warehouses wh, string message, Exception exception, int indent)
        {
            if (_logMessages == null) _logMessages = new List<LoggingData>();
            lock (LockObject) 
                _logMessages.Add(new LoggingData { Env = env, Project = project, Wh = wh, Message = message, Exception = exception, Indent = indent });
        }

        public static StringBuilder Printlog(bool summaryError=false)
        {
            if (_logMessages == null || _logMessages.Count == 0) return new StringBuilder("Nothing to report!");
            var ret = new StringBuilder();
            IOrderedEnumerable<LoggingData> output =null;
            try
            {
                if (summaryError)
                {
                    output = _logMessages.Where(x=>x.Exception!=null).OrderBy(x => x.Env.ToString()).ThenBy(x => x.Project.ToString()).ThenBy(x => x.Wh.ToString()).ThenBy(x => x.OrderNr).ThenBy(x => x.Group);
                    ret.AppendLine("Error summary:\n--------------");
                }
                else
                {
                    output = _logMessages.OrderBy(x => x.Env.ToString()).ThenBy(x => x.Project.ToString()).ThenBy(x => x.Wh.ToString()).ThenBy(x => x.OrderNr).ThenBy(x => x.Group);
                
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                if (_logMessages==null)
                    Console.WriteLine("_logMessages == null");
            }
            var prevEnv = "";
            var prevProj = "";
            var prevWh = "";

            foreach (var loggingData in output)
            {
                if (summaryError)
                    ret.AppendFormat("\nEnv:{1} Project:{2} WH:{3}\tMessage:{4}\n", loggingData.Indent, loggingData.Env,
                            loggingData.Project, loggingData.Wh, loggingData.Message , loggingData.Exception.Message, loggingData.Exception.StackTrace);

                else
                if (loggingData.Exception != null)
                {
                    if (prevWh == loggingData.Wh.ToString() && prevProj == loggingData.Project.ToString() && prevEnv == loggingData.Env.ToString())
                        ret.AppendFormat("\tMessage:{4}\n\tException:{5}\n\tStackTrace:{6}\n", loggingData.Indent, loggingData.Env,
                            loggingData.Project, loggingData.Wh, (String.IsNullOrWhiteSpace(loggingData.ExtraMessage)) ? loggingData.Message : loggingData.Message + "\n\t\t" + loggingData.ExtraMessage.Replace("\n", "\n\t\t"), loggingData.Exception.Message, loggingData.Exception.StackTrace);
                    else if (prevWh == loggingData.Wh.ToString() && prevProj == loggingData.Project.ToString())
                        ret.AppendFormat("\nEnv:{1} Project:{2}\n\tMessage:{4}\n\tException:{5}\n\tStackTrace:{6}\n", loggingData.Indent, loggingData.Env,
                            loggingData.Project, loggingData.Wh, (String.IsNullOrWhiteSpace(loggingData.ExtraMessage)) ? loggingData.Message : loggingData.Message + "\n\t\t" + loggingData.ExtraMessage.Replace("\n", "\n\t\t"), loggingData.Exception.Message, loggingData.Exception.StackTrace);
                    else if (prevWh == loggingData.Wh.ToString())
                        ret.AppendFormat("\nEnv:{1}\n\tException:{5}\n\tStackTrace:{6}\n", loggingData.Indent, loggingData.Env,
                            loggingData.Project, loggingData.Wh, (String.IsNullOrWhiteSpace(loggingData.ExtraMessage)) ? loggingData.Message : loggingData.Message + "\n\t\t" + loggingData.ExtraMessage.Replace("\n", "\n\t\t"), loggingData.Exception.Message, loggingData.Exception.StackTrace);
                    else
                        
                        ret.AppendFormat("\nEnv:{1} Project:{2} WH:{3}\n\tMessage:{4}\n\tException:{5}\n\tStackTrace:{6}\n", loggingData.Indent, loggingData.Env,
                            loggingData.Project, loggingData.Wh, (String.IsNullOrWhiteSpace(loggingData.ExtraMessage)) ? loggingData.Message : loggingData.Message + "\n\t\t" + loggingData.ExtraMessage.Replace("\n", "\n\t\t"), loggingData.Exception.Message, loggingData.Exception.StackTrace);
                }
                else
                {
                    if (prevWh == loggingData.Wh.ToString() && prevProj == loggingData.Project.ToString() && prevEnv == loggingData.Env.ToString())
                        ret.AppendFormat("\tMessage:{4}\n", loggingData.Indent, loggingData.Env, loggingData.Project, loggingData.Wh, (String.IsNullOrWhiteSpace(loggingData.ExtraMessage)) ? loggingData.Message : loggingData.Message + "\n\t\t" + loggingData.ExtraMessage);
                    else if (prevWh == loggingData.Wh.ToString() && prevProj == loggingData.Project.ToString())
                        ret.AppendFormat("\nEnv:{1} Project:{2}\n\tMessage:{4}\n", loggingData.Indent, loggingData.Env, loggingData.Project, loggingData.Wh, (String.IsNullOrWhiteSpace(loggingData.ExtraMessage)) ? loggingData.Message : loggingData.Message + "\n\t\t" + loggingData.ExtraMessage.Replace("\n", "\n\t\t"));
                    else if (prevWh == loggingData.Wh.ToString())
                        ret.AppendFormat("\nEnv:{1}\n\tMessage:{4}\n", loggingData.Indent, loggingData.Env, loggingData.Project, loggingData.Wh, (String.IsNullOrWhiteSpace(loggingData.ExtraMessage)) ? loggingData.Message : loggingData.Message + "\n\t\t" + loggingData.ExtraMessage.Replace("\n", "\n\t\t"));
                    else
                        ret.AppendFormat("\nEnv:{1} Project:{2} WH:{3}\n\tMessage:{4}\n", loggingData.Indent, loggingData.Env, loggingData.Project, loggingData.Wh, (String.IsNullOrWhiteSpace(loggingData.ExtraMessage)) ? loggingData.Message : loggingData.Message + "\n\t\t" + loggingData.ExtraMessage.Replace("\n", "\n\t\t"));
                }
                prevEnv = loggingData.Env.ToString();
                prevProj = loggingData.Project.ToString();
                prevWh = loggingData.Wh.ToString();
            }
            return ret;
        }

        public static bool HasError()
        {
            if (_logMessages != null && _logMessages.Count > 0)
                return _logMessages.Any(loggingData => loggingData.Exception != null);
            return false;
        }

    }
}
