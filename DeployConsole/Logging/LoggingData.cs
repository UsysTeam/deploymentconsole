﻿using System;
using DeployConsole.Config;

namespace DeployConsole.Logging
{
    class LoggingData
    {
        public Environments? Env { get; set; }
        public ProjectTypes? Project { get; set; }
        public Warehouses? Wh { get; set; }
        public string Message { get; set; }
        public Exception Exception { get; set; }
        public int? Indent { get; set; }
        public string Group { get; set; }
        public int? OrderNr { get; set; }
        public string ExtraMessage { get; set; }
    }
}
