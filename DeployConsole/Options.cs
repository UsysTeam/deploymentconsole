﻿using CommandLine;
using CommandLine.Text;

namespace DeployConsole
{
    class Options 
    {

        [HelpVerbOption]
        public string GetUsage(string verb)
        {
            return HelpText.AutoBuild(this, verb);
        }

        [VerbOption("Spinup", HelpText = "Spinup")]
        public SpinUpOptions SpinUpVerb { get; set; }


        [VerbOption("Prep", HelpText = "Prep build")]
        public PrepOptions PrepVerb { get; set; }


        [VerbOption("Deploy", HelpText = "Depoly build")]
        public DeployOptions DepolyVerb { get; set; }

        [VerbOption("StartStop", HelpText = "Stop=0 | Start=1")]
        public StartStopEnvironmentOptions StartStopEnvironmentVerb { get; set; }

        [VerbOption("GetVersion", HelpText = "Get deployed version")]
        public GetVersionOptions GetVersionVerb { get; set; }
    }

    class PrepOptions
    {

        [Option('e', "Environment", HelpText = "Environment to Prep", Required = true)]
        public int Env { get; set; }

        [Option('p', "Projects", HelpText = "Projects to Prep", Required = true)]
        public int Projects { get; set; }

        [Option('w', "Warehouses", HelpText = "Warehouses to Prep", Required = true)]
        public int Warehouses { get; set; }

        [Option('l', "BuildLocation", HelpText = "Location of the build", Required = true)]
        public string InputLocation { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this);
        }
    }


    class DeployOptions
    {
        [Option('e', "Environment", HelpText = "Environment to Deploy", Required = true)]
        public int Env { get; set; }

        [Option('p', "Projects", HelpText = "Projects to Deploy", Required = true)]
        public int Projects { get; set; }

        [Option('w', "Warehouses", HelpText = "Warehouses to Deploy", Required = true)]
        public int Warehouses { get; set; }

        [Option('b', "DbBackup", HelpText = "Take DB Backup", Required = false,DefaultValue=0)]
        public int DbBackup { get; set; }

        public bool TakeDbBackup => DbBackup == 1;

        [Option('s', "Spinup", HelpText = "Spinup pools after deploy", Required = false, DefaultValue = 1)]
        public int Spinup { get; set; }

        public bool DoSpinup => Spinup == 1;

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this);
        }
    }

    class StartStopEnvironmentOptions
    {
        [Option('e', "Environment", HelpText = "Environment to start/stop", Required = true)]
        public int Env { get; set; }

        [Option('p', "Projects", HelpText = "Projects to start/stop", Required = true)]
        public int Projects { get; set; }

        [Option('w', "Warehouses", HelpText = "Warehouses to start/stop", Required = true)]
        public int Warehouses { get; set; }

        [Option('s', "State", HelpText = "Stopped(0) or started(1)", Required = true)]
        public int State { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this);
        }
    }

    class SpinUpOptions
    {
        [Option('e', "Environment", HelpText = "Environment to spinup", Required = true)]
        public int Env { get; set; }

        [Option('p', "Projects", HelpText = "Projects to spinup", Required = true)]
        public int Projects { get; set; }

        [Option('w', "Warehouses", HelpText = "Warehouses to spinup", Required = true)]
        public int Warehouses { get; set; }
        

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this);
        }
    }

    class GetVersionOptions
    {
        [Option('e', "Environment", HelpText = "Environment to get version from", Required = true)]
        public int Env { get; set; }

        [Option('p', "Projects", HelpText = "Projects to get version from", Required = true)]
        public int Projects { get; set; }

        [Option('w', "Warehouses", HelpText = "Warehouses to get version from", Required = true)]
        public int Warehouses { get; set; }


        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this);
        }
    }
}
