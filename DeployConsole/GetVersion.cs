﻿using DeployConsole.Config;
using DeployConsole.Helpers;
using DeployConsole.Managers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeployConsole
{
    public class GetVersion
    {
        public static void GetMVCVersions(Environments env, ProjectTypes projs, Warehouses whs)
        {
            Parallel.ForEach(EnumHelper.MaskToList<ProjectTypes>(projs), currentProj =>
            {
                var servers = new ConfigPropertyManager().GetServers(env, currentProj, whs);

                if (servers == null || !servers.Any())
                    throw new Exception($"Cannot find servers for Env: {env}  Project: {currentProj}");

                foreach (var server in servers)
                {
                    FileVersionInfo fvi = null;
                    string location = null;
                    switch (currentProj)
                    {
                        case ProjectTypes.CommandApi:
                            location = $@"\\{server}\c$\Usys\{env.ToString()}\WebServices\Command.Api\v{fvi.FileVersion}";
                            fvi = FileVersionInfo.GetVersionInfo(Path.Combine(location, "bin", "Usys.Server.CommandApi.dll"));
                            break;
                        case ProjectTypes.PartyApi:
                            location = $@"\\{server}\c$\Usys\{env.ToString()}\WebServices\Usys.Server.PartyWebApi\v{fvi.FileVersion}";
                            fvi = FileVersionInfo.GetVersionInfo(Path.Combine(location, "bin", "Usys.Server.PartyWebApi.dll"));
                            break;
                        case ProjectTypes.BFApi:
                            location = $@"\\{server}\c$\Usys\{env.ToString()}\WebServices\Usys.Server.BenFreshWebApi\v{fvi.FileVersion}";
                            fvi = FileVersionInfo.GetVersionInfo(Path.Combine(location, "bin", "Usys.Server.BenFreshWebApi.dll"));
                            break;
                        case ProjectTypes.BFJobWebservice:
                            // BFJobWebservice 5.4.5
                            //location = $@"\\{server}\c$\Usys\{env.ToString()}\WebServices\Usys.JobExecutor.Webservice\v{fvi.FileVersion}";
                            //fvi = FileVersionInfo.GetVersionInfo(Path.Combine(location, "bin", "Usys.JobExecutor.Webservice.dll"));
                            //
                            // BFJobWebservice 5.5+
                            location = $@"\\{server}\c$\Usys\{env.ToString()}\WebServices\Usys.JobExecutor.Webservice\Usys.JobExecutor.WebApi\v{fvi.FileVersion}";
                            fvi = FileVersionInfo.GetVersionInfo(Path.Combine(location, "bin", "Usys.JobExecutor.WebApi.dll"));
                            break;
                        case ProjectTypes.InventoryApi:
                            location = $@"\\{server}\c$\Usys\{env.ToString()}\WebServices\Usys.Server.UnivegFilesWebApi\v{fvi.FileVersion}";
                            fvi = FileVersionInfo.GetVersionInfo(Path.Combine(location, "bin", "Usys.Server.UnivegFilesWebApi.dll"));
                            break;
                        case ProjectTypes.MVC:
                            location = $@"\\{server}\c$\Usys\{env.ToString()}\Usys2.Webshell\v{fvi.FileVersion}";
                            fvi = FileVersionInfo.GetVersionInfo(Path.Combine(location, "bin", "Usys2.Web.Shell.dll"));
                            break;
                        default:
                            break;
                    }
                }
            });
        }

        //public static void GetWcfWhVersions()
        //{
        //    Parallel.ForEach(EnumHelper.MaskToList<ProjectTypes>(projs), currentProj =>
        //    {
        //        var servers = new ConfigPropertyManager().GetServers(env, currentProj, whs);

        //        if (servers == null || !servers.Any())
        //            throw new Exception($"Cannot find servers for Env: {env}  Project: {currentProj}");

        //        foreach (var server in servers)
        //        {

        //        }
        //    });
        //}
    }
}
