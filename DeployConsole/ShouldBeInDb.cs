using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DeployConsole.Config;
using DeployConsole.Helpers;

namespace DeployConsole
{
    public class ShouldBeInDb
    {
        public static List<string> GetSiteNames(Environments env, ProjectTypes proj, Warehouses whs)
        {
            var ret = new List<string>();
            switch (env)
            {
                case Environments.PRD:
                    switch (proj)
                    {
                        case ProjectTypes.MVC:
                            Parallel.ForEach(EnumHelper.MaskToList<Warehouses>(whs), currentWh =>
                            { if (currentWh != Warehouses.None) ret.Add($"{currentWh}-logistics.univeg.com"); });
                            break;
                        case ProjectTypes.WCFWarehouse:
                            Parallel.ForEach(EnumHelper.MaskToList<Warehouses>(whs), currentWh =>
                            {
                                if (currentWh != Warehouses.None)
                                {
                                    //ret.Add(string.Format("Usys2.WarehouseServices.PRD.TRUNK.dev.{0}", currentWh));
                                    ret.Add($"Usys2.WarehouseServices.PRD.{currentWh}");
                                }
                            });
                            break;
                        case ProjectTypes.WCFGeneral:
                            //ret.Add("Usys2.GeneralServices.PRD.trunk.dev");
                            ret.Add("Usys2.GeneralServices.PRD");
                            break;
                        default:
                            throw new Exception("'GetSiteNames' for not found 1");
                    }
                    break;
                case Environments.DEV:
                case Environments.ACC:
                case Environments.STA:
                case Environments.CPYPRD:
                case Environments.TST:
                    switch (proj)
                    {
                        case ProjectTypes.MVC:
                            Parallel.ForEach(EnumHelper.MaskToList<Warehouses>(whs), currentWh =>
                            { if (currentWh != Warehouses.None) ret.Add(
                                $"{currentWh}-{env.ToString()}.univegsolutions.com"); });
                            break;
                        case ProjectTypes.PartyApi:
                            Parallel.ForEach(EnumHelper.MaskToList<Warehouses>(whs), currentWh =>
                            { if (currentWh != Warehouses.None) ret.Add(
                                $"{currentWh}-{env.ToString()}-api.univegsolutions.com"); });
                            break;
                        case ProjectTypes.WCFWarehouse:
                            Parallel.ForEach(EnumHelper.MaskToList<Warehouses>(whs), currentWh =>
                            { if (currentWh != Warehouses.None) ret.Add(string.Format("Usys2.WarehouseServices.{1}.{0}", currentWh, env.ToString())); });
                            break;
                        case ProjectTypes.WCFGeneral:
                            ret.Add($"Usys2.GeneralServices.{env}");
                            break;
                        default:
                            throw new Exception("'GetSiteNames' for not found 1");
                    }
                    break;

                default:
                    throw new Exception("'GetSiteNames' for not found 0");

            }
            return ret;
        }

        public static Deploy.ServiceCredentials GetCredentials(Environments env, ProjectTypes proj, Warehouses whs)
        {
            Deploy.ServiceCredentials ret;
            switch (env)
            {
                case Environments.PRD:
                    //switch (proj)
                    //{
                    //    default:
                    //        throw new Exception("'GetCredentials' for not found PRD");
                    //}
                    switch (proj)
                    {
                        case ProjectTypes.DocGenService:
                            ret = new Deploy.ServiceCredentials
                            {
                                ServiceName = "USys2 DocGenService - " + env + " - " + whs,
                                UserName = "usysdocg-srv@prdlog",
                                Password = "Univeg01",
                                ServiceExeName = "USys2.DocumentGeneration.Service.exe"
                            };
                            break;
                        case ProjectTypes.PrintService:
                            ret = new Deploy.ServiceCredentials
                            {
                                ServiceName = "USys2 PrintingService  - " + env + " - " + whs,
                                UserName = @".\usysprint",
                                Password = "Print$erver9",
                                ServiceExeName = "USys2.PrintingService.exe"
                            };
                            break;
                        case ProjectTypes.ScannerManagementService:
                            ret = new Deploy.ServiceCredentials
                            {
                                ServiceName = "USys2 ScannerManagement  - " + env + " - " + whs,
                                UserName = null,
                                Password = null,
                                ServiceExeName = "USys2.ScannerConsoleManagement.Service.exe"
                            };
                            break;
                        case ProjectTypes.uProcessMonitor:
                            ret = new Deploy.ServiceCredentials
                            {
                                ServiceName = "USys2 uProcessMonitor  - " + env,
                                UserName = null,
                                Password = null,
                                ServiceExeName = "Usys2.uProcessMonitor.exe"
                            };
                            break;
                        default:
                            throw new Exception(
                                $"'GetCredentials' for not found Env: {env} Project: {proj}, Warehouse: {whs}");
                    }
                    break;
                case Environments.DEV:
                    switch (proj)
                    {
                        case ProjectTypes.DocGenService:
                            ret = new Deploy.ServiceCredentials
                            {
                                ServiceName = "USys2 DocGenService - " + env + " - " + whs,
                                UserName = "Usysdocg-srv-DEV@prdlog",
                                Password = "DocGen09@D",
                                ServiceExeName = "USys2.DocumentGeneration.Service.exe"
                            };
                            break;
                        case ProjectTypes.PrintService:
                            ret = new Deploy.ServiceCredentials
                            {
                                ServiceName = "USys2 PrintingService  - " + env + " - " + whs,
                                UserName = "usys2ptr-srv-DEV@prdlog",
                                Password = "Printing09@D",
                                ServiceExeName = "USys2.PrintingService.exe"
                            };
                            break;
                        case ProjectTypes.ScannerManagementService:
                            ret = new Deploy.ServiceCredentials
                            {
                                ServiceName = "USys2 ScannerManagement  - " + env + " - " + whs,
                                UserName = null,
                                Password = null,
                                ServiceExeName = "USys2.ScannerConsoleManagement.Service.exe"
                            };
                            break;
                        case ProjectTypes.uProcessMonitor:
                            ret = new Deploy.ServiceCredentials
                            {
                                ServiceName = "USys2 uProcessMonitor  - " + env,
                                UserName = null,
                                Password = null,
                                ServiceExeName = "Usys2.uProcessMonitor.exe"
                            };
                            break;
                        default:
                            throw new Exception(
                                $"'GetCredentials' for not found Env: {env} Project: {proj}, Warehouse: {whs}");
                    }
                    break;
                case Environments.ACC:
                    switch (proj)
                    {
                        case ProjectTypes.DocGenService:
                            ret = new Deploy.ServiceCredentials
                            {
                                ServiceName = "USys2 DocGenService - " + env + " - " + whs,
                                UserName = "Usysdocg-srv-ACC@prdlog",
                                Password = "DocGen05@A",
                                ServiceExeName = "USys2.DocumentGeneration.Service.exe"
                            };
                            break;
                        case ProjectTypes.PrintService:
                            ret = new Deploy.ServiceCredentials
                            {
                                ServiceName = "USys2 PrintingService  - " + env + " - " + whs,
                                UserName = "usys2ptr-srv-ACC@prdlog",
                                Password = "Printing05@A",
                                ServiceExeName = "USys2.PrintingService.exe"
                            };
                            break;
                        case ProjectTypes.ScannerManagementService:
                            ret = new Deploy.ServiceCredentials
                            {
                                ServiceName = "USys2 ScannerManagement  - " + env + " - " + whs,
                                UserName = null,
                                Password = null,
                                ServiceExeName = "USys2.ScannerConsoleManagement.Service.exe"
                            };
                            break;
                        case ProjectTypes.uProcessMonitor:
                            ret = new Deploy.ServiceCredentials
                            {
                                ServiceName = "USys2 uProcessMonitor  - " + env,
                                UserName = null,
                                Password = null,
                                ServiceExeName = "Usys2.uProcessMonitor.exe"
                            };
                            break;
                        default:
                            throw new Exception(
                                $"'GetCredentials' for not found Env: {env} Project: {proj}, Warehouse: {whs}");
                    }
                    break;
                case Environments.STA:
                    switch (proj)
                    {
                        case ProjectTypes.DocGenService:
                            ret = new Deploy.ServiceCredentials
                            {
                                ServiceName = "USys2 DocGenService - " + env + " - " + whs,
                                UserName = "Usysdocg-srv-STA@prdlog",
                                Password = "DocGen02@S",
                                ServiceExeName = "USys2.DocumentGeneration.Service.exe"
                            };
                            break;
                        case ProjectTypes.PrintService:
                            ret = new Deploy.ServiceCredentials
                            {
                                ServiceName = "USys2 PrintingService  - " + env + " - " + whs,
                                UserName = "usys2ptr-srv-STA@prdlog",
                                Password = "Printing02@S",
                                ServiceExeName = "USys2.PrintingService.exe"
                            };
                            break;
                        case ProjectTypes.ScannerManagementService:
                            ret = new Deploy.ServiceCredentials
                            {
                                ServiceName = "USys2 ScannerManagement  - " + env + " - " + whs,
                                UserName = null,
                                Password = null,
                                ServiceExeName = "USys2.ScannerConsoleManagement.Service.exe"
                            };
                            break;
                        case ProjectTypes.uProcessMonitor:
                            ret = new Deploy.ServiceCredentials
                            {
                                ServiceName = "USys2 uProcessMonitor  - " + env,
                                UserName = null,
                                Password = null,
                                ServiceExeName = "Usys2.uProcessMonitor.exe"
                            };
                            break;
                        default:
                            throw new Exception(
                                $"'GetCredentials' for not found Env: {env} Project: {proj}, Warehouse: {whs}");
                    }
                    break;
                case Environments.CPYPRD:
                    switch (proj)
                    {
                        case ProjectTypes.DocGenService:
                            ret = new Deploy.ServiceCredentials
                            {
                                ServiceName = "USys2 DocGenService - " + env + " - " + whs,
                                UserName = "Usysdocg-srv-CPY-PRD@prdlog",
                                Password = "DocGen04@C",
                                ServiceExeName = "USys2.DocumentGeneration.Service.exe"
                            };
                            break;
                        case ProjectTypes.PrintService:
                            ret = new Deploy.ServiceCredentials
                            {
                                ServiceName = "USys2 PrintingService  - " + env + " - " + whs,
                                UserName = "usys2ptr-srv-CPY-PRD@prdlog",
                                Password = "Printing04@C",
                                ServiceExeName = "USys2.PrintingService.exe"
                            };
                            break;
                        case ProjectTypes.ScannerManagementService:
                            ret = new Deploy.ServiceCredentials
                            {
                                ServiceName = "USys2 ScannerManagement  - " + env + " - " + whs,
                                UserName = null,
                                Password = null,
                                ServiceExeName = "USys2.ScannerConsoleManagement.Service.exe"
                            };
                            break;
                        case ProjectTypes.uProcessMonitor:
                            ret = new Deploy.ServiceCredentials
                            {
                                ServiceName = "USys2 uProcessMonitor  - " + env,
                                UserName = null,
                                Password = null,
                                ServiceExeName = "Usys2.uProcessMonitor.exe"
                            };
                            break;
                        default:
                            throw new Exception(
                                $"'GetCredentials' for not found Env: {env} Project: {proj}, Warehouse: {whs}");
                    }
                    break;
                case Environments.TST:
                    switch (proj)
                    {
                        default:
                            throw new Exception("'GetCredentials' for not found TST");
                    }
                default:
                    throw new Exception("'GetCredentials' for not found 0");

            }
            return ret;
        }

        public static List<string> GetServers(Environments env, ProjectTypes proj)
        {
            var servers = new List<string>();
            switch (env)
            {
                case Environments.PRD:
                    switch (proj)
                    {
                        case ProjectTypes.MVC:
                            servers.Add("10.3.145.16");
                            servers.Add("10.3.145.17");
                            servers.Add("10.3.145.18");
                            servers.Add("10.3.145.19");
                            servers.Add("10.3.145.20");
                            servers.Add("10.3.145.43");
                            servers.Add("10.3.145.48");
                            break;
                        case ProjectTypes.WCFGeneral:
                        case ProjectTypes.WCFWarehouse:
                            //servers.Add("10.3.145.9");
                            //servers.Add("10.3.145.10");
                            //servers.Add("10.3.145.11");
                            //servers.Add("10.3.145.12");
                            //servers.Add("10.3.145.13");
                            servers.Add("10.3.145.46");
                            servers.Add("10.3.145.47");
                            servers.Add("10.3.145.60");
                            servers.Add("10.3.145.61");
                            servers.Add("10.3.145.62");
                            servers.Add("10.3.145.63");
                            servers.Add("10.3.145.64");
                            break;
                        case ProjectTypes.ScannerManagementService:
                        case ProjectTypes.ScannerConsole:
                        case ProjectTypes.uProcessMonitor:
                            servers.Add("10.3.145.3");
                            servers.Add("10.3.145.4");
                            servers.Add("10.3.145.5");
                            servers.Add("10.3.145.6");
                            break;
                        case ProjectTypes.DocGenService:
                            servers.Add("10.3.145.7");//BE11,BG01,FR01,PT01
                            //servers.Add("10.3.145.8");//PL01,PL02,PL03,PL04
                            break;
                        case ProjectTypes.PrintService:
                            throw new Exception($"You cannot deploy printservice Env: {env}  Project: {proj}");
                            break;
                        default:
                            throw new Exception($"Cannot find servers for Env: {env}  Project: {proj}");
                    }
                    break;
                case Environments.DEV:
                    switch (proj)
                    {
                        case ProjectTypes.MVC:
                        case ProjectTypes.PartyApi:
                            servers.Add("10.3.147.10");
                            break;
                        case ProjectTypes.WCFGeneral:
                        case ProjectTypes.WCFWarehouse:
                            servers.Add("10.3.147.11");
                            break;
                        case ProjectTypes.ScannerManagementService:
                        case ProjectTypes.ScannerConsole:
                        case ProjectTypes.uProcessMonitor:
                            servers.Add("10.3.147.3");
                            //servers.Add("10.3.147.4");
                            break;
                        case ProjectTypes.DocGenService:
                        case ProjectTypes.PrintService:
                            servers.Add("10.3.147.5");
                            break;
                        default:
                            throw new Exception($"Cannot find servers for Env: {env}  Project: {proj}");
                    }
                    break;
                case Environments.STA:
                    switch (proj)
                    {
                        case ProjectTypes.MVC:
                        case ProjectTypes.PartyApi:
                            servers.Add("10.3.147.10");
                            break;
                        case ProjectTypes.WCFGeneral:
                        case ProjectTypes.WCFWarehouse:
                            servers.Add("10.3.147.11");
                            break;
                        case ProjectTypes.ScannerManagementService:
                        case ProjectTypes.ScannerConsole:
                        case ProjectTypes.uProcessMonitor:
                            servers.Add("10.3.147.3");
                            //servers.Add("10.3.147.4");
                            break;
                        case ProjectTypes.DocGenService:
                        case ProjectTypes.PrintService:
                            servers.Add("10.3.147.5");
                            break;
                        default:
                            throw new Exception($"Cannot find servers for Env: {env}  Project: {proj}");
                    }
                    break;
                case Environments.ACC:
                    switch (proj)
                    {
                        case ProjectTypes.MVC:
                        case ProjectTypes.PartyApi:
                            servers.Add("10.3.147.6");
                            break;
                        case ProjectTypes.WCFGeneral:
                        case ProjectTypes.WCFWarehouse:
                            servers.Add("10.3.147.7");
                            break;
                        case ProjectTypes.ScannerManagementService:
                        case ProjectTypes.ScannerConsole:
                        case ProjectTypes.uProcessMonitor:
                            servers.Add("10.3.147.3");
                            //servers.Add("10.3.147.4");
                            break;
                        case ProjectTypes.DocGenService:
                        case ProjectTypes.PrintService:
                            servers.Add("10.3.147.5");
                            break;
                        default:
                            throw new Exception($"Cannot find servers for Env: {env}  Project: {proj}");
                    }
                    break;
                case Environments.CPYPRD: 
                    switch (proj)
                    {
                        case ProjectTypes.MVC:
                        case ProjectTypes.PartyApi:
                            servers.Add("10.3.147.10");
                            break;
                        case ProjectTypes.WCFGeneral:
                        case ProjectTypes.WCFWarehouse:
                            servers.Add("10.3.147.11");
                            break;
                        case ProjectTypes.ScannerManagementService:
                        case ProjectTypes.ScannerConsole:
                        case ProjectTypes.uProcessMonitor:
                            servers.Add("10.3.147.3");
                            //servers.Add("10.3.147.4");
                            break;
                        case ProjectTypes.DocGenService:
                        case ProjectTypes.PrintService:
                            servers.Add("10.3.147.5");
                            break;
                        default:
                            throw new Exception($"Cannot find servers for Env: {env}  Project: {proj}");
                    }
                    break;
                default:
                    throw new Exception($"No implementation for GetServers Env: {env}  Project: {proj}");
            }
            return servers;
        }
    }
}