﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;
using DeployConsole.Config;
using DeployConsole.Helpers;
using DeployConsole.Helpers.RemoteProcess;
using DeployConsole.Helpers.ServiceUtils;
using DeployConsole.Logging;
using Microsoft.Web.Administration;
using DeployConsole.Managers;
using System.Collections.Generic;

namespace DeployConsole
{
    class StartStopEnvironment
    {

        public static void DeployStartStop(Environments env, ProjectTypes proj, Warehouses whs, int state)
        {

            Parallel.ForEach(EnumHelper.MaskToList<ProjectTypes>(proj), currentProj =>
            {
                var prepLocation = @"d:\prep\" + env.ToString() + @"\" + currentProj.ToString();

                switch (currentProj)
                {
                    case ProjectTypes.InventoryApi:
                        SetIISStateMVC(env, Warehouses.BE11, state, currentProj);
                        break;
                    case ProjectTypes.MVC:
                    case ProjectTypes.CommandApi:
                        SetIISStateMVC(env, whs, state, currentProj);
                        break;
                    case ProjectTypes.WCFGeneral:
                    case ProjectTypes.WCFWarehouse:
                        SetIISStateWCF(env, whs, currentProj, state);
                        break;
                    case ProjectTypes.ScannerConsole:
                        if (state == 0) KillScanner(env, whs, currentProj);
                        break;
                    case ProjectTypes.uProcessMonitor:
                        //pfff
                        break;
                    default:
                        Services(env, whs, prepLocation, currentProj, state);
                        break;
                }
            });
        }


        public static void Services(Environments env, Warehouses whs, string prepLocation, ProjectTypes currentProj, int state)
        {
            var servers = new ConfigPropertyManager().GetServers(env, currentProj, whs);
            if (servers.Count == 0)
                throw new Exception($"Cannot find servers for Env: {env}  Project: {currentProj}");

            foreach (var server in servers)
            {

                foreach (var currentWh in EnumHelper.MaskToList<Warehouses>(whs))
                {
                    if (currentWh == Warehouses.None) continue;
                    var c = new ConfigPropertyManager().GetCredentials(env, currentProj, currentWh);
                    if (c == null)
                        throw new Exception(
                            $"'GetCredentials' for not found Env: {env} Project: {currentProj}, Warehouse: {whs}");

                    var serviceName = string.Format(c.ServiceName, env, currentWh);

                    var serviceExeName = c.ServiceExeName;


                    var logGroup = Guid.NewGuid().ToString();

                    var destinationPathLocal = $@"c:\Usys\{env}\{currentProj}\{currentWh}";

                    var destinationFullPathLocal = Path.Combine(destinationPathLocal, serviceExeName);
                    var serviceAlreadyInstalled = WmiService.Instance.IsServiceInstalled(server, serviceName);
                    if (state == 0)
                    {
                        try
                        {
                            var pidToWatch = WmiService.Instance.GetPidForService(server, destinationFullPathLocal);
                            Console.WriteLine("Working for {0} {1} pid: {2} on {3}", currentProj, currentWh, pidToWatch.ToString(CultureInfo.InvariantCulture), server);
                            var sh = new ServiceHelper(serviceName, server);
                            if (sh.GetStatus() == ServiceControllerStatus.Running)
                                ExecutionHelper.DoSomething(() => { if (sh != null) sh.Stop(); }
                                    , "Stop service", $"Name: {serviceName}", env, currentProj, currentWh, logGroup, 0);
                            else
                            {
                                Logger.Log(new LoggingData
                                {
                                    Env = env,
                                    Exception = null,
                                    ExtraMessage = "",
                                    Group = logGroup,
                                    Message = $"Stopping '{serviceName}' NOT EXECUTED, it's in state {sh.GetStatus()}",
                                    OrderNr = 0,
                                    Project = currentProj,
                                    Wh = currentWh
                                });
                            }
                            sh.Dispose();

                            if (pidToWatch != 0)
                            {
                                if (!Deploy.WaitForProcess(pidToWatch, server, TimeSpan.FromSeconds(55)))
                                    ExecutionHelper.DoSomething(() => KillRemoteProcess.KillProcess(env, currentProj, pidToWatch, currentWh, server)
                                        , "Kill remote process because it still exists", "", env, currentProj, currentWh, logGroup, 1, true);
                            }
                            //NEEM BACKUP!!!
                        }
                        catch (Exception e)
                        {
                            Logger.Log(new LoggingData
                            {
                                Env = env,
                                Exception = e,
                                ExtraMessage = "",
                                Group = logGroup,
                                Message = $"Stopping service '{serviceName}' FAILED",
                                OrderNr = 3,
                                Project = currentProj,
                                Wh = currentWh
                            });
                        }

                    }
                    else
                    {

                        var sh = new ServiceHelper(serviceName, server);
                        if (sh.GetStatus() == ServiceControllerStatus.Stopped)
                        {
                            var isItStarted = ExecutionHelper.DoSomething(() => sh.StartTimeout()
                                , "Start service", $"Servicename: {serviceName}", env, currentProj, currentWh, logGroup, 5);
                            if (!isItStarted)
                            {
                                Logger.Log(new LoggingData
                                {
                                    Env = env,
                                    Exception = new Exception($"Starting service '{serviceName}' on server {server}  FAILED"),
                                    ExtraMessage = "",
                                    Group = logGroup,
                                    Message = $"Starting service '{serviceName}' on server {server} FAILED",
                                    OrderNr = 6,
                                    Project = currentProj,
                                    Wh = currentWh
                                });
                            }
                        }
                        else
                        {
                            Logger.Log(new LoggingData
                            {
                                Env = env,
                                Exception = null,
                                ExtraMessage = "",
                                Group = logGroup,
                                Message =
                                    $"Starting service '{serviceName}' NOT EXECUTED on server {server}, it's in state {sh.GetStatus()}",
                                OrderNr = 5,
                                Project = currentProj,
                                Wh = currentWh
                            });
                        }
                    }
                };
            };
        }


        private static void SetIISStateWCF(Environments env, Warehouses whs, ProjectTypes currentProj, int state)
        {
            var servers = new ConfigPropertyManager().GetServers(env, currentProj, whs);
            if (servers.Count == 0)
                throw new Exception($"Cannot find servers for Env: {env}  Project: {currentProj}");

            Parallel.ForEach(servers, server =>
            {
                //Stop AppPool + update VD + Start pool

                var siteNameConfig = new ConfigPropertyManager().GetSimpleConfigProperties(env, currentProj, "SiteName");
                List<string> siteNames = new List<string>();
                var values = whs.ToString()
                       .Split(new[] { ", " }, StringSplitOptions.None)
                       .Select(v => (Warehouses)Enum.Parse(typeof(Warehouses), v));
                foreach (var w in values)
                {
                    if (w.Equals(Warehouses.None))
                        throw new Exception("'GetSiteNames' for not found 1");
                    else if (w.Equals(Warehouses.BE11))
                        siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.BE11, env, currentProj));

                    else if (w.Equals(Warehouses.BG01))
                        siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.BG01, env, currentProj));

                    else if (w.Equals(Warehouses.FR01))
                        siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.FR01, env, currentProj));

                    else if (w.Equals(Warehouses.PL01))
                        siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PL01, env, currentProj));

                    else if (w.Equals(Warehouses.PL02))
                        siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PL02, env, currentProj));

                    else if (w.Equals(Warehouses.PL03))
                        siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PL03, env, currentProj));

                    else if (w.Equals(Warehouses.PL04))
                        siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PL04, env, currentProj));

                    else if (w.Equals(Warehouses.PT01))
                        siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PT01, env, currentProj));
                }

                var iis = IisHelper.GetIisHelper(server);
                var siteNamesString = "";
                var logGroup = Guid.NewGuid().ToString();

                foreach (var siteName in siteNames)
                {
                    siteNamesString += siteName + "; ";
                }

                ExecutionHelper.DoSomething(
                    () =>
                        iis.SetApplicationPoolStateForVd(siteNames,
                            state == 0 ? ObjectState.Stopped : ObjectState.Started)
                    , "Set application pools:" + (state == 0 ? "Stopped" : "Started"), siteNamesString, env,
                    currentProj, null, logGroup, 0);

            });

        }
        private static void SetIISStateMVC(Environments env, Warehouses whs, int state, ProjectTypes proj)
        {
            var servers = new ConfigPropertyManager().GetServers(env, proj, whs);
            if (servers.Count == 0)
                throw new Exception($"Cannot find servers for Env: {env}  Project: {proj}");

            Parallel.ForEach(servers, server =>
            {
                var siteNameConfig = new ConfigPropertyManager().GetSimpleConfigProperties(env, proj, "SiteName");
                List<string> siteNames = new List<string>();
                var values = whs.ToString()
                       .Split(new[] { ", " }, StringSplitOptions.None)
                       .Select(v => (Warehouses)Enum.Parse(typeof(Warehouses), v));
                foreach (var w in values)
                {
                    if (w.Equals(Warehouses.None))
                        throw new Exception("'GetSiteNames' for not found 1");
                    else if (w.Equals(Warehouses.BE11))
                        siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.BE11, env, proj));

                    else if (w.Equals(Warehouses.BG01))
                        siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.BG01, env, proj));

                    else if (w.Equals(Warehouses.FR01))
                        siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.FR01, env, proj));

                    else if (w.Equals(Warehouses.PL01))
                        siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PL01, env, proj));

                    else if (w.Equals(Warehouses.PL02))
                        siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PL02, env, proj));

                    else if (w.Equals(Warehouses.PL03))
                        siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PL03, env, proj));

                    else if (w.Equals(Warehouses.PL04))
                        siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PL04, env, proj));

                    else if (w.Equals(Warehouses.PT01))
                        siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PT01, env, proj));
                }

                var iis = IisHelper.GetIisHelper(server);
                var siteNamesString = "";
                var logGroup = Guid.NewGuid().ToString();

                foreach (var siteName in siteNames)
                {
                    siteNamesString += siteName + "; ";
                }

                ExecutionHelper.DoSomething(
                    () => iis.SetApplicationPoolStateForSite(siteNames, state == 0 ? ObjectState.Stopped : ObjectState.Started)
                    , "Set application pools:" + (state == 0 ? "Stopped" : "Started"), siteNamesString, env, proj, null, logGroup, 0);

            });
        }
        private static void KillScanner(Environments env, Warehouses whs, ProjectTypes currentProj)
        {
            var servers = new ConfigPropertyManager().GetServers(env, currentProj, whs);
            if (servers.Count == 0)
                throw new Exception($"Cannot find servers for Env: {env}  Project: {currentProj}");

            foreach (var server in servers)
            {
                var server1 = server; //Bypass Resharper AccessToForEachVariableInClosure
                Parallel.ForEach(EnumHelper.MaskToList<Warehouses>(whs), currentWh =>
                {
                    try
                    {
                        if (currentWh == Warehouses.None) return;
                        try
                        {
                            //kill all processes for currentWH
                            var processes = Process.GetProcessesByName("USys2.ScannerConsole", server1);
                            KillRemoteProcess.KillProcess(env, currentProj, processes.Select(process => process.Id).ToList(), currentWh, server1);
                        }
                        catch (Exception e)
                        {
                            Logger.Log(new LoggingData
                            {
                                Env = env,
                                Exception = e,
                                Message =
                                    $"Kill process FAILED on server {server1}",
                                Project = currentProj,
                                Wh = currentWh
                            });
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Log(new LoggingData
                        {
                            Env = env,
                            Exception = e,
                            Message = e.Message + "\n" + e.StackTrace,
                            Project = currentProj,
                            Wh = currentWh
                        });
                    }
                });
            }
        }


    }
}
