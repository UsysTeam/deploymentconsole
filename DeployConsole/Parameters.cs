﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml.Xsl;
using DeployConsole.Config;
using DeployConsole.Managers;

namespace DeployConsole
{
    class Parameters
    {
        public  static XsltArgumentList GetAllArgumentList(Environments currentEnv, ProjectTypes currentProj, Warehouses currentWH = Warehouses.None)
        {
            var argsList = new XsltArgumentList();
            var parms = GetTransformParametersDB(currentEnv, currentProj, currentWH);
            //One exception - WH
            if (currentWH != Warehouses.None) parms.Add("WS", currentWH.ToString());

            //Resolving Parameters in parameters [Specific for env/proj/wh]
            foreach (var parm in parms.ToList())
            {
                var value = parm.Value;
                var tel = 0;

                while (value.Contains("%"))
                {
                    tel++;
                    if (tel > 20)
                    {
#if DEBUG
                        Debugger.Break();
#else
                        throw new Exception(string.Format("Looping more then 20 times through param '{0}', org value is '{1}', current value is '{2}'",  parm.Key, parm.Value, value));
#endif
                    }

                    if (value.Contains("%WS%"))
                        if (currentWH == Warehouses.None)
                        {
                            value = value.Replace("_%WS%", "");
                            if (value.Contains("%WS%"))
                                value = "";
                        }
                        else
                            value = value.Replace("%WS%", currentWH.ToString());
                    else if (value.Contains("%"))
                    {
                        var splitted = value.Split(Convert.ToChar("%"));
                        for (int i = 1; i < splitted.Length; i++)
                        {
                            //try
                            //{
                                if (!IsOdd(i)) continue;
                                //Console.WriteLine(splitted[i]);
                                string transformParametersPerEnvWh = "";
                                //if (currentWH != Warehouse.None)
                                transformParametersPerEnvWh = GetTransformParametersPerEnvWH(currentEnv, currentWH, splitted[i]);
                                if (string.IsNullOrEmpty(transformParametersPerEnvWh)) //
                                {
                                    //no warehouse specific var, try resolving from paramlist
                                    if (parms.ContainsKey(splitted[i]))
                                        transformParametersPerEnvWh = parms[splitted[i]];
                                    else
                                        throw new Exception(
                                            $"Cannot resolve variable {splitted[i]} for parameter {parm.Key} [{parm.Value}]");
                                }
                                splitted[i] = transformParametersPerEnvWh;
                            //}
                            //catch (Exception e)
                            //{
                            //    Console.WriteLine(e);
                            //}
                        }
                        value = string.Join("", splitted);
                    }
                }

                argsList.AddParam(parm.Key, "", value);
            }
            return argsList;
        }



        public static IDictionary<string, string> GetAllParmList(Environments currentEnv, ProjectTypes currentProj, Warehouses currentWH = Warehouses.None,string key=null)
        {
            //var argsList = new XsltArgumentList();
            var parms = GetTransformParametersDB(currentEnv, currentProj, currentWH, key);
            //One exception - WH
            if (currentWH != Warehouses.None) parms.Add("WS", currentWH.ToString());

            //Resolving Parameters in parameters [Specific for env/proj/wh]
            foreach (var parm in parms.ToList())
            {
                var value = parm.Value;
                var tel = 0;

                while (value.Contains("%"))
                {
                    tel++;
                    if (tel > 20)
                    {
#if DEBUG
                        Debugger.Break();
#else
                        throw new Exception(string.Format("Looping more then 20 times through param '{0}', org value is '{1}', current value is '{2}'",  parm.Key, parm.Value, value));
#endif
                    }

                    if (value.Contains("%WS%"))
                        if (currentWH == Warehouses.None)
                        {
                            value = value.Replace("_%WS%", "");
                            if (value.Contains("%WS%"))
                                value = "";
                        }
                        else
                            value = value.Replace("%WS%", currentWH.ToString());
                    else if (value.Contains("%"))
                    {
                        var splitted = value.Split(Convert.ToChar("%"));
                        for (int i = 1; i < splitted.Length; i++)
                        {
                            try
                            {
                                if (!IsOdd(i)) continue;
                               // Console.WriteLine(splitted[i]);
                                string transformParametersPerEnvWh = "";
                                //if (currentWH != Warehouse.None)
                                transformParametersPerEnvWh = GetTransformParametersPerEnvWH(currentEnv, currentWH, splitted[i]);
                                if (string.IsNullOrEmpty(transformParametersPerEnvWh)) //
                                {
                                    //no warehouse specific var, try resolving from paramlist
                                    if (parms.ContainsKey(splitted[i]))
                                        transformParametersPerEnvWh = parms[splitted[i]];
                                    else
                                        throw new Exception(
                                            $"Cannot resolve variable {splitted[i]} for parameter {parm.Key} [{parm.Value}]");
                                }
                                splitted[i] = transformParametersPerEnvWh;
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }
                        }
                        value = string.Join("", splitted);
                    }
                }
                parms[parm.Key] = value;
                //argsList.AddParam(parm.Key, "", value);
            }
            return parms;
        }

        public static bool IsOdd(int value)
        {
            return value % 2 != 0;
        }


        private static IDictionary<string, string> GetTransformParametersDB(Environments currentEnv, ProjectTypes currentProj, Warehouses wh, string key = null)
        {
            var ret = new Dictionary<string, string>();
            //ret.Add("test", "ok");
            //if (currentEnv == Environments.DEV)
            //{
                var arr = new ConfigPropertyManager().GetSimpleConfigProperties(currentEnv, currentProj, wh, key).ToDictionary(p => p.PropertyName, k => k.PropertyValue);
                return arr;
            //}
            //return new Dictionary<string, string>();
        }

        private static IDictionary<string, string> GetTransformParameters(Environments currentEnv, ProjectTypes currentProj, Warehouses wh)
        {
            var ret = new Dictionary<string, string>();
            return new ConfigPropertyManager().GetSimpleConfigProperties(currentEnv, currentProj, wh).ToDictionary(p => p.PropertyName, k => k.PropertyValue);
            if (currentEnv == Environments.DEV)
            {
                //return new ConfigPropertyManager().GetSimpleConfigProperties(currentEnv, currentProj, wh).ToDictionary(p => p.PropertyName, k => k.PropertyValue);
                ////common
                ////ret.Add("TracelogConnectionString", "Data Source=10.3.93.136,1435;Initial Catalog=TRUNK_TST_USYS2_LOG_%WS%;Uid=usys2admin; Pwd=univeg;MultipleActiveResultSets=True");
                //ret.Add("SQLServer", "10.3.145.57,DEVACCUSYS");
                //ret.Add("TelnetServer", "10.3.147.3");
                //ret.Add("TracelogConnectionString", "Data Source=%SQLServer%;Initial Catalog=DEV_USYS2_LOG_%WS%;Uid=%Usys2AdminUser%; Pwd=%Usys2AdminPassw%;MultipleActiveResultSets=True");

                //ret.Add("MinLogLevel", "Information");
                //ret.Add("Usys2AdminUser", "Usys2Admin");
                //ret.Add("Usys2AdminPassw", "univeg");


                //ret.Add("Usys2User", "Usys2");
                //ret.Add("Usys2Passw", "univeg");

                //ret.Add("GeneralServicesBaseUri", "https://servicesDEV.univegsolutions.com/Usys2.GeneralServices.DEV/");
                //ret.Add("WarehouseServicesBaseUri", "https://servicesDEV.univegsolutions.com/Usys2.WarehouseServices.DEV.%WS%/");
                ////ScannerManagement
                //ret.Add("ManagementAddress", "net.tcp://%TelnetServer%:%ManagementPort%/ScannerManagement");
                ////scannerConsole
                //ret.Add("OverrideWarehouseServicesBaseUri", "");

                ////Printing
                //ret.Add("SpoolerZombieTimeout", "150000");
                //ret.Add("NoPrintersWaitDelay", "120000");
                //ret.Add("PrintTimeout", "60000");
                //ret.Add("LogStatus", "False");
                //ret.Add("SanityInterval", "100000");
                //ret.Add("SpoolerInterval", "3000");
                ////docgen
                //ret.Add("DeleteSuccessfullRequests", "false");
                //ret.Add("MaxWorkerThreads", "15");
                //ret.Add("ForceSequentialProcessing", "false");
                //ret.Add("ThreadSleepTime", "1000");
                //ret.Add("ClientSettingsProvider.ServiceUri", "");
                //ret.Add("WarehouseEntities", "metadata=res://*/Warehouse.csdl|res://*/Warehouse.ssdl|res://*/Warehouse.msl;provider=System.Data.SqlClient;provider connection string=&quot;Data Source=%SQLServer%;Initial Catalog=DEV_USYS2_W_%WS%;Uid=%Usys2AdminUser%; Pwd=%Usys2AdminPassw%;MultipleActiveResultSets=True&quot;");
                //ret.Add("GeneralEntities", "metadata=res://*/General.csdl|res://*/General.ssdl|res://*/General.msl;provider=System.Data.SqlClient;provider connection string=&amp;quot;Data Source=%SQLServer%;Initial Catalog=DEV_USYS2_GENERAL;Uid=%Usys2AdminUser%; Pwd=%Usys2AdminPassw%;MultipleActiveResultSets=True&amp;quot;&quot; providerName=&quot;System.Data.EntityClient");
                //ret.Add("Warehouse", "%WS%");

                ////web-config
                //ret.Add("EnableTrafficLogging", "true"); //Add
                //ret.Add("configSource", @"Config\SystemWeb\SessionState.config");
                //ret.Add("connectionStrings", @"");   //Add
                //ret.Add("generalWebServices", @"Config\Services\GeneralWebServices.PRD.config");
                //ret.Add("warehouseWebServices", @"Config\Services\WarehouseWebServices.PRD.config");
                //ret.Add("ReportServerUrl", "https://WebReportsDev.univegsolutions.com/Reports_DEVACCUSYS/Pages/Folder.aspx");

                ////web-ApplicationInsights.config
                ////namespace http://schemas.microsoft.com/ApplicationInsights/2013/Settings
                ////xpath /ApplicationInsights/Profiles/Defaults/ComponentSettings/ComponentID
                //ret.Add("ComponentID", "26ccb6b6-9674-4019-9dd2-1cdddfa2f1e4-ME TESTING");
                //ret.Add("ComponentName", "Usys 2 - Mvc - 4.0-ME TESTING");

                //ret.Add("SessionStateSqlConnectionString", "Data Source=%SQLServer%;User ID=%Usys2StateUser%;Password=%Usys2StatePassw%;");
                //ret.Add("Usys2StateUser", "Usys2StateUser");
                //ret.Add("Usys2StatePassw", "Univeg01");

                ////web- GeneralWebServices.dev
                ////ret.Add("generalWebServicesbaseUrl", "https://localhost/DEV.GeneralServices-transformOK");
                //ret.Add("generalWebServicesBaseUrl", "https://localhost/DEV.GeneralServices");
                ////web- WarehouseWebServices.dev.config
                ////ret.Add("warehouseWebServicesbaseUrl", "https://localhost/DEV.WarehouseServices-%WS%/");
                //ret.Add("warehouseWebServicesbaseUrl", "");
                ////web- Config\SystemWeb\SessionState.config
                ////ret.Add("sessionStatesqlConnectionString", "https://localhost/DEV.WarehouseServices");
                //ret.Add("TracelogConnectionStringName", "TraceLog");
                //ret.Add("GeneralEntitiesName", "GeneralEntities");
                //ret.Add("WarehouseEntitiesName", "WarehouseEntities");
                //ret.Add("WarehouseServiceServers", "10.3.147.7");


            }
            return ret;
        }

        #region "ORG"
        private static IDictionary<string, string> GetTransformParameters_org(Environments currentEnv, ProjectTypes currentProj)
        {
            var ret = new Dictionary<string, string>();
            if (currentEnv == Environments.DEV)
            {
                //common
                //ret.Add("TracelogConnectionString", "Data Source=10.3.93.136,1435;Initial Catalog=TRUNK_TST_USYS2_LOG_%WS%;Uid=usys2admin; Pwd=univeg;MultipleActiveResultSets=True");
                ret.Add("SQLServer", "10.3.145.57,DEVACCUSYS");
                ret.Add("TelnetServer", "10.3.147.3");
                ret.Add("TracelogConnectionString", "Data Source=%SQLServer%;Initial Catalog=DEV_USYS2_LOG_%WS%;Uid=%Usys2AdminUser%; Pwd=%Usys2AdminPassw%;MultipleActiveResultSets=True");

                ret.Add("MinLogLevel", "Information");
                ret.Add("Usys2AdminUser", "Usys2Admin");
                ret.Add("Usys2AdminPassw", "univeg");


                ret.Add("Usys2User", "Usys2");
                ret.Add("Usys2Passw", "univeg");

                ret.Add("GeneralServicesBaseUri", "https://servicesDEV.univegsolutions.com/Usys2.GeneralServices.DEV/");
                ret.Add("WarehouseServicesBaseUri", "https://servicesDEV.univegsolutions.com/Usys2.WarehouseServices.DEV.%WS%/");
                //ScannerManagement
                ret.Add("ManagementAddress", "net.tcp://%TelnetServer%:%ManagementPort%/ScannerManagement");
                //scannerConsole
                ret.Add("OverrideWarehouseServicesBaseUri", "");

                //Printing
                ret.Add("SpoolerZombieTimeout", "150000");
                ret.Add("NoPrintersWaitDelay", "120000");
                ret.Add("PrintTimeout", "60000");
                ret.Add("LogStatus", "False");
                ret.Add("SanityInterval", "100000");
                ret.Add("SpoolerInterval", "3000");
                //docgen
                ret.Add("DeleteSuccessfullRequests", "false");
                ret.Add("MaxWorkerThreads", "15");
                ret.Add("ForceSequentialProcessing", "false");
                ret.Add("ThreadSleepTime", "1000");
                ret.Add("ClientSettingsProvider.ServiceUri", "");
                ret.Add("WarehouseEntities", "metadata=res://*/Warehouse.csdl|res://*/Warehouse.ssdl|res://*/Warehouse.msl;provider=System.Data.SqlClient;provider connection string=&quot;Data Source=%SQLServer%;Initial Catalog=DEV_USYS2_W_%WS%;Uid=%Usys2AdminUser%; Pwd=%Usys2AdminPassw%;MultipleActiveResultSets=True&quot;");
                ret.Add("GeneralEntities", "metadata=res://*/General.csdl|res://*/General.ssdl|res://*/General.msl;provider=System.Data.SqlClient;provider connection string=&amp;quot;Data Source=%SQLServer%;Initial Catalog=DEV_USYS2_GENERAL;Uid=%Usys2AdminUser%; Pwd=%Usys2AdminPassw%;MultipleActiveResultSets=True&amp;quot;&quot; providerName=&quot;System.Data.EntityClient");
                ret.Add("Warehouse", "%WS%");

                //web-config
                ret.Add("EnableTrafficLogging", "true"); //Add
                ret.Add("configSource", @"Config\SystemWeb\SessionState.config");
                ret.Add("connectionStrings", @"");   //Add
                ret.Add("generalWebServices", @"Config\Services\GeneralWebServices.PRD.config");
                ret.Add("warehouseWebServices", @"Config\Services\WarehouseWebServices.PRD.config");
                ret.Add("ReportServerUrl", "https://WebReportsDev.univegsolutions.com/Reports_DEVACCUSYS/Pages/Folder.aspx");

                //web-ApplicationInsights.config
                //namespace http://schemas.microsoft.com/ApplicationInsights/2013/Settings
                //xpath /ApplicationInsights/Profiles/Defaults/ComponentSettings/ComponentID
                ret.Add("ComponentID", "26ccb6b6-9674-4019-9dd2-1cdddfa2f1e4-ME TESTING");
                ret.Add("ComponentName", "Usys 2 - Mvc - 4.0-ME TESTING");

                ret.Add("SessionStateSqlConnectionString", "Data Source=%SQLServer%;User ID=%Usys2StateUser%;Password=%Usys2StatePassw%;");
                ret.Add("Usys2StateUser", "Usys2StateUser");
                ret.Add("Usys2StatePassw", "Univeg01");

                //web- GeneralWebServices.dev
                //ret.Add("generalWebServicesbaseUrl", "https://localhost/DEV.GeneralServices-transformOK");
                ret.Add("generalWebServicesBaseUrl", "https://localhost/DEV.GeneralServices");
                //web- WarehouseWebServices.dev.config
                //ret.Add("warehouseWebServicesbaseUrl", "https://localhost/DEV.WarehouseServices-%WS%/");
                ret.Add("warehouseWebServicesbaseUrl", "");
                //web- Config\SystemWeb\SessionState.config
                //ret.Add("sessionStatesqlConnectionString", "https://localhost/DEV.WarehouseServices");
                ret.Add("TracelogConnectionStringName", "TraceLog");
                ret.Add("GeneralEntitiesName", "GeneralEntities");
                ret.Add("WarehouseEntitiesName", "WarehouseEntities");
                ret.Add("WarehouseServiceServers", "10.3.147.7");


            }
            return ret;
        }


        private static XsltArgumentList GetAllArgumentList_ORG(Environments currentEnv, ProjectTypes currentProj, Warehouses currentWH = Warehouses.None)
        {
            var argsList = new XsltArgumentList();
            var parms = GetTransformParameters(currentEnv, currentProj, currentWH);
            //One exception - WH
            if (currentWH != Warehouses.None) parms.Add("WS", currentWH.ToString());

            //Resolving Parameters in parameters [Specific for env/proj/wh]
            foreach (var parm in parms.ToList())
            {
                var value = parm.Value;
                var tel = 0;
                while (value.Contains("%"))
                {
                    tel++;
                    if (tel > 20)
                    {
#if DEBUG
                        Debugger.Break();
#else
                        throw new Exception(string.Format("Looping more then 20 times through param '{0}', org value is '{1}', current value is '{2}'",  parm.Key, parm.Value, value));
#endif
                    }

                    if (value.Contains("%WS%"))
                        if (currentWH == Warehouses.None)
                        {
                            value = value.Replace("_%WS%", "");
                            if (value.Contains("%WS%"))
                                value = "";
                        }
                        else
                            value = value.Replace("%WS%", currentWH.ToString());
                    else if (value.Contains("%"))
                    {
                        var splitted = value.Split(Convert.ToChar("%"));
                        for (int i = 1; i < splitted.Length; i++)
                        {
                            try
                            {
                                if (!IsOdd(i)) continue;
                                //Console.WriteLine(splitted[i]);
                                string transformParametersPerEnvWh = "";
                                //if (currentWH != Warehouse.None)
                                transformParametersPerEnvWh = GetTransformParametersPerEnvWH(currentEnv, currentWH, splitted[i]);
                                if (string.IsNullOrEmpty(transformParametersPerEnvWh)) //
                                {
                                    //no warehouse specific var, try resolving from paramlist
                                    if (parms.ContainsKey(splitted[i]))
                                        transformParametersPerEnvWh = parms[splitted[i]];
                                    else
                                        throw new Exception(
                                            $"Cannot resolve variable {splitted[i]} for parameter {parm.Key} [{parm.Value}]");
                                }
                                splitted[i] = transformParametersPerEnvWh;
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }
                        }
                        value = string.Join("", splitted);
                    }
                }
                argsList.AddParam(parm.Key, "", value);
            }
            return argsList;
        }


        #endregion

        private static string GetTransformParametersPerEnvWH(Environments currentEnv, Warehouses wh, string key)
        {
            var ret = "";
            return new ConfigPropertyManager().GetSimpleConfigProperties(currentEnv, wh, key).ToDictionary(p => p.PropertyName, k => k.PropertyValue).FirstOrDefault().Value;
            switch (currentEnv)
            {
                case Environments.DEV:
                    switch (wh.ToString())
                    {
                        case "PT01":
                            if (key == "ManagementPort")
                                ret = "23009";
                            break;
                        case "BG01":
                            if (key == "ManagementPort")
                                ret = "23001";
                            break;
                        case "PL01":
                            if (key == "ManagementPort")
                                ret = "23004";
                            break;
                        case "PL02":
                            if (key == "ManagementPort")
                                ret = "23003";
                            break;
                        case "PL03":
                            if (key == "ManagementPort")
                                ret = "23008";
                            break;
                        case "FR01":
                            if (key == "ManagementPort")
                                ret = "23010";
                            break;
                        case "None":
                            if (key == "ManagementPort")
                                ret = "99999";
                            break;
                        default:
                            throw new Exception($"Unable to find {wh} for Env: {currentEnv}");
                    }
                    break;
                case Environments.ACC:
                    switch (wh.ToString())
                    {
                        case "PT01":
                            if (key == "ManagementPort")
                                ret = "27009";
                            break;
                        case "BG01":
                            if (key == "ManagementPort")
                                ret = "27001";
                            break;
                        case "PL01":
                            if (key == "ManagementPort")
                                ret = "27004";
                            break;
                        case "PL02":
                            if (key == "ManagementPort")
                                ret = "27003";
                            break;
                        case "PL03":
                            if (key == "ManagementPort")
                                ret = "27008";
                            break;
                        case "None":
                            if (key == "ManagementPort")
                                ret = "99999";
                            break;
                        default:
                            throw new Exception($"Unable to find {wh} for Env: {currentEnv}");
                    }
                    break;
                case Environments.STA:
                    switch (wh.ToString())
                    {
                        case "PT01":
                            if (key == "ManagementPort")
                                ret = "26009";
                            break;
                        case "BG01":
                            if (key == "ManagementPort")
                                ret = "26001";
                            break;
                        case "PL01":
                            if (key == "ManagementPort")
                                ret = "26004";
                            break;
                        case "PL02":
                            if (key == "ManagementPort")
                                ret = "26003";
                            break;
                        case "PL03":
                            if (key == "ManagementPort")
                                ret = "26008";
                            break;
                        case "None":
                            if (key == "ManagementPort")
                                ret = "99999";
                            break;
                        default:
                            throw new Exception($"Unable to find {wh} for Env: {currentEnv}");
                    }
                    break;
                case Environments.TST:
                    switch (wh.ToString())
                    {
                        case "PT01":
                            if (key == "ManagementPort")
                                ret = "25009";
                            break;
                        case "BG01":
                            if (key == "ManagementPort")
                                ret = "25001";
                            break;
                        case "PL01":
                            if (key == "ManagementPort")
                                ret = "25004";
                            break;
                        case "PL02":
                            if (key == "ManagementPort")
                                ret = "25003";
                            break;
                        case "PL03":
                            if (key == "ManagementPort")
                                ret = "25008";
                            break;
                        case "None":
                            if (key == "ManagementPort")
                                ret = "99999";
                            break;
                        default:
                            throw new Exception($"Unable to find {wh} for Env: {currentEnv}");
                    }
                    break;
                case Environments.PRD:
                    switch (wh.ToString())
                    {
                        case "PT01":
                            if (key == "ManagementPort")
                                ret = "23009";
                            break;
                        case "BG01":
                            if (key == "ManagementPort")
                                ret = "23001";
                            break;
                        case "PL01":
                            if (key == "ManagementPort")
                                ret = "23004";
                            break;
                        case "PL02":
                            if (key == "ManagementPort")
                                ret = "23003";
                            break;
                        case "PL03":
                            if (key == "ManagementPort")
                                ret = "23008";
                            break;
                        case "FR01":
                            if (key == "ManagementPort")
                                ret = "23010";
                            break;
                        case "None":
                            if (key == "ManagementPort")
                                ret = "99999";
                            break;
                        default:
                            throw new Exception($"Unable to find {wh} for Env: {currentEnv}");
                    }
                    break;
                default:
                    throw new Exception("Unable to Env: " + currentEnv);
            }
            return ret;
        }
    }
}
