﻿using System;
using DeployConsole.Config;

namespace DeployConsole.Data.Model
{
    public class ConfigProperty
    {
        public int Id { get; set; }

        public Environments Environment { get; set; }

        public ProjectTypes? ProjectType { get; set; }

        public Warehouses? Warehouse { get; set; }

        public string PropertyName { get; set; }

        public string PropertyValue { get; set; }

        public ProjectTypes? ReferenceProjectType { get; set; }

        public string StockOwnerCode { get; set; }
    }

    public class ConfigProperty_ORG
    {
        public Guid Id { get; set; }

        public Environments Environment { get; set; }

        public ProjectTypes ProjectType { get; set; }

        public Warehouses Warehouse { get; set; }

        public string PropertyName { get; set; }

        public string PropertyValue { get; set; }

        public bool? HasVariables { get; set; }
    }
}
