﻿using System;
using DeployConsole.Config;

namespace DeployConsole.Data.Model
{
    public class PropertyVariable
    {
        public Guid Id { get; set; }

        public Environments Environment { get; set; }

        public ProjectTypes ProjectType { get; set; }

        public Warehouses Warehouse { get; set; }

        public string VariableName { get; set; }

        public string VariableValue { get; set; }
    }
}