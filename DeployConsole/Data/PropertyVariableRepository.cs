﻿using System;
using System.Collections.Generic;
using System.Linq;
using DeployConsole.Config;
using DeployConsole.Data.Model;
using DeployConsole.Logging;

namespace DeployConsole.Data
{
    /// <summary>
    /// Repository for peroperty variables
    /// </summary>
    public class PropertyVariableRepository
    {
        private readonly DataContext _context;

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyVariableRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public PropertyVariableRepository(DataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Gets the property variables.
        /// </summary>
        /// <param name="environments">The environments.</param>
        /// <param name="projectTypes">The project types.</param>
        /// <param name="warehouses">The warehouses.</param>
        /// <returns></returns>
        public List<PropertyVariable> GetPropertyVariables(Environments environments, ProjectTypes projectTypes, Warehouses warehouses)
        {
            try
            {
                var environmentList = Enum.GetValues(typeof(Environments)).Cast<Environments>().Where(e => environments.HasFlag(e)).ToList();
                var projectTypeList = Enum.GetValues(typeof(ProjectTypes)).Cast<ProjectTypes>().Where(e => projectTypes.HasFlag(e)).ToList();
                var warehouseList = Enum.GetValues(typeof(Warehouses)).Cast<Warehouses>().Where(e => warehouses.HasFlag(e)).ToList();


                var variables = _context.PropertyVariables
                    .Where(p => environmentList.Contains(p.Environment)
                        && projectTypeList.Contains(p.ProjectType)
                        && warehouseList.Contains(p.Warehouse))
                    .ToList();

                return variables;
            }
            catch (Exception exception)
            {
                var data = new LoggingData
                {
                    Env = environments,
                    Project = projectTypes,
                    Wh = warehouses,
                    Exception = exception,
                    Message = "Unable to retrieve the config properties."
                };
                Logger.Log(data);
                throw;
            }
        }

        public List<PropertyVariable> GetPropertyVariables_ORG(Environments environments, ProjectTypes projectTypes, Warehouses warehouses)
        {
            try
            {
                var environmentList = Enum.GetValues(typeof(Environments)).Cast<Environments>().Where(e => environments.HasFlag(e)).ToList();
                var projectTypeList = Enum.GetValues(typeof(ProjectTypes)).Cast<ProjectTypes>().Where(e => projectTypes.HasFlag(e)).ToList();
                var warehouseList = Enum.GetValues(typeof(Warehouses)).Cast<Warehouses>().Where(e => warehouses.HasFlag(e)).ToList();

                var variables = _context.PropertyVariables
                    .Where(p => environmentList.Contains(p.Environment)
                        && projectTypeList.Contains(p.ProjectType)
                        && warehouseList.Contains(p.Warehouse))
                    .ToList();

                return variables;
            }
            catch (Exception exception)
            {
                var data = new LoggingData
                {
                    Env = environments,
                    Project = projectTypes,
                    Wh = warehouses,
                    Exception = exception,
                    Message = "Unable to retrieve the config properties."
                };
                Logger.Log(data);
                throw;
            }
        }
    }
}