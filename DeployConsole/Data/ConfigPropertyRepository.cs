﻿using System;
using System.Collections.Generic;
using System.Linq;
using DeployConsole.Config;
using DeployConsole.Data.Model;
using DeployConsole.Logging;

namespace DeployConsole.Data
{
    /// <summary>
    /// Repository for config properties.
    /// </summary>
    public class ConfigPropertyRepository
    {
        private readonly DataContext _context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigPropertyRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public ConfigPropertyRepository(DataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Gets the configuration properties.
        /// </summary>
        /// <param name="environments">The environment.</param>
        /// <param name="projectTypes">The project types.</param>
        /// <returns></returns>
        public List<ConfigProperty> GetConfigProperties(Environments environments, ProjectTypes projectTypes, Warehouses whs, string key = null)
        {
            try
            {
                var environmentList = MaskToList<Environments>(environments).ToList();
                var projectTypeList = MaskToList<ProjectTypes>(projectTypes).ToList();
                var warehouseList = MaskToList<Warehouses>(whs).ToList();

                List<ConfigProperty> configPropertiesToReturn = new List<ConfigProperty>();

                if (whs == Warehouses.None)
                {
                    if (key==null)
                    {
                        var properties = _context.ConfigProperties
                            .Where(p => environmentList.Contains(p.Environment)
                                && ((projectTypeList.Contains(p.ProjectType.Value)) || !p.ProjectType.HasValue)
                                && (!p.Warehouse.HasValue))
                            .ToList();
                        configPropertiesToReturn = properties;
                    }
                    else
                    {
                        var properties = _context.ConfigProperties
                            .Where(p => environmentList.Contains(p.Environment)
                                && ((projectTypeList.Contains(p.ProjectType.Value)) || !p.ProjectType.HasValue)
                                && (!p.Warehouse.HasValue) && p.PropertyName==key)
                            .ToList();
                        configPropertiesToReturn = properties;
                    }
                }
                else
                {
                    if (key == null)
                    {
                        var properties = _context.ConfigProperties
                            .Where(p => environmentList.Contains(p.Environment)
                                && ((projectTypeList.Contains(p.ProjectType.Value)) || !p.ProjectType.HasValue)
                                && ((warehouseList.Contains(p.Warehouse.Value)) || !p.Warehouse.HasValue))
                            .ToList();
                        configPropertiesToReturn = properties;
                    }
                    else
                    {
                        var properties = _context.ConfigProperties
                            .Where(p => environmentList.Contains(p.Environment)
                                && ((projectTypeList.Contains(p.ProjectType.Value)) || !p.ProjectType.HasValue)
                                && ((warehouseList.Contains(p.Warehouse.Value)) || !p.Warehouse.HasValue) && p.PropertyName == key)
                            .ToList();
                        configPropertiesToReturn = properties;
                    }
                }

                if(configPropertiesToReturn != null && configPropertiesToReturn.Any())
                {
                    if (configPropertiesToReturn.Where(x => x.PropertyValue == "%!CONFIG_REF!%").Any())
                    {
                        List<ConfigProperty> configPropertiesToAdd = new List<ConfigProperty>();
                        foreach (var config in configPropertiesToReturn.Where(x => x.PropertyValue == "%!CONFIG_REF!%"))
                        {
                            ConfigProperty prop = _context.ConfigProperties
                                .SingleOrDefault(p => config.ReferenceProjectType != null 
                                    && p.ProjectType == config.ReferenceProjectType
                                    && p.PropertyName == config.PropertyName);

                            if(prop != null)
                                configPropertiesToAdd.Add(prop);
                        }

                        configPropertiesToReturn.AddRange(configPropertiesToAdd);
                    }
                }

                return configPropertiesToReturn;
            }
            catch (Exception exception)
            {
                var data = new LoggingData
                {
                    Env = environments,
                    Project = projectTypes,
                    Exception = exception,
                    Message = "Unable to retrieve the config properties."
                };
                Logger.Log(data);
                throw;
            }
        }

        internal string GetSimpleConfigProperties(Environments env, ProjectTypes currentProj,  string propertyName)
        {
            try
            {
                //var configuration = _context.ConfigProperties.Where(p => p.Environment.HasFlag(env) && p.ProjectType.Value == currentProj && p.PropertyName.Equals(propertyName)).ToList();
                //return configuration;
                var configuration = _context.ConfigProperties.Where(p => p.Environment.HasFlag(env) && p.ProjectType.Value == currentProj && p.PropertyName.Equals(propertyName)).FirstOrDefault();
                //List<string> configurations = new List<string>();
                //configurations.Add(configuration.PropertyValue);
                return configuration.PropertyValue;
                
            }
            catch (Exception exception)
            {
                var data = new LoggingData
                {
                    Env = env,
                    Exception = exception,
                    Message = "Unable to retrieve the config properties. "
                };
                Logger.Log(data);
                throw;
            }
        }

        internal Deploy.ServiceCredentials GetCredentials(Environments env, ProjectTypes currentProj, Warehouses currentWh)
        {
            try
            {
                var properties = _context.ConfigProperties.Where(p => p.Environment.HasFlag(env) && p.ProjectType.Value == currentProj && (p.PropertyName == "ServiceName" || p.PropertyName == "UserName" || p.PropertyName == "Password" || p.PropertyName == "ServiceExeName")).ToList();
               
                var returnValue = new Deploy.ServiceCredentials {   Password = properties.Where(p => p.PropertyName == "Password").Select(p => p.PropertyValue).FirstOrDefault(),
                                                                    UserName = (properties.Where(p => p.PropertyName == "UserName").Select(p => p.PropertyValue).FirstOrDefault())?? null,
                                                                    ServiceName = (properties.Where(p => p.PropertyName == "ServiceName").Select(p => p.PropertyValue).FirstOrDefault())?? null,
                                                                    ServiceExeName = properties.Where(p => p.PropertyName == "ServiceExeName").Select(p => p.PropertyValue).FirstOrDefault(),
                                                                    StockOwnerCode = (properties.Where(p => !string.IsNullOrWhiteSpace(p.StockOwnerCode)).Select(p => p.StockOwnerCode).FirstOrDefault()) ?? null,
                };
                
                return returnValue;
            }
            catch (Exception exception)
            {
                var data = new LoggingData
                {
                    Env = env,
                    Wh = currentWh,
                    Project = currentProj,
                    Exception = exception,
                    Message = "Unable to retrieve the config properties. "
                };
                Logger.Log(data);
                throw;
            }
        }

        internal List<string> GetStockOwners(Environments env, ProjectTypes currentProj, Warehouses whs)
        {
            var stockOwnerCodes = _context.ConfigProperties
                    .Where(p => p.Environment.HasFlag(env))
                    .Where(p => p.ProjectType.Value == currentProj)
                    .Where(p => p.StockOwnerCode != null)
                    .Where(p => whs == Warehouses.None || (p.Warehouse != null && p.Warehouse.Value.HasFlag(whs)))
                    .Select(p => p.StockOwnerCode)
                    .Distinct()
                    .ToList();
            return stockOwnerCodes;
        }

        internal List<string> GetServers(Environments env, ProjectTypes currentProj, Warehouses whs)
        {
            try
            {
                //first try for warehouse
                var properties = _context.ConfigProperties
                    .Where(p => p.Environment.HasFlag(env))
                    .Where(p => p.ProjectType.Value == currentProj)
                    .Where(p => p.PropertyName.Equals("Server"))
                    .Where(p => p.Warehouse != null && p.Warehouse.Value.HasFlag(whs))
                    .ToList();

                //if non found, then try with warehouse null
                if (!properties.Any())
                {
                    properties = _context.ConfigProperties
                    .Where(p => p.Environment.HasFlag(env))
                    .Where(p => p.ProjectType.Value == currentProj)
                    .Where(p => p.PropertyName.Equals("Server"))
                    .Where(p => p.Warehouse == null)
                    .ToList();
                }
                List<string> returnProperties = new List<string>();

                foreach(var propertie in properties)
                {
                    if(env.HasFlag( Environments.PRD) && currentProj.HasFlag(ProjectTypes.DocGenService))
                    {
                        if(whs.HasFlag(propertie.Warehouse))
                            returnProperties.Add(propertie.PropertyValue);
                    }
                    else if (propertie.PropertyValue.Contains(";"))
                    {
                        var splitValue = propertie.PropertyValue.Split(';');
                        foreach (var split in splitValue)
                            returnProperties.Add(split);
                    }
                    else
                        returnProperties.Add(propertie.PropertyValue);                  
                }
                return returnProperties;
            }
            catch (Exception exception)
            {
                var data = new LoggingData
                {
                    Env = env,
                    Exception = exception,
                    Message = "Unable to retrieve the config properties. "
                };
                Logger.Log(data);
                throw;
            }
        }

        internal List<ConfigProperty> GetConfigProperties(Environments environments, string key)
        {
            return GetConfigProperties(environments, new List<string> { key });
        }

        internal List<ConfigProperty> GetConfigProperties(Environments environments, IEnumerable<string> keys)
        {
            try
            {
                var environmentList = MaskToList<Environments>(environments).ToList();

                var properties = _context.ConfigProperties
                    .Where(p => environmentList.Contains(p.Environment)
                        && keys.Contains(p.PropertyName))
                    .ToList();

                return properties;
            }
            catch (Exception exception)
            {
                var data = new LoggingData
                {
                    Env = environments,
                    Exception = exception,
                    Message = "Unable to retrieve the config properties. " + string.Join(", ", keys)
                };
                Logger.Log(data);
                throw;
            }
        }

        public List<ConfigProperty> GetConfigProperties(Environments environments, Warehouses wh, string Key)
        {
            try
            {
                var environmentList = MaskToList<Environments>(environments).ToList();
                var warehouseList = MaskToList<Warehouses>(wh).ToList();

                var properties = _context.ConfigProperties
                    .Where(p => environmentList.Contains(p.Environment)
                        && warehouseList.Contains(p.Warehouse.Value)
                        && p.PropertyName == Key)
                    .ToList();

                return properties;
            }
            catch (Exception exception)
            {
                var data = new LoggingData
                {
                    Env = environments,
                    Wh = wh,
                    Exception = exception,
                    Message = "Unable to retrieve the config properties. " + Key
                };
                Logger.Log(data);
                throw;
            }
        }



        public List<ConfigProperty_ORG> GetConfigProperties_org(Environments environments, ProjectTypes projectTypes, Warehouses warehouses)
        {
            try
            {
                var environmentList = Enum.GetValues(typeof(Environments)).Cast<Environments>().Where(e => environments.HasFlag(e)).ToList();
                var projectTypeList = Enum.GetValues(typeof(ProjectTypes)).Cast<ProjectTypes>().Where(e => projectTypes.HasFlag(e)).ToList();
                var warehouseList = Enum.GetValues(typeof(Warehouses)).Cast<Warehouses>().Where(e => warehouses.HasFlag(e)).ToList();

                var properties = _context.ConfigProperties_org
                    .Where(p => environmentList.Contains(p.Environment)
                        && projectTypeList.Contains(p.ProjectType)
                        && warehouseList.Contains(p.Warehouse))
                    .ToList();

                return properties;
            }
            catch (Exception exception)
            {
                var data = new LoggingData
                {
                    Env = environments,
                    Project = projectTypes,
                    Wh = warehouses,
                    Exception = exception,
                    Message = "Unable to retrieve the config properties."
                };
                Logger.Log(data);
                throw;
            }
        }

        private static IEnumerable<T> MaskToList<T>(Enum mask)
        {
            if (typeof(T).IsSubclassOf(typeof(Enum)) == false)
                throw new ArgumentException();

            return Enum.GetValues(typeof(T))
                                 .Cast<Enum>()
                                 .Where(m => mask.HasFlag(m))
                                 .Cast<T>().ToList();
        }

    }
}
