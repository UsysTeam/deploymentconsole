﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DeployConsole.Config;
using DeployConsole.Helpers;
using DeployConsole.Logging;
using DeployConsole.Managers;

namespace DeployConsole
{
    public class SpinUpPool
    {
        public static void SpinUp(Environments env, ProjectTypes pjs, Warehouses whs)
        {
            var urls = GetUrlsToSpinUp(env, pjs, whs);
            if (urls.Any())
                DoWebRequest(env, pjs, whs, urls).Wait();//Task.Factory.StartNew(async () => { await DoWebRequest(env, pjs, whs, urls); });
        }

        private static List<string> GetUrlsToSpinUp(Environments env, ProjectTypes pjs, Warehouses whs)
        {
            var keysToGet = new List<string> {"SpinUpPoolAdress"};
            var configPropertyManager = new ConfigPropertyManager();
            var configProperties = configPropertyManager.GetSimpleConfigProperties(env, keysToGet);
            var urls = new List<string>();

            var lockObject = new object();
            var allowedProjects = new List<ProjectTypes>
            {
                ProjectTypes.CommandApi,
                ProjectTypes.WCFGeneral,
                ProjectTypes.WCFWarehouse,
                ProjectTypes.MVC
            };

            Parallel.ForEach(EnumHelper.MaskToList<Environments>(env), currentEnv =>
            {
                Parallel.ForEach(EnumHelper.MaskToList<ProjectTypes>(pjs), currentPj =>
                {
                    //only do this for command, general and warehouse
                    if (!allowedProjects.Contains(currentPj))
                        return;

                    var firstWh = EnumHelper.MaskToList<Warehouses>(whs).First(wh => wh != Warehouses.None);

                    Parallel.ForEach(EnumHelper.MaskToList<Warehouses>(whs).Where(wh => wh != Warehouses.None), currentWh =>
                    {
                        var configPropertiesForThisEnvWhAndProj =
                            configPropertyManager.GetConfigPropertyByPriorityAndEnrich(configProperties, currentEnv, currentPj,
                                currentWh);

                        //if general, only do it once
                        if (currentPj == ProjectTypes.WCFGeneral && currentWh != firstWh)
                            return;

                        var logGroup = new Guid().ToString();
                        Logger.Log(new LoggingData
                        {
                            Env = currentEnv,
                            Exception = null,
                            Group = logGroup,
                            Wh = currentWh,
                            Project = pjs,
                            Message = $"Trying to spin up pools for {currentEnv} and wh {currentWh} and proj {currentPj}"
                        });

                        if (configPropertiesForThisEnvWhAndProj != null && configPropertiesForThisEnvWhAndProj.Any())
                        {
                            var values = configPropertiesForThisEnvWhAndProj
                                .Where(c => !string.IsNullOrWhiteSpace(c.PropertyValue))
                                .Select(c => c.PropertyValue)
                                .ToList();

                            foreach (var value in values)
                            {
                                lock (lockObject)
                                {
                                    urls.AddRange(value.Split(';'));
                                }
                            }
                        }
                    });
                });
            });
            return urls.Distinct().ToList();
        }

        private static async Task DoWebRequest(Environments env, ProjectTypes pjs, Warehouses whs, IReadOnlyCollection<string> urls)
        {
            Logger.Log(new LoggingData
            {
                Env = env,
                Exception = null,
                Group = "",
                Wh = whs,
                Project = pjs,
                Message = $"Calling {urls.Count}# urls for starting up the pools"
            });
            //foreach (var url in urls)
            var tasks = Parallel.ForEach(urls, url =>
            {
                try
                {
                    Console.WriteLine($"Calling url: {url}");
                    var req = WebRequest.CreateHttp(url);
                    req.Timeout = 60000; //1min
                    req.GetResponse();
                }
                catch (Exception e)
                {
                    if ((e is WebException))
                    {
                        if (string.Compare(e.Message, "The operation has timed out", StringComparison.InvariantCultureIgnoreCase) == 0)
                            return;//continue;
                        if (string.Compare(e.Message, "The remote server returned an error: (500) Internal Server Error.", StringComparison.InvariantCultureIgnoreCase) == 0)
                            return;//continue;
                    }
                    Console.WriteLine($"Calling url: {url} failed with exception: {e}");
                    Logger.Log(new LoggingData
                    {
                        Env = env,
                        Exception = null, //ignore the exception, just log it as a warning
                        Group = "",
                        Wh = whs,
                        Project = pjs,
                        Message = $"Calling url {url} for starting up the pools failed with exception: {e}"
                    });
                }
            //}
            });
            Console.WriteLine($"Calling {urls.Count}# urls for starting up the pools finished {(tasks.IsCompleted ? "successfull" : "unsucessfull")}");
            Logger.Log(new LoggingData
            {
                Env = env,
                Exception = null,
                Group = "",
                Wh = whs,
                Project = pjs,
                Message = $"Calling {urls.Count}# urls for starting up the pools finished { (tasks.IsCompleted ? "successfull" : "unsucessfull")}"
            });
        }
    }
}
