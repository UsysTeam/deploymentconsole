﻿using System;
using DeployConsole.Config;
using DeployConsole.Logging;

namespace DeployConsole.Helpers
{
    class ExecutionHelper
    {
        public static void DoSomething(Action action, string actionMsg, string extraMessage, Environments? env, ProjectTypes? currentProj, Warehouses? currentWh, string group, int? orderNr, bool treatErrorAsWarning = false)
        {
            try
            {
                Logger.Log(new LoggingData
                {
                    Env = env,
                    Message = $"Begin {actionMsg}",
                    ExtraMessage = extraMessage,
                    Project = currentProj,
                    Wh = currentWh == Warehouses.None ? null : currentWh,
                    Group = group,
                    OrderNr = orderNr
                });
                Console.WriteLine("Begin {0}\n\t{1}", actionMsg, extraMessage);
                action();
                Logger.Log(new LoggingData
                {
                    Env = env,
                    Message = $"End   {actionMsg}",
                    Project = currentProj,
                    Wh = currentWh == Warehouses.None ? null : currentWh,
                    Group = group,
                    OrderNr = orderNr
                });

            }
            catch (Exception e)
            {
                Logger.Log(new LoggingData
                {
                    Env = env,
                    Exception = treatErrorAsWarning ? null : e,
                    Message = treatErrorAsWarning ? $"WARNING {actionMsg}" : $"FAILED {actionMsg}",
                    Project = currentProj,
                    Wh = currentWh == Warehouses.None ? null : currentWh,
                    Group = group,
                    OrderNr = orderNr
                });
                Console.WriteLine(treatErrorAsWarning ? $"WARNING {actionMsg}" : $"FAILED {actionMsg}");
            }
        }
        public static T DoSomething<T>(Func<T> action, string actionMsg, string extraMessage, Environments? env, ProjectTypes? currentProj, Warehouses? currentWh, string group, int? orderNr,bool treatErrorAsWarning=false)
        {
            var loggerId = Guid.NewGuid().ToString();
            try
            {
                Logger.Log(new LoggingData
                {
                    Env = env,
                    Message = $"Begin {actionMsg}",
                    ExtraMessage = extraMessage,
                    Project = currentProj,
                    Wh = currentWh,
                    Group = loggerId,
                    OrderNr=orderNr
                });
                var res = action();
                Logger.Log(new LoggingData
                {
                    Env = env,
                    Message = $"End {actionMsg} return code: {res}",
                    Project = currentProj,
                    Wh = currentWh,
                    Group = loggerId,
                    OrderNr = orderNr
                });
                return res;
            }
            catch (Exception e)
            {
                Logger.Log(new LoggingData
                {
                    Env = env,
                    Exception = treatErrorAsWarning?null:e,
                    Message = treatErrorAsWarning? $"WARNING {actionMsg}" : $"FAILED {actionMsg}",
                    Project = currentProj,
                    Wh = currentWh,
                    Group = loggerId,
                    OrderNr = orderNr
                });
                return default(T);
            }
        }
       
    }
}
