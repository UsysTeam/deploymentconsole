﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SHDocVw;

namespace DeployConsole.Helpers
{
    class FsHelper
    {
        public static void ClearDirectory(string dirName)
        {
            var di = new DirectoryInfo(dirName);
            if (di.Exists)
            {
                var dirs = di.GetDirectories();
                Parallel.ForEach(dirs, dir =>
                //foreach (var dir in di.GetDirectories())
                {
                    var tel = 0;

                    while (true)
                    {
                        try
                        {
                            try
                            {
                                CloseExplorerProcessToDir(dirName);
                            }
                            catch
                            {

                            }
                            dir.Delete(true);
                            break;
                        }
                        catch (Exception)
                        {
                            tel++;
                            if (tel > 2)
                                throw;
                            Thread.Sleep(500);
                        }
                    }
                });
            }
        }

        public static void DeleteDirectory(string dirName)
        {
            var di = new DirectoryInfo(dirName);
            if (di.Exists)
                di.Delete(true);
        }
        public static void RenameDirectory(string org,string newName)
        {
            var di = new DirectoryInfo(org);
            if (di.Exists)
                di.MoveTo(newName);
        }

        private static void CloseExplorerProcessToDir(string dirName)
        {
            //http://stackoverflow.com/questions/13463639/is-there-a-way-to-close-a-particular-instance-of-explorer-with-c
            

            try
            {
                var shellWindows = new ShellWindows();
                foreach (var ie in from InternetExplorer ie in shellWindows let processType = Path.GetFileNameWithoutExtension(ie.FullName).ToLower() where processType.Equals("explorer") && ie.LocationURL.Contains(dirName.Replace(@"\", "/")) select ie)
                {
                    ie.Quit();
                    Thread.Sleep(500);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static bool DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {


            // Get the subdirectories for the specified directory.
            var dir = new DirectoryInfo(sourceDirName);
            var dirs = dir.GetDirectories();

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            
            if (Directory.Exists(destDirName))
            {

                var downloadedMessageInfo = new DirectoryInfo(destDirName);
                foreach (var file in downloadedMessageInfo.GetFiles())
                {
                    file.Delete();
                }

                foreach (var subdir in downloadedMessageInfo.GetDirectories())
                    subdir.Delete(true);
            }

            // If the destination directory doesn't exist, create it. 
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            var files = dir.GetFiles();
            foreach (var file in files)
            {
                var temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location. 
            if (!copySubDirs) return true;
            foreach (var subdir in dirs)
            {
                var temppath = Path.Combine(destDirName, subdir.Name);
                DirectoryCopy(subdir.FullName, temppath, true);
            }
            return true;
        }
    }
}
