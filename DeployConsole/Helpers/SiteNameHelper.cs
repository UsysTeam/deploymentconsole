﻿using DeployConsole.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeployConsole.Helpers
{
    public class SiteNameHelper
    {
        public static string GenerateSiteName(string siteNameConfig, Warehouses whs, Environments env, ProjectTypes currentProj)
        {
            var returnValue = string.Empty;
            switch (currentProj)
            {
                case ProjectTypes.DocGenService:
                    break;
                case ProjectTypes.MVC:
                    return string.Format(siteNameConfig, whs, env);
                    break;
                case ProjectTypes.PartyApi:
                    return string.Format(siteNameConfig, whs, env);
                    break;
                case ProjectTypes.PrintService:
                    return string.Format(siteNameConfig, whs, env);
                    break;
                case ProjectTypes.ScannerManagementService:
                    return string.Format(siteNameConfig, whs, env);
                    break;
                case ProjectTypes.uProcessMonitor:
                    return string.Format(siteNameConfig, env);
                    break;
                case ProjectTypes.WCFGeneral:
                    return string.Format(siteNameConfig, env);
                    break;
                case ProjectTypes.WCFWarehouse:
                    return string.Format(siteNameConfig, whs, env);
                    break;
                case ProjectTypes.CommandApi:
                    return string.Format(siteNameConfig, whs, env);
                    break;
                case ProjectTypes.BFApi:
                    return string.Format(siteNameConfig, whs, env);
                    break;
                case ProjectTypes.InventoryApi   :
                    return string.Format(siteNameConfig, whs, env);
                    break;
                case ProjectTypes.ApiWcfApi:
                    return string.Format(siteNameConfig, whs, env);
                    break;
                case ProjectTypes.BFJobWebservice:
                    return string.Format(siteNameConfig,  env);
                    break;
                default:
                    throw new Exception("'GetSiteNames' for not found 1");
            }
            return string.Empty;
        }
    }
}
