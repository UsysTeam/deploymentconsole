﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Microsoft.Web.Administration;

namespace DeployConsole.Helpers
{
    class IisHelper : IDisposable
    {
        readonly ServerManager _manager;
        private readonly string _serverName;
        private readonly object _progressLock = new Object();
        private IisHelper(string iporDns)
        {
            _serverName = iporDns;
            _manager = ServerManager.OpenRemote(_serverName);

        }

        private static readonly Dictionary<string, IisHelper> IisHelpers = new Dictionary<string, IisHelper>();

        public static IisHelper GetIisHelper(string iporDns)
        {
            lock (IisHelpers)
            {
                if (IisHelpers.ContainsKey(iporDns))
                    return IisHelpers[iporDns];
            }

            lock (IisHelpers)
            {
                if (!IisHelpers.ContainsKey(iporDns))
                    IisHelpers.Add(iporDns, new IisHelper(iporDns));
            }
            lock (IisHelpers)
            {
                return IisHelpers[iporDns];
            }
        }

        public static void CommitAndCloseAllIisHelpers()
        {
            foreach (var iisHelper in IisHelpers)
            {
                iisHelper.Value.CommitAndClose();
            }
        }

        public void UpdatePhysicalPathSite(List<string> siteName, string newPath)
        {
            foreach (var virtualRoot in siteName.Select(s => _manager.Sites[s]).Select(thisSite => thisSite.Applications.FirstOrDefault().VirtualDirectories.FirstOrDefault()))
            {
                virtualRoot.PhysicalPath = @newPath;
            }
        }

        public void UpdatePhysicalPathVd(List<string> siteNames, string newPath)
        {
            var defSite = _manager.Sites.FirstOrDefault(s => s.Name == "Default Web Site");
            if (defSite == null) throw new Exception("Cannot find 'Default Web Site' on machine: " + _serverName);
            var myErr = new List<Exception>();
            try
            {
                lock (_progressLock)
                {
                    foreach (var sName in siteNames)
                    {
                        var name = sName;//Bypass Resharper AccessToForEachVariableInClosure
                        var thisApp = defSite.Applications.FirstOrDefault(aApp => aApp.Path == "/" + name);

                        if (thisApp == null)
                            myErr.Add(new Exception("Cannot find virtual Directory 'Default Web Site/" + sName + "' on machine: " + _serverName));
                        else
                            thisApp.VirtualDirectories.FirstOrDefault().PhysicalPath = newPath;
                    }
                }
            }
            catch (Exception e)
            {
                myErr.Add(e);
            }
            if (myErr != null && myErr.Count > 0) throw new AggregateException(myErr);
        }


        public void SetApplicationPoolStateForSite(List<string> siteName, ObjectState desiredObjectState)
        {
            foreach (var thisSite in siteName.Select(s => _manager.Sites[s]))
            {
                var poolName = thisSite.Applications.FirstOrDefault().ApplicationPoolName;
                switch (desiredObjectState)
                {
                    case ObjectState.Started:
                        if (_manager.ApplicationPools[poolName].State == ObjectState.Stopped)
                            _manager.ApplicationPools[poolName].Start();
                        else
                            try
                            {
                                _manager.ApplicationPools[poolName].Start();
                            }
                            catch (Exception)
                            {

                            }
                        //wait for it
                        while (_manager.ApplicationPools[poolName].State != desiredObjectState)
                            Thread.Sleep(1000);
                        break;
                    case ObjectState.Stopped:
                        if (_manager.ApplicationPools[poolName].State == ObjectState.Started)
                            _manager.ApplicationPools[poolName].Stop();
                        else
                            try
                            {
                                _manager.ApplicationPools[poolName].Stop();
                            }
                            catch (Exception)
                            {

                            }

                        break;

                    default:
                        throw new Exception(
                            $"No implementation for this objectstate : {desiredObjectState} Server: {_serverName} Site: {thisSite}");
                }
                //wait for it
                var teller = 0;
                while (_manager.ApplicationPools[poolName].State != desiredObjectState && teller <= 10)
                {
                    Thread.Sleep(1000);
                    teller += 1;
                }
                if (teller > 10)
                    Console.WriteLine("Unable to set pool in desiredState '{2}' for Site: {0} State: {1}", thisSite, _manager.ApplicationPools[poolName].State, desiredObjectState);
                else
                    Console.WriteLine("Site: {0} State: {1}", thisSite, _manager.ApplicationPools[poolName].State);
            }
        }


        public void SetApplicationPoolStateForVd(List<string> siteNames, ObjectState desiredObjectState)
        {
            var defSite = _manager.Sites.FirstOrDefault(s => s.Name == "Default Web Site");
            if (defSite == null) throw new Exception("Cannot find 'Default Web Site' on machine: " + _serverName);
            var myErr = new List<Exception>();
            try
            {
                foreach (var sName in siteNames)
                {
                    var name = sName;//Bypass Resharper AccessToForEachVariableInClosure
                    var thisApp = defSite.Applications.FirstOrDefault(aApp => aApp.Path == "/" + name);

                    if (thisApp == null)
                        myErr.Add(
                            new Exception("Cannot find virtual Directory 'Default Web Site/" + sName + "' on machine: " +
                                          _serverName));
                    else
                    {
                        var poolName = thisApp.ApplicationPoolName;

                        switch (desiredObjectState)
                        {
                            case ObjectState.Started:
                                if (_manager.ApplicationPools[poolName].State == ObjectState.Stopped)
                                    _manager.ApplicationPools[poolName].Start();
                                else
                                    try
                                    {
                                        _manager.ApplicationPools[poolName].Start();
                                    }
                                    catch (Exception)
                                    {

                                    }
                                break;
                            case ObjectState.Stopped:
                                if (_manager.ApplicationPools[poolName].State == ObjectState.Started)
                                    _manager.ApplicationPools[poolName].Stop();
                                else
                                    try
                                    {
                                        _manager.ApplicationPools[poolName].Stop();
                                    }
                                    catch (Exception)
                                    {

                                    }
                                break;
                            default:
                                throw new Exception(
                                    $"No implementation for this objectstate : {desiredObjectState} Server: {_serverName} Site: {sName}");

                        }
                        //wait for it
                        var teller = 0;
                        while (_manager.ApplicationPools[poolName].State != desiredObjectState && teller <= 10)
                        {
                            Thread.Sleep(1000);
                            teller += 1;
                        }
                        if (teller > 10)
                            Console.WriteLine("Unable to set pool in desiredState '{2}' for Site: {0} State: {1}", thisApp, _manager.ApplicationPools[poolName].State, desiredObjectState);
                    }
                }
            }
            catch (Exception e)
            {
                myErr.Add(e);
            }

            if (myErr != null && myErr.Count > 0) throw new AggregateException(myErr);
        }

        private void CommitAndClose()
        {
            _manager.CommitChanges();
            Dispose();
        }

        public void Dispose()
        {
            if (_manager != null) _manager.Dispose();
        }
    }
}
