﻿using System;
using System.ServiceProcess;
using System.Threading;

namespace DeployConsole.Helpers
{
    class ServiceHelper:IDisposable

    {
        private readonly ServiceController _sc;
        public ServiceHelper(string serviceName, string ip)
        {
            _sc = new ServiceController(serviceName, ip);
        }

        public bool StartTimeout()
        {
            if (_sc.Status != ServiceControllerStatus.Stopped) return false;
            _sc.Start();
            _sc.WaitForStatus(ServiceControllerStatus.Running, TimeSpan.FromMinutes(2));
            if (_sc.Status==ServiceControllerStatus.Running)
                return true;
            return false;
        }
        public void Start()
        {
            if (_sc.Status != ServiceControllerStatus.Stopped) return;
            _sc.Start();
            _sc.WaitForStatus(ServiceControllerStatus.Running);
        }

        public void Stop()
        {
            if (_sc.Status != ServiceControllerStatus.Running) return;
            _sc.Stop();
        
            _sc.WaitForStatus(ServiceControllerStatus.Stopped, TimeSpan.FromMinutes(2));
            Thread.Sleep(5000);
        }

        public ServiceControllerStatus GetStatus()
        {
            return _sc.Status;
        }
        public void Dispose()
        {
            if (_sc != null)
                _sc.Dispose();
        }
    }
}
