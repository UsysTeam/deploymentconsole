﻿using System;
using System.Management;
using System.Threading;

namespace DeployConsole.Helpers.RemoteExecute
{
    class ProcessWMI
    {
        public uint ProcessId;
        public int ExitCode;
        public bool EventArrived;
        public ManualResetEvent Mre = new ManualResetEvent(false);
        public void ProcessStoptEventArrived(object sender, EventArrivedEventArgs e)
        {
            if ((uint)e.NewEvent.Properties["ProcessId"].Value == ProcessId)
            {
                Console.WriteLine("Process: {0}, Stopped with Code: {1}", (int)(uint)e.NewEvent.Properties["ProcessId"].Value, (int)(uint)e.NewEvent.Properties["ExitStatus"].Value);
                ExitCode = (int)(uint)e.NewEvent.Properties["ExitStatus"].Value;
                EventArrived = true;
                Mre.Set();
            }
        }
        public ProcessWMI()
        {
            ProcessId = 0;
            ExitCode = -1;
            EventArrived = false;
        }

        public int ExecuteRemoteProcessWMI(string remoteComputerName, string arguments, int waitTimePerCommand)
        {
            var strUserName = string.Empty;
            try
            {
                var connOptions = new ConnectionOptions
                {
                    Impersonation = ImpersonationLevel.Impersonate,
                    EnablePrivileges = true
                };
                var manScope = new ManagementScope($@"\\{remoteComputerName}\ROOT\CIMV2", connOptions);

                try
                {
                    manScope.Connect();
                }
                catch (Exception e)
                {
                    throw new Exception("Management Connect to remote machine " + remoteComputerName + " as user " + strUserName + " failed with the following error " + e.Message);
                }
                var objectGetOptions = new ObjectGetOptions();
                var managementPath = new ManagementPath("Win32_Process");
                using (var processClass = new ManagementClass(manScope, managementPath, objectGetOptions))
                {
                    using (var inParams = processClass.GetMethodParameters("Create"))
                    {
                        inParams["CommandLine"] = arguments;
                        using (var outParams = processClass.InvokeMethod("Create", inParams, null))
                        {

                            if ((uint)outParams["returnValue"] != 0)
                            {
                                throw new Exception("Error while starting process " + arguments + " creation returned an exit code of " + outParams["returnValue"] + ". It was launched as " + strUserName + " on " + remoteComputerName);
                            }
                            ProcessId = (uint)outParams["processId"];
                        }
                    }
                }

                var checkProcess = new SelectQuery("Select * from Win32_Process Where ProcessId = " + ProcessId);
                using (var processSearcher = new ManagementObjectSearcher(manScope, checkProcess))
                {
                    using (var moC = processSearcher.Get())
                    {
                        if (moC.Count == 0)
                        {
                            throw new Exception("ERROR AS WARNING: Process " + arguments + " terminated before it could be tracked on " + remoteComputerName);
                        }
                    }
                }

                var q = new WqlEventQuery("Win32_ProcessStopTrace");
                using (var w = new ManagementEventWatcher(manScope, q))
                {
                    w.EventArrived += ProcessStoptEventArrived;
                    w.Start();
                    if (!Mre.WaitOne(waitTimePerCommand, false))
                    {
                        w.Stop();
                        EventArrived = false;
                        Console.WriteLine("Process " + arguments + " was timed-out on " + remoteComputerName + " it took longer then " + waitTimePerCommand + " milliseconds.");
                    }
                    else
                    {
                        w.Stop();
                    }
                }
                if (!EventArrived)
                {
                    var sq = new SelectQuery("Select * from Win32_Process Where ProcessId = " + ProcessId);
                    using (var searcher = new ManagementObjectSearcher(manScope, sq))
                    {
                        foreach (var o in searcher.Get())
                        {
                            var queryObj = (ManagementObject) o;
                            queryObj.InvokeMethod("Terminate", null);
                            queryObj.Dispose();
                            throw new Exception("Process " + arguments + " timed out and was killed on " + remoteComputerName);
                        }
                    }
                }
                else
                {
                    if (ExitCode != 0)
                        throw new Exception("Process " + arguments + "exited with exit code " + ExitCode + " on " + remoteComputerName + " run as " + strUserName);
                    Console.WriteLine("process exited with Exit code 0");
                }
            }
            catch (Exception e)
            {
                throw new Exception(
                    $"Execute process failed Machinename {remoteComputerName}, ProcessName {arguments}, RunAs {strUserName}, Error is {e.Message}, Stack trace {e.StackTrace}", e);
            }
            return ExitCode;
        }
    }
}
