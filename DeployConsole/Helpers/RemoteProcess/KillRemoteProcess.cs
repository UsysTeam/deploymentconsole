using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using DeployConsole.Config;
using DeployConsole.Logging;

namespace DeployConsole.Helpers.RemoteProcess
{
    public class KillRemoteProcess
    {
        public static void KillProcess(Environments env, ProjectTypes currentProj, int justOneProcessesId, Warehouses currentWh, string server)
        {
            var pids = $"processid = '{justOneProcessesId}'";
            if (pids.Length <= 0) return;
            var scope = new ManagementScope($@"\\{server}\root\cimv2");
            scope.Connect();
            var query = new ObjectQuery("select * from win32_process where " + pids);
            var searcher = new ManagementObjectSearcher(scope, query);
            var objectcollection = searcher.Get();
            foreach (var o in objectcollection)
            {
                var managementObject = (ManagementObject) o;
                try
                {
                    managementObject.InvokeMethod("Terminate", null);
                }
                catch (Exception e)
                {
                    Logger.Log(new LoggingData
                    {
                        Env = env,
                        Exception = e,
                        Message =
                            $"Begin Kill FAILED for id '{managementObject}' server '{server}'",
                        Project = currentProj,
                        Wh = currentWh
                    });
                }
            }
        }

        public static void KillProcess(Environments env, ProjectTypes currentProj, List<int> pids, Warehouses currentWh, string server)
        {
            var queryPids = "";
            foreach (var pid in pids)
            {
                if (pids.IndexOf(pid) == 0)
                    queryPids = $"processid = '{pid}'";
                else
                    queryPids += $" or processid = '{pid}'";
            }

            if (queryPids.Length <= 0) return;
            var scope = new ManagementScope($@"\\{server}\root\cimv2");
            scope.Connect();
            var query = new ObjectQuery("select * from win32_process where (" + queryPids + ") and ExecutablePath like '%" + env + "%" + currentWh + "%'");
            var searcher = new ManagementObjectSearcher(scope, query);
            var objectcollection = searcher.Get();
            foreach (var managementObject in objectcollection.Cast<ManagementObject>())
            {
                try
                {
                    managementObject.InvokeMethod("Terminate", null);
                }
                catch (Exception e)
                {
                    Logger.Log(new LoggingData
                    {
                        Env = env,
                        Exception = e,
                        Message =
                            $"Begin Kill FAILED for id '{managementObject}' server '{server}'",
                        Project = currentProj,
                        Wh = currentWh
                    });
                }
            }
        }
    }
}