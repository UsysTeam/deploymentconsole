﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace DeployConsole.Helpers.RemoteProcess
{
    public class RemoteRegistryConfig
    {
        public static bool SetRemoteRegistryKey(string server, string registryKeyLocation, string registryKeyName, string newRegistryKeyValue)
        {
            bool registryKeyHasChanged = false;
            RegistryHive registryHive;
            var orignalRegistryKeyLocation = registryKeyLocation;
            var firstPartRegistryKey = registryKeyLocation.Substring(0, registryKeyLocation.IndexOf(@"\"));
            switch (firstPartRegistryKey.ToUpperInvariant())
            {
                case "HKEY_LOCAL_MACHINE":
                    registryHive = RegistryHive.LocalMachine;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(registryKeyLocation);
            }

            //delete the registryHive and first "/" from the regkey location
            registryKeyLocation = registryKeyLocation.Replace(firstPartRegistryKey, string.Empty).Substring(1);

            using (var environmentKey =
                        RegistryKey.OpenRemoteBaseKey(registryHive, server, RegistryView.Registry32).OpenSubKey(registryKeyLocation, true) ??
                        RegistryKey.OpenRemoteBaseKey(registryHive, server, RegistryView.Registry64).OpenSubKey(registryKeyLocation, true))
            {
                if (environmentKey == null)
                    throw new Exception($"No registry key '{orignalRegistryKeyLocation}' found on server '{server}'");

                var registryValue = environmentKey.GetValue(registryKeyName);
                if (string.IsNullOrWhiteSpace(registryValue?.ToString()))
                    throw new Exception($@"No registry keyname '{orignalRegistryKeyLocation}\{registryKeyName}' found on server '{ server }'");

                //change reg key to new value
                if (string.Compare(registryValue as string, newRegistryKeyValue, StringComparison.InvariantCultureIgnoreCase) != 0)
                {
                    environmentKey.SetValue(registryKeyName, newRegistryKeyValue);
                    registryKeyHasChanged = true;
                }
            }
            return registryKeyHasChanged;
        }
    }
}
