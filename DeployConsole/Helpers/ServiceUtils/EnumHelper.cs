using System;
using System.Collections.Generic;
using System.Linq;

namespace DeployConsole.Helpers
{
    class EnumHelper
    {
        public static IEnumerable<T> MaskToList<T>(Enum mask)
        {
            if (typeof(T).IsSubclassOf(typeof(Enum)) == false)
                throw new ArgumentException();

            return Enum.GetValues(typeof(T))
                .Cast<Enum>()
                .Where(mask.HasFlag)
                .Cast<T>();
        }
    }
}