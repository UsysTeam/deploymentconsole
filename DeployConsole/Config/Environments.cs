﻿ using System;

namespace DeployConsole.Config
{
    [Flags]
    public enum Environments
    {
        DEV=1,
        ACC=2,
        STA=4,
        TST=8,
        PRD=16,
        CPYPRD=32
    }
}
