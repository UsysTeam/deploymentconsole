﻿using System;

namespace DeployConsole.Config
{

    [Flags]
    public enum Warehouses
    {
        None = 0,
        PT01 = 1,
        BG01 = 2,
        PL01 = 4,
        PL02 = 8,
        PL03 = 16,
        FR01 = 32,
        BE11 = 64,
        PL04 = 128
    }
}
