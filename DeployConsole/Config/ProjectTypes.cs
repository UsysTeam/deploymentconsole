﻿using System;

namespace DeployConsole.Config
{
    [Flags]
    public enum ProjectTypes
    {
        //MVC=1,
        //WCF = 2,
        //ScannerManagementService = 4,
        //DocGenService = 8,
        //PrintService = 16,
        //ScannerConsole = 32
        MVC = 1,
        ScannerManagementService = 2,
        DocGenService = 4,
        PrintService = 8,
        ScannerConsole = 16,
        WCFGeneral=32,
        WCFWarehouse=64,
        uProcessMonitor=128,
        //PocService = 256,
        PartyApi = 512,
        CommandApi=1024,
        QueryDB=2048,
        BFApi=4096,
        BFJobExecutor= 8192,
        BFJobWebservice= 16384,
        //PocQueryDb= 32768,
        //PocCommandDb = 65536,
        InventoryApi = 131072,

        //CF Interfacing
        SFTPConnector = 262144, //Prep + Deploy works
        InboundApi = 524288, //TODO Prep + Deploy
        Handlers = 1048576, //TODO Prep + Deploy
        JobProcessorCF = 2097152, //TODO Prep + Deploy
        FileChecker = 4194304, //TODO Prep + Deploy
        InterfacingCFApi = 8388608,
        ApiWcfApi = 16777216,

        BfInterfacingDacPac = 33554432
    }
}
