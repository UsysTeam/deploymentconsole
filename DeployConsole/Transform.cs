﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using DeployConsole.Config;
using DeployConsole.Helpers;
using DeployConsole.Logging;
using DeployConsole.Managers;

namespace DeployConsole
{
    class Transform
    {
        public static void Transformer(Environments envToPrep, ProjectTypes projectToPrep, Warehouses whToPrep, string buildOutput)
        {
            //const string buildOutput = @"c:\output";
            const string prepLoc = @"d:\prep";
            FsHelper.ClearDirectory(prepLoc);
            try
            {
                Parallel.ForEach(EnumHelper.MaskToList<Environments>(envToPrep), currentEnv => Parallel.ForEach(EnumHelper.MaskToList<ProjectTypes>(projectToPrep), currentProj =>
                {
                    var inDir = Path.Combine(buildOutput, currentProj.ToString());
                    if (currentProj == ProjectTypes.WCFGeneral || currentProj == ProjectTypes.MVC || currentProj == ProjectTypes.uProcessMonitor ||
                        currentProj == ProjectTypes.CommandApi || currentProj == ProjectTypes.QueryDB)
                    {
                        //WH independant projects 
                        var logGroup = Guid.NewGuid().ToString();
                        var argsList = Parameters.GetAllArgumentList(currentEnv, currentProj);
                        inDir = Path.Combine(buildOutput, @"server\_PublishedWebsites\USys2.GeneralServices");
                        if (currentProj == ProjectTypes.MVC)
                            inDir = Path.Combine(buildOutput, @"Web");
                        if (currentProj == ProjectTypes.CommandApi)
                            inDir = Path.Combine(buildOutput, @"server\_PublishedWebsites\Usys.Server.CommandApi");
                        if (currentProj == ProjectTypes.QueryDB)
                            inDir = Path.Combine(buildOutput, @"QueryDB");
                        if (currentProj == ProjectTypes.uProcessMonitor)
                            inDir = Path.Combine(buildOutput, @"uProcessMonitor");
                        CopyAndTransform(prepLoc, currentEnv, currentProj, inDir, logGroup, argsList);
                    }
                    else if (currentProj == ProjectTypes.PartyApi || currentProj == ProjectTypes.BFJobExecutor || currentProj == ProjectTypes.BFJobWebservice ||
                             currentProj == ProjectTypes.SFTPConnector || currentProj == ProjectTypes.Handlers || currentProj == ProjectTypes.JobProcessorCF || 
                             currentProj == ProjectTypes.InboundApi || currentProj == ProjectTypes.FileChecker || currentProj == ProjectTypes.BfInterfacingDacPac)
                    {
                        Parallel.ForEach(EnumHelper.MaskToList<Warehouses>(whToPrep), currentWh =>
                        {
                            if (currentWh == Warehouses.BE11)
                            {
                                var logGroup = Guid.NewGuid().ToString();
                                var argsList = Parameters.GetAllArgumentList(currentEnv, currentProj);

                                var useWh = false;
                                switch (currentProj)
                                {
                                    case ProjectTypes.InterfacingCFApi:
                                        inDir = Path.Combine(buildOutput, @"server\_PublishedWebsites\Usys.Server.Interfacing.be11_cf.Api");
                                        break;
                                    case ProjectTypes.PartyApi: //without wh code???
                                        inDir = Path.Combine(buildOutput, @"server\_PublishedWebsites\Usys.Server.PartyWebApi");
                                        break;
                                    case ProjectTypes.BFJobExecutor: //without wh code???
                                        inDir = Path.Combine(buildOutput, @"PocJobExecutor");
                                        break;
                                    case ProjectTypes.BFJobWebservice: //without wh code???
                                        //TODO: cleanup after 5.5 is in release
                                        if (Directory.Exists(Path.Combine(buildOutput, @"Usys.JobExecutor.Webservice\_PublishedWebsites\Usys.JobExecutor.Webservice")))
                                            inDir = Path.Combine(buildOutput, @"Usys.JobExecutor.Webservice\_PublishedWebsites\Usys.JobExecutor.Webservice");
                                        else
                                            if (Directory.Exists(Path.Combine(buildOutput, @"Usys.JobExecutor.Webservice\_PublishedWebsites\Usys.JobExecutor.WebApi")))
                                                inDir = Path.Combine(buildOutput, @"Usys.JobExecutor.Webservice\_PublishedWebsites\Usys.JobExecutor.WebApi");
                                            else
                                                throw new Exception("Cannot find input for Jobexecutor.web in Transform.cs-Transformer");
                                        break;



                                        // BFJobWebservice 5.4.5
                                        // inDir = Path.Combine(buildOutput, @"Usys.JobExecutor.Webservice\_PublishedWebsites\Usys.JobExecutor.Webservice");
                                        //
                                        // BFJobWebservice 5.5+
                                        //inDir = Path.Combine(buildOutput, @"Usys.JobExecutor.Webservice\_PublishedWebsites\Usys.JobExecutor.WebApi");
                                        //break;
                                    case ProjectTypes.SFTPConnector:
                                        inDir = Path.Combine(buildOutput, @"Interfacing\ConnectorsSFTP");
                                        useWh = true;
                                        break;
                                    case ProjectTypes.Handlers:
                                        inDir = Path.Combine(buildOutput, @"Interfacing\Handlers");
                                        useWh = true;
                                        break;
                                    case ProjectTypes.JobProcessorCF:
                                        inDir = Path.Combine(buildOutput, @"Interfacing\JobSchedulerConsole");
                                        useWh = true;
                                        break;
                                    case ProjectTypes.InboundApi:
                                        inDir = Path.Combine(buildOutput, @"Interfacing\Usys.Interfacing.InboundApi");
                                        useWh = true;
                                        break;
                                    case ProjectTypes.FileChecker:
                                        inDir = Path.Combine(buildOutput, @"Interfacing\FileChecker");
                                        useWh = true;
                                        break;
                                    case ProjectTypes.BfInterfacingDacPac:
                                        inDir = Path.Combine(buildOutput, @"BfInterfacingDacPac");
                                        useWh = true;
                                        break;
                                }
                                CopyAndTransform(prepLoc, currentEnv, currentProj, inDir, logGroup, argsList, useWh ? currentWh : Warehouses.None);

                            }
                        });
                    }
                    else
                    {
                        Parallel.ForEach(EnumHelper.MaskToList<Warehouses>(whToPrep), currentWh =>
                        {
                            if (currentWh == Warehouses.None) return;
                            var logGroup = Guid.NewGuid().ToString();
                            var argsList = Parameters.GetAllArgumentList(currentEnv, currentProj, currentWh);
                            if (currentProj == ProjectTypes.WCFWarehouse)
                            {
                                inDir = Path.Combine(buildOutput, @"server\_PublishedWebsites\USys2.WarehouseServices");
                                CopyAndTransform(prepLoc, currentEnv, currentProj, inDir, logGroup, argsList, currentWh);
                            }
                            if (currentWh != Warehouses.BE11)
                            {
                                if (currentProj == ProjectTypes.BFApi || currentProj == ProjectTypes.InventoryApi || currentProj == ProjectTypes.ApiWcfApi)
                                    return;
                            }

                            if (currentProj == ProjectTypes.BFApi)
                                inDir = Path.Combine(buildOutput, @"server\_PublishedWebsites\Usys.Server.BenFreshWebApi");
                            if (currentProj == ProjectTypes.InventoryApi)
                                inDir = Path.Combine(buildOutput, @"server\_PublishedWebsites\Usys.Server.UnivegFilesWebApi");
                            if (currentProj == ProjectTypes.InterfacingCFApi)
                                inDir = Path.Combine(buildOutput, @"server\_PublishedWebsites\Usys.Server.Interfacing.be11_cf.Api");
                            if (currentProj == ProjectTypes.ApiWcfApi)
                                inDir = Path.Combine(buildOutput, @"ApiWcfApi");

                            CopyAndTransform(prepLoc, currentEnv, currentProj, inDir, logGroup, argsList, currentWh);
                        });
                    }
                }));
            }
            catch (AggregateException ae)
            {
                foreach (var aex in ae.InnerExceptions)
                {
                    var sb = new StringBuilder();
                    var exception = aex as AggregateException;
                    if (exception != null)
                    {
                        exception.Handle(x =>
                        {
                            Console.WriteLine(x.Message);
                            if (x.InnerException != null)
                            {
                                Console.WriteLine("\t" + x.InnerException.Message);
                                Console.WriteLine("\n\n\t" + x.InnerException.StackTrace);
                            }
                            Console.WriteLine(new string('-', 80));

                            sb.AppendLine(x.Message);
                            if (x.InnerException != null)
                            {
                                sb.AppendLine("\t" + x.InnerException.Message);
                                sb.AppendLine("\n\n\t" + x.InnerException.StackTrace);
                            }
                            sb.AppendLine(new string('-', 80));
                            return true;
                        });
                    }
                    else
                    {
                        Console.WriteLine(aex.Message);
                        sb.AppendLine(aex.Message);
                    }
                    Logger.Log(new LoggingData
                    {
                        Env = null,
                        Exception = aex,
                        Message = $"FAILED \n\n{sb}",
                        Project = null,
                        Wh = null
                    });
                }
            }

#if DEBUG
            try
            {
                Process.Start(prepLoc);
            }
            catch
            {

            }
#endif
        }

        private static void CopyAndTransform(string prepLoc, Environments currentEnv, ProjectTypes currentProj, string inDir, string logGroup, XsltArgumentList argsList, Warehouses wh = Warehouses.None)
        {
            var stockOwners = new ConfigPropertyManager().GetStockOwners(currentEnv, currentProj, wh);
            if (!stockOwners.Any())
            {
                stockOwners = new ConfigPropertyManager().GetStockOwners(currentEnv, currentProj, Warehouses.None);
                if (!stockOwners.Any())
                    stockOwners.Add("");
            }

            Parallel.ForEach(stockOwners, stockOwner =>
            {
                var outDir = Path.Combine(prepLoc, currentEnv.ToString(), currentProj.ToString());
                if (wh != Warehouses.None)
                    outDir = Path.Combine(outDir, wh.ToString());
                if (!string.IsNullOrWhiteSpace(stockOwner))
                    outDir = outDir + stockOwner;

                DoActualCopyAndTransForm(currentEnv, currentProj, inDir, logGroup, argsList, wh, outDir);

                if (wh == Warehouses.BE11)
                {
                    if (currentProj == ProjectTypes.Handlers || currentProj == ProjectTypes.JobProcessorCF)
                    {
                        var dirInfoInParent = new DirectoryInfo(inDir).Parent;

                        #region DacPacs Interfacing
                        var dbDirsIn = dirInfoInParent.GetDirectories("Database.*", SearchOption.TopDirectoryOnly);
                        Parallel.ForEach(dbDirsIn, dbDirIn =>
                        {
                            if (!dbDirIn.Name.Contains("Outbound"))
                            {
                                if (currentProj == ProjectTypes.Handlers && dbDirIn.Name.Contains("JobScheduler"))
                                    return;
                                if (currentProj == ProjectTypes.JobProcessorCF && !dbDirIn.Name.Contains("JobScheduler"))
                                    return;
                            }
                            var dbDirOut = Path.Combine(outDir, "DacPacs", dbDirIn.Name);
                            DoActualCopyAndTransForm(currentEnv, currentProj, dbDirIn.FullName, logGroup, argsList, wh, dbDirOut);
                        });
                        #endregion

                        #region JobProcessor special bin folder
                        if (currentProj == ProjectTypes.JobProcessorCF)
                        {
                            var jobDefinitionsDir = dirInfoInParent.GetDirectories("Usys.Interfacing.Outbound.JobDefinitions", SearchOption.TopDirectoryOnly).FirstOrDefault();
                            if (jobDefinitionsDir != null)
                            {
                                var binFolder = Path.Combine(outDir, "!BIN");
                                if (!Directory.Exists(binFolder))
                                    Directory.CreateDirectory(binFolder);
                                var dllsToCopy = jobDefinitionsDir.GetFiles("Usys.Interfacing.Outbound.JobDefinitions*.dll", SearchOption.TopDirectoryOnly);
                                Parallel.ForEach(dllsToCopy, dllToCopy =>
                                {
                                    File.Copy(dllToCopy.FullName, binFolder + @"\" + dllToCopy.Name);
                                });
                            }
                        }
                        #endregion
                    }
                }
            });
        }

        private static void DoActualCopyAndTransForm(Environments currentEnv, ProjectTypes currentProj, string inDir, string logGroup, XsltArgumentList argsList, Warehouses wh, string outDir)
        {
            ExecutionHelper.DoSomething(() => FsHelper.DirectoryCopy(inDir, outDir, true)
                , "DirectoryCopy", $"From: {inDir}\nTo: {outDir}", currentEnv, currentProj, wh, logGroup, 1);

            var di = new DirectoryInfo(outDir);
            var xsltFiles = di.GetFiles("*.xslt", SearchOption.AllDirectories);
            Parallel.ForEach(xsltFiles, fi =>
            //foreach (var fi in xsltFiles)
            {
                var fi1 = fi;    //Bypass Resharper AccessToForEachVariableInClosure
                ExecutionHelper.DoSomething(() => TransformFile(
                    Path.Combine(fi1.Directory.FullName, Path.GetFileNameWithoutExtension(fi1.FullName)), fi1.FullName, argsList, currentEnv),
                    "Transform " + fi.FullName, "", currentEnv, currentProj, wh, logGroup, 2);
            });
        }

        private static void TransformFile(string configFile, string transFile, XsltArgumentList argsList, Environments currentEnv)
        {
            try
            {
                var fi = new FileInfo(configFile);
                var replacedEnvironment = false;

                //change the environment for CPYPRD to CPY_PRD for dacpacs
                if (currentEnv.HasFlag(Environments.CPYPRD) && configFile.ToLowerInvariant().Contains("dacpacs".ToLowerInvariant()))
                {
                    var env = argsList.GetParam("Environment", string.Empty);
                    if (!string.IsNullOrWhiteSpace(env?.ToString()))
                    {
                        var environment = env as string;
                        if (string.Compare(environment, Environments.CPYPRD.ToString(), StringComparison.InvariantCultureIgnoreCase) == 0)
                        {
                            argsList.RemoveParam("Environment", string.Empty);
                            argsList.AddParam("Environment", string.Empty, "CPY_PRD");
                            replacedEnvironment = true;
                        }
                    }
                }

                var outfile = Path.Combine(fi.Directory.FullName, Path.GetFileNameWithoutExtension(configFile) + ".new");

                var xslt = new XslCompiledTransform();
                xslt.Load(transFile);

                var doc = new XPathDocument(configFile);
                var writer = XmlWriter.Create(outfile, xslt.OutputSettings);

                xslt.Transform(doc, argsList, writer);

                writer.Close();

                File.Delete(configFile);
                File.Move(outfile, configFile);
                File.Delete(transFile);

                //undo the change after the file is transformed
                if (replacedEnvironment)
                {
                    argsList.RemoveParam("Environment", string.Empty);
                    argsList.AddParam("Environment", string.Empty, Environments.CPYPRD.ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

    }
}
