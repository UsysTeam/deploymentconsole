﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using DeployConsole.Config;
using DeployConsole.Helpers;
using DeployConsole.Logging;
using DeployConsole.Managers;
using System.Diagnostics;

namespace DeployConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger.Log(new LoggingData
            {
                Env = null,
                Message = "Startup args: " + string.Join(" ", args),
                ExtraMessage = "",
                Project = null,
                Wh = null,
                Group = null,
                OrderNr = 0
            });
            var invokedVerb = "";
            object invokedVerbInstance = null;

            var options = new Options();


            //------------- This displays argument options when enterd wrong
            //if (!CommandLine.Parser.Default.ParseArguments(args, options, (verb, subOptions) =>
            //{
            //    invokedVerb = verb;
            //    invokedVerbInstance = subOptions;
            //}))
            //{
            //    Environment.Exit(CommandLine.Parser.DefaultExitCodeFail);
            //}


            //------------- This DOESN'T displays argument options when enterd wrong
            var myParser = new Parser(settings =>
            {
                settings.MutuallyExclusive = true;
                settings.CaseSensitive = false;
                settings.HelpWriter = Console.Out;
            });

            if (!myParser.ParseArguments(args, options, (verb, subOptions) =>
            {
                invokedVerb = verb;
                invokedVerbInstance = subOptions;
            }))
            {
                Logger.Log(new LoggingData
                {
                    Env = null,
                    Message = "Startup args not correct.",
                    ExtraMessage = "",
                    Project = null,
                    Wh = null,
                    Group = null,
                    Exception = new Exception("Startup args not correct."),
                    OrderNr = 0
                });
                Environment.Exit(Parser.DefaultExitCodeFail);
            }

            var cpyPrdNeedsToSpingup = false;

            switch (invokedVerb.ToLower())
            {
                case "prep":
                    var prepOptions = (PrepOptions)invokedVerbInstance;
                    LogActions(invokedVerb.ToLower(), (Environments)prepOptions.Env, (ProjectTypes)prepOptions.Projects, (Warehouses)prepOptions.Warehouses);
                    Transform.Transformer((Environments)prepOptions.Env, (ProjectTypes)prepOptions.Projects, (Warehouses)prepOptions.Warehouses, prepOptions.InputLocation);
                    break;
                case "deploy":
                    var deployOptions = (DeployOptions)invokedVerbInstance;
                    LogActions(invokedVerb.ToLower(), (Environments)deployOptions.Env, (ProjectTypes)deployOptions.Projects, (Warehouses)deployOptions.Warehouses, deployOptions.TakeDbBackup);
                    if (((Warehouses)deployOptions.Warehouses).HasFlag(Warehouses.BG01))
                        throw new Exception("You shall not pass!!! No deploy to BG01 is allowed!!");
                    if (((Environments)deployOptions.Env).HasFlag(Environments.PRD))
                        throw new Exception("You shall not pass!!! No deploy to PRD unless you know what you're doing!!");
                    if (deployOptions.TakeDbBackup)
                    {
                        Sql.CreateDbBackup((Environments)deployOptions.Env, (Warehouses)deployOptions.Warehouses);
                    }
                    DeployImp((Environments)deployOptions.Env, (ProjectTypes)deployOptions.Projects, (Warehouses)deployOptions.Warehouses);

                    foreach (var dacPac in Deploy.concurrentDictionary.Values)
                    {
                        Logger.Log(new LoggingData
                        {
                            Env = (Environments)deployOptions.Env,
                            Project = (ProjectTypes)deployOptions.Projects,
                            Wh = (Warehouses)deployOptions.Warehouses,
                            Message = $"The following dacpac was executed:{Environment.NewLine}{dacPac}"
                        });
                    }

                    //SpinUpPool.SpinUp((Environments)deployOptions.Env, (ProjectTypes)deployOptions.Projects, (Warehouses)deployOptions.Warehouses);
                    //if (((Environments)deployOptions.Env).HasFlag(Environments.PRD) && !Logger.HasError())
                    //{
                    //    Sql.CopyDbBackup((Environments)deployOptions.Env, (Warehouses)deployOptions.Warehouses);
                    //    if (!Logger.HasError())
                    //        Sql.RestoreBackupFromPrdToCpyPrd((Warehouses)deployOptions.Warehouses);
                    //    if (!Logger.HasError() && Directory.Exists(@"d:\prep\" + Environments.CPYPRD))
                    //        DeployImp(Environments.CPYPRD, (ProjectTypes)deployOptions.Projects, (Warehouses)deployOptions.Warehouses);

                    //    cpyPrdNeedsToSpingup = true;
                    //   // //SpinUpPool.SpinUp(Environments.CPYPRD, (ProjectTypes)deployOptions.Projects, (Warehouses)deployOptions.Warehouses);
                    //}

                    break;
                case "startstop":
                    var startStopOptions = (StartStopEnvironmentOptions)invokedVerbInstance;
                    LogActions(invokedVerb.ToLower(), (Environments)startStopOptions.Env, (ProjectTypes)startStopOptions.Projects, (int)startStopOptions.State, (Warehouses)startStopOptions.Warehouses);
                    if (((Environments)startStopOptions.Env).HasFlag(Environments.PRD))
                        throw new Exception("You shall not pass!!! No state changing to PRD unless you know what you're doing!!");
                    StartStopEnvironment.DeployStartStop((Environments)startStopOptions.Env, (ProjectTypes)startStopOptions.Projects, (Warehouses)startStopOptions.Warehouses, (int)startStopOptions.State);
                    break;

                case "spinup":
                    var spinupOption = (SpinUpOptions)invokedVerbInstance;
                    SpinUpPool.SpinUp((Environments)spinupOption.Env, (ProjectTypes)spinupOption.Projects, (Warehouses)spinupOption.Warehouses);
                    break;
                case "getversion":
                    var getVersionOptions = (GetVersionOptions)invokedVerbInstance;
                    LogActions(invokedVerb.ToLower(), (Environments)getVersionOptions.Env, (ProjectTypes)getVersionOptions.Projects, (Warehouses)getVersionOptions.Warehouses);
                    //GetVersion.GetVersions((Environments)getVersionOptions.Env, (ProjectTypes)getVersionOptions.Projects, (Warehouses)getVersionOptions.Warehouses);
                    break;
            }

            IisHelper.CommitAndCloseAllIisHelpers();

            var sb = new StringBuilder();
            sb.AppendLine(new string('=', 80));
            sb.AppendLine("   " + new string('-', 30) + " ORDERED LOG " + new string('-', 30));
            sb.AppendLine(new string('=', 80));

            Console.WriteLine(sb.ToString());
            Console.WriteLine(Logger.Printlog());
            Console.WriteLine("Done with " + invokedVerb.ToLower() + "! - Has Error: " + Logger.HasError());
            if (Logger.HasError())
                Console.WriteLine("\n\n" + Logger.Printlog(true));
            File.WriteAllText(@"c:\temp\Buildlog" + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".log", Logger.Printlog() + "\n\n" + (Logger.HasError() ? Logger.Printlog(true) + "\n\n" : "") + "Done " + invokedVerb.ToLower() + "! - Has Error: " + Logger.HasError());

            if (invokedVerb.ToLower() == "deploy" && !Logger.HasError())
            {
                var deployOptions = (DeployOptions)invokedVerbInstance;
                if (deployOptions.DoSpinup)
                {
                    var startInfo = new ProcessStartInfo
                    {
                        FileName = System.Reflection.Assembly.GetEntryAssembly().Location,
                        WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory,
                        Arguments = $"spinup -w {deployOptions.Warehouses} -p {deployOptions.Projects} -e {deployOptions.Env}",
                    };
                    Process.Start(startInfo);
                }
            }

            if (Logger.HasError())
                Environment.Exit(-1);
        }

        private static void LogActions(string action, Environments envToPrep, ProjectTypes projectToPrep, Warehouses whToPrep, bool takeDbBackup = false)
        {
            var actions = $"Environments: {envToPrep}\nProjects: {projectToPrep}\nWarehouses: {whToPrep}\nTakeDbBackup: {takeDbBackup}";
            Logger.Log(new LoggingData
            {
                Env = null,
                Message = action,
                ExtraMessage = actions,
                Project = null,
                Wh = null,
                Group = null,
                OrderNr = 0
            });
            Console.WriteLine("Action:{0}\n{1}\n\n", action, actions);
            Console.WriteLine("");
        }

        private static void LogActions(string action, Environments envToPrep, ProjectTypes projects, int state, Warehouses whToPrep)
        {
            var actions = string.Format("Environments: {0}\nProjects: {3}\nState: {1}\nWarehouses: {2}", envToPrep, state, whToPrep, projects);
            Logger.Log(new LoggingData
            {
                Env = null,
                Message = action,
                ExtraMessage = actions,
                Project = null,
                Wh = null,
                Group = null,
                OrderNr = 0
            });
            Console.WriteLine("Action:{0}\n{1}\n\n", action, actions);
            Console.WriteLine("");
        }

        private static void DeployImp(Environments env, ProjectTypes proj, Warehouses whs)
        {
            var buildGeneral = false;
            var DoRdb = false;
            var DoCdb = false;
            if (proj.HasFlag(ProjectTypes.WCFGeneral))
                if (proj.HasFlag(ProjectTypes.WCFWarehouse))
                {
                    buildGeneral = true;
                    proj -= ProjectTypes.WCFGeneral;
                }

            if (proj.HasFlag(ProjectTypes.CommandApi))
                if (proj.HasFlag(ProjectTypes.WCFWarehouse))
                {
                    DoCdb = true;
                    proj -= ProjectTypes.CommandApi;
                }

            if (proj.HasFlag(ProjectTypes.QueryDB))
                if (proj.HasFlag(ProjectTypes.WCFWarehouse))
                {
                    DoRdb = true;
                    proj -= ProjectTypes.QueryDB;
                }

            if (proj.HasFlag(ProjectTypes.ApiWcfApi))
            {
                if (whs.HasFlag(Warehouses.BE11))
                {
                        var prepLocation = @"d:\prep\" + env + @"\" + ProjectTypes.ApiWcfApi + @"\" + Warehouses.BE11;
                        Deploy.MVC(env, Warehouses.BE11, prepLocation, ProjectTypes.ApiWcfApi);
                }
                proj -= ProjectTypes.ApiWcfApi;
            }

            //BF Interfaces
            if (proj.HasFlag(ProjectTypes.BFApi))
            {
                if (whs.HasFlag(Warehouses.BE11))
                {
                    if (!env.HasFlag(Environments.CPYPRD))
                    {
                        var prepLocation = @"d:\prep\" + env + @"\" + ProjectTypes.BFApi;
                        Deploy.MVC(env, Warehouses.BE11, Path.Combine(prepLocation, Warehouses.BE11.ToString()), ProjectTypes.BFApi);
                    }
                }
                proj -= ProjectTypes.BFApi;
            }

            if (proj.HasFlag(ProjectTypes.BFJobExecutor))
            {
                if (whs.HasFlag(Warehouses.BE11))
                {
                    if (!env.HasFlag(Environments.CPYPRD))
                    {
                        var prepLocation = @"d:\prep\" + env + @"\" + ProjectTypes.BFJobExecutor;
                        Deploy.ServicesWHIndependent(env, prepLocation, ProjectTypes.BFJobExecutor);
                    }
                }
                proj -= ProjectTypes.BFJobExecutor;
            }

            if (proj.HasFlag(ProjectTypes.BFJobWebservice))
            {
                if (whs.HasFlag(Warehouses.BE11))
                {
                    if (!env.HasFlag(Environments.CPYPRD))
                    {
                        var prepLocation = @"d:\prep\" + env + @"\" + ProjectTypes.BFJobWebservice;
                        Deploy.MVC(env, Warehouses.BE11, prepLocation, ProjectTypes.BFJobWebservice);
                    }
                }
                proj -= ProjectTypes.BFJobWebservice;
            }

            if (proj.HasFlag(ProjectTypes.PartyApi))
            {
                if (whs.HasFlag(Warehouses.BE11))
                {
                    if (!env.HasFlag(Environments.CPYPRD))
                    {
                        var prepLocation = @"d:\prep\" + env + @"\" + ProjectTypes.PartyApi;
                        Deploy.MVC(env, Warehouses.BE11, prepLocation, ProjectTypes.PartyApi);
                    }
                }
                proj -= ProjectTypes.PartyApi;
            }

            if (proj.HasFlag(ProjectTypes.InventoryApi))
            {
                if (whs.HasFlag(Warehouses.BE11))
                {
                    if (!env.HasFlag(Environments.CPYPRD))
                    {
                        var prepLocation = @"d:\prep\" + env + @"\" + ProjectTypes.InventoryApi;
                        Deploy.PublishRDB(env, Warehouses.BE11, @"d:\prep\" + env.ToString() + @"\InventoryApi\BE11\DacPacs", ProjectTypes.InventoryApi);
                        Deploy.MVC(env, Warehouses.BE11, Path.Combine(prepLocation, Warehouses.BE11.ToString()), ProjectTypes.InventoryApi);
                    }
                }
                proj -= ProjectTypes.InventoryApi;
            }

            if(proj.HasFlag(ProjectTypes.BfInterfacingDacPac))
            {
                if(whs.HasFlag(Warehouses.BE11))
                {
                    if(!env.HasFlag(Environments.CPYPRD))
                    {
                        var prepLocation = @"d:\prep\" + env + @"\" + ProjectTypes.BfInterfacingDacPac;
                        Deploy.PublishRDB(env, Warehouses.BE11, @"d:\prep\" + env.ToString() + @"\BfInterfacingDacPac", ProjectTypes.BfInterfacingDacPac);
                    }
                }
                proj -= ProjectTypes.BfInterfacingDacPac;
            }

            //ALL the rest
            Parallel.ForEach(MaskToList<ProjectTypes>(proj), currentProj =>
            {
                var prepLocation = @"d:\prep\" + env.ToString() + @"\" + currentProj.ToString();

                if (currentProj == ProjectTypes.BFApi || currentProj == ProjectTypes.BFJobExecutor ||
                    currentProj == ProjectTypes.BFJobWebservice || currentProj == ProjectTypes.PartyApi)
                    return;

                switch (currentProj)
                {
                    case ProjectTypes.MVC:
                        Deploy.MVC(env, whs, prepLocation, currentProj);
                        break;
                    case ProjectTypes.WCFGeneral:
                        Deploy.WCFGeneral(env, whs, currentProj, prepLocation);
                        break;
                    case ProjectTypes.WCFWarehouse:
                        Deploy.WCFWH(env, whs, currentProj, prepLocation);
                        if (buildGeneral)
                            Deploy.WCFGeneral(env, whs, ProjectTypes.WCFGeneral, @"d:\prep\" + env.ToString() + @"\" + ProjectTypes.WCFGeneral.ToString());
                        if (DoCdb)
                            Deploy.APISites(env, whs, @"d:\prep\" + env.ToString() + @"\" + ProjectTypes.CommandApi.ToString(), ProjectTypes.CommandApi);
                        if (DoRdb)
                            Deploy.PublishRDB(env, whs, @"d:\prep\" + env.ToString() + @"\" + ProjectTypes.QueryDB.ToString(), ProjectTypes.QueryDB);
                        break;
                    case ProjectTypes.ScannerConsole:
                        Deploy.Scanner(env, whs, currentProj, prepLocation);
                        break;
                    case ProjectTypes.uProcessMonitor:
                        Deploy.ServicesWHIndependent(env, prepLocation, currentProj);
                        break;
                    case ProjectTypes.CommandApi:
                        Deploy.APISites(env, whs, prepLocation, currentProj);
                        break;
                    case ProjectTypes.QueryDB:
                        Deploy.PublishRDB(env, whs, prepLocation, currentProj);
                        break;
                    case ProjectTypes.InboundApi:
                        //TODO
                        break;
                    default:
                        Deploy.Services(env, whs, prepLocation, currentProj);
                        break;
                }
            });
        }

        public static IEnumerable<T> MaskToList<T>(Enum mask)
        {
            if (typeof(T).IsSubclassOf(typeof(Enum)) == false)
                throw new ArgumentException();

            return Enum.GetValues(typeof(T))
                                 .Cast<Enum>()
                                 //.Where(m => mask.HasFlag(m))
                                 .Where(mask.HasFlag)
                                 .Cast<T>();
        }

        public static bool HandleException(Exception ex)
        {
            Console.WriteLine("Not handling: {0}", ex.Message);
            return false;
        }
    }
}
