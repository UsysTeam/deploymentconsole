﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DeployConsole.Config;
using DeployConsole.Helpers;
using DeployConsole.Helpers.RemoteExecute;
using DeployConsole.Helpers.RemoteProcess;
using DeployConsole.Helpers.ServiceUtils;
using DeployConsole.Logging;
using Microsoft.Web.Administration;
using DeployConsole.Managers;
using System.Collections.Concurrent;
using System.Collections.Generic;
using DeployConsole.Data.Model;
using System.Security.Cryptography;

namespace DeployConsole
{
    public class Deploy
    {
        private const string PragmaServiceName = "InetD";

        public static void Scanner(Environments env, Warehouses whs, ProjectTypes currentProj, string prepLocation)
        {
            var configPropertyManager = new ConfigPropertyManager();
            var servers = configPropertyManager.GetServers(env, currentProj, whs);
            if (servers.Count == 0)
                throw new Exception($"Cannot find servers for Env: {env}  Project: {currentProj}");

            var keysToGet = new List<string> { "PragmaRegistryKeyLocation", "PragmaRegistryKeyName" };
            var configProperties = configPropertyManager.GetSimpleConfigProperties(env, keysToGet);

            foreach (var server in servers)
            {
                //stop InetD service
                //var sh = new ServiceHelper("InetD", server);
                //sh.Stop();
                var server1 = server; //Bypass Resharper AccessToForEachVariableInClosure
                var lockObject = new object();
                var pragmaServiceNeedsRestarting = false;
                Parallel.ForEach(EnumHelper.MaskToList<Warehouses>(whs), currentWh =>
                {
                    try
                    {
                        if (currentWh == Warehouses.None) return;
                        var prepLocationWh = Path.Combine(prepLocation, currentWh.ToString());
                        //FileCopy
                        var destinationPath = $@"\\{server1}\c$\Usys\{env.ToString()}\ScannerConsole\{currentWh}";
                        try
                        {
                            try
                            {
                                //kill all processes for currentWH
                                var processes = Process.GetProcessesByName("USys2.ScannerConsole", server1);
                                KillRemoteProcess.KillProcess(env, currentProj, processes.Select(process => process.Id).ToList(), currentWh, server1);

                                Logger.Log(new LoggingData
                                {
                                    Env = env,
                                    Exception = null,
                                    Message =
                                        $"Begin filecopy from '{prepLocationWh}' to '{destinationPath}'",
                                    Project = currentProj,
                                    Wh = currentWh
                                });
                                FsHelper.DirectoryCopy(prepLocationWh, destinationPath, true);
                                Logger.Log(new LoggingData
                                {
                                    Env = env,
                                    Exception = null,
                                    Message = "End filecopy.",
                                    Project = currentProj,
                                    Wh = currentWh
                                });

                                try
                                {
                                    var configPropertiesForThisEnvWhAndProj = configPropertyManager.GetConfigPropertyByPriorityAndEnrich(configProperties, env, currentProj, currentWh);

                                    foreach (var key in keysToGet)
                                    {
                                        var configProperty = configPropertiesForThisEnvWhAndProj.FirstOrDefault(c => c.Environment == env && c.PropertyName == key);
                                        if (configProperty == null)
                                        {
                                            throw new Exception($"Cannot find property {key} for Env: {env}  Project: {currentProj}");
                                        }
                                    }

                                    var keyHasChanged = SetRegistryKeyForScanner(env, currentProj, currentWh, server1, configPropertiesForThisEnvWhAndProj);
                                    lock (lockObject)
                                    {
                                        if (keyHasChanged && !pragmaServiceNeedsRestarting)
                                            pragmaServiceNeedsRestarting = true;
                                    }
                                }
                                catch (Exception e)
                                {
                                    Logger.Log(new LoggingData
                                    {
                                        Env = env,
                                        Exception = e,
                                        Message = $"Setting registry key for scanner FAILED on server {server1}",
                                        Project = currentProj,
                                        Wh = currentWh
                                    });
                                }
                            }
                            catch (Exception e)
                            {
                                Logger.Log(new LoggingData
                                {
                                    Env = env,
                                    Exception = e,
                                    Message =
                                        $"Kill process FAILED on server {server1}",
                                    Project = currentProj,
                                    Wh = currentWh
                                });
                            }
                        }
                        catch (Exception e)
                        {
                            Logger.Log(new LoggingData
                            {
                                Env = env,
                                Exception = e,
                                Message =
                                    $"Begin filecopy FAILED from '{prepLocationWh}' to '{destinationPath}'",
                                Project = currentProj,
                                Wh = currentWh
                            });
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Log(new LoggingData
                        {
                            Env = env,
                            Exception = e,
                            Message = e.Message + "\n" + e.StackTrace,
                            Project = currentProj,
                            Wh = currentWh
                        });
                    }
                });

                if (pragmaServiceNeedsRestarting)
                {
                    try
                    {
                        Logger.Log(new LoggingData
                        {
                            Env = env,
                            Exception = null,
                            Message = $"Restarting PragmaService {PragmaServiceName} on server '{server}'.",
                            Project = currentProj,
                            Wh = whs
                        });
                        RestartPragmaService(env, whs, server);
                        Logger.Log(new LoggingData
                        {
                            Env = env,
                            Exception = null,
                            Message = $"Restarting PragmaService {PragmaServiceName} on server '{server}' succeeded.",
                            Project = currentProj,
                            Wh = whs
                        });
                    }
                    catch (Exception e)
                    {
                        Logger.Log(new LoggingData
                        {
                            Env = env,
                            Exception = e,
                            Message = $@"Restarting PragmaService {PragmaServiceName} on server '{server}' failed: { e.Message } { Environment.NewLine } { e.StackTrace}",
                            Project = currentProj,
                            Wh = whs
                        });
                    }
                }
            }
        }

        private static bool SetRegistryKeyForScanner(Environments env, ProjectTypes currentProj, Warehouses currentWh, string server, List<ConfigProperty> configProperties)
        {
            var keyHasChanged = false;
            try
            {
                Logger.Log(new LoggingData
                {
                    Env = env,
                    Exception = null,
                    Message = "Checking registrykey for pragma",
                    Project = currentProj,
                    Wh = currentWh
                });

                var pragmaRegistryKeyLocation = configProperties.First(c => c.Environment == env && c.PropertyName == "PragmaRegistryKeyLocation");
                var pragmaRegistryKeyName = configProperties.First(c => c.Environment == env && c.PropertyName == "PragmaRegistryKeyName");

                if (!string.IsNullOrWhiteSpace(pragmaRegistryKeyLocation?.PropertyValue) && !string.IsNullOrWhiteSpace(pragmaRegistryKeyName?.PropertyValue))
                {
                    var newRegistryKeyValue = $@"C:\Usys\{env}\ScannerConsole\{currentWh}\USys2.ScannerConsole.exe";

                    Logger.Log(new LoggingData
                    {
                        Env = env,
                        Exception = null,
                        Message = $@"Setting pragma registrykey '{pragmaRegistryKeyLocation.PropertyValue}\{pragmaRegistryKeyName.PropertyValue}' on server '{server}' to '{newRegistryKeyValue}'",
                        Project = currentProj,
                        Wh = currentWh
                    });

                    keyHasChanged = RemoteRegistryConfig.SetRemoteRegistryKey(server, pragmaRegistryKeyLocation.PropertyValue, pragmaRegistryKeyName.PropertyValue, newRegistryKeyValue);

                    if (keyHasChanged)
                    {
                        Logger.Log(new LoggingData
                        {
                            Env = env,
                            Exception = null,
                            Message = $@"Setting pragma registrykey '{pragmaRegistryKeyLocation.PropertyValue}\{pragmaRegistryKeyName.PropertyValue}' on server '{server}' to '{newRegistryKeyValue}' succeeded",
                            Project = currentProj,
                            Wh = currentWh
                        });
                    }
                    else
                    {
                        Logger.Log(new LoggingData
                        {
                            Env = env,
                            Exception = null,
                            Message = $@"Pragma registrykey '{pragmaRegistryKeyLocation.PropertyValue}\{pragmaRegistryKeyName.PropertyValue}' on server '{server}' with value '{newRegistryKeyValue}' doesn't need changing.",
                            Project = currentProj,
                            Wh = currentWh
                        });
                    }
                }
                else
                {
                    Logger.Log(new LoggingData
                    {
                        Env = env,
                        Exception = null,
                        Message = "No registrykey location or name for pragma found",
                        Project = currentProj,
                        Wh = currentWh
                    });
                }
            }
            catch (Exception e)
            {
                Logger.Log(new LoggingData
                {
                    Env = env,
                    Exception = e,
                    Message = e.Message + "\n" + e.StackTrace,
                    Project = currentProj,
                    Wh = currentWh
                });
            }
            return keyHasChanged;
        }

        private static void RestartPragmaService(Environments env, Warehouses currentWh, string server)
        {
            using (var sh = new ServiceHelper(PragmaServiceName, server))
            {
                if (sh != null)
                {
                    var logGroup = new Guid().ToString();
                    ExecutionHelper.DoSomething(() => sh.Stop(),
                        "Stop service", $"Servicename: {PragmaServiceName}", env, ProjectTypes.ScannerConsole, currentWh, logGroup, 0);

                    var isItStarted = false;
                    var retryCount = 0;
                    var maxRetry = 3;
                    do
                    {
                        isItStarted = ExecutionHelper.DoSomething(() => sh.StartTimeout(),
                            "Start service", $"Servicename: {PragmaServiceName}", env, ProjectTypes.ScannerConsole, currentWh, logGroup, 1);
                        if (!isItStarted) retryCount++;
                    } while (!isItStarted && retryCount < maxRetry);
                }
            }
        }

        public static void WCFWH(Environments env, Warehouses whs, ProjectTypes currentProj, string prepLocation)
        {
            var servers = new ConfigPropertyManager().GetServers(env, currentProj, whs);
            if (servers.Count == 0)
                throw new Exception($"Cannot find servers for Env: {env}  Project: {currentProj}");

            var migrationSucceeded = false;
            var migrationCompleted = false;
            foreach (var server in servers)//Parallel.ForEach(servers, server =>
            {
                //Stop AppPool + update VD + Start pool 

                var siteNameConfig = new ConfigPropertyManager().GetSimpleConfigProperties(env, currentProj, whs, "SiteName").FirstOrDefault();

                var siteNames = EnumHelper.MaskToList<Warehouses>(whs)
                                            .Where(wh => wh != Warehouses.None)
                                            .Select(wh => string.Format(siteNameConfig.PropertyValue, wh))
                                            .ToList();

                var iis = IisHelper.GetIisHelper(server);
                var siteNamesString = "";
                var logGroup = Guid.NewGuid().ToString();

                foreach (var siteName in siteNames)
                {
                    siteNamesString += siteName + "; ";
                }

                ExecutionHelper.DoSomething(
                    () => iis.SetApplicationPoolStateForVd(siteNames, ObjectState.Stopped)
                    , "Stop application pools", siteNamesString, env, currentProj, null, logGroup, 0);

                foreach (var currentWh in EnumHelper.MaskToList<Warehouses>(whs))//, currentWh =>
                {
                    if (currentWh == Warehouses.None) continue;
                    var prepLocationWh = Path.Combine(prepLocation, currentWh.ToString());
                    //FileCopy
                    var temp = "_" + DateTime.Now.ToString("yyyyMMddhhmmss");
                    var destinationPath =
                        $@"\\{server}\c$\Usys\{env.ToString()}\WebServices\WarehouseServices\{currentWh}{temp}";
                    var destinationPathLocal =
                        $@"c:\Usys\{env.ToString()}\WebServices\WarehouseServices\{currentWh}{temp}";
                    var path = destinationPath;//Bypass Resharper AccessToForEachVariableInClosure
                    var copySucceeded = ExecutionHelper.DoSomething(() => FsHelper.DirectoryCopy(prepLocationWh, path, true)
                        , "DirectoryCopy", $"From: {prepLocationWh}\n\t\tTo: {destinationPath}", env, currentProj, currentWh, logGroup, 1);

                    if (!copySucceeded) continue;
                    //Run Migrations
                    if (server == servers.First())
                    {
                        var path1 = destinationPath; //Bypass Resharper AccessToForEachVariableInClosure
                        migrationSucceeded =
                            ExecutionHelper.DoSomething(
                                () => RunMigration(env, currentProj, currentWh, destinationPathLocal, path1, server)
                                , "RunMigration", $"For: {currentWh.ToString()}", env, currentProj,
                                currentWh, logGroup, 2);
                        migrationCompleted = true;
                        if (migrationSucceeded)
                        {
                            var destinationPath1 = destinationPath; //Bypass Resharper AccessToForEachVariableInClosure
                            ExecutionHelper.DoSomething(
                                () => FsHelper.DeleteDirectory(destinationPath1.Replace(temp, ""))
                                , "DeletePreviousFolder", $"Dir: {destinationPath1.Replace(temp, "")}",
                                env, currentProj, currentWh, logGroup, 3);
                            var path2 = destinationPath; //Bypass Resharper AccessToForEachVariableInClosure
                            ExecutionHelper.DoSomething(() => FsHelper.RenameDirectory(path2, path2.Replace(temp, ""))
                                , "RenameDirectory",
                                $"From: {path2}\n\t\tTo:{path2.Replace(temp, "")}", env, currentProj,
                                currentWh, logGroup, 4);
                            destinationPathLocal = destinationPathLocal.Replace(temp, "");
                            var site = siteNames.Where(x => x.Contains(currentWh.ToString()));
                            ExecutionHelper.DoSomething(
                                () => iis.UpdatePhysicalPathVd(site.ToList(), destinationPathLocal)
                                , "UpdatePhysicalPathVD",
                                $"Site: {site.FirstOrDefault()}\n\t\tDir: {destinationPathLocal}",
                                env, currentProj, currentWh, logGroup, 5);
                        }
                        else
                        {
                            ExecutionHelper.DoSomething(() => FsHelper.DeleteDirectory(destinationPath)
                                , "DeleteTempFolder", $"Dir: {destinationPath}", env, currentProj, currentWh,
                                logGroup, 5);
                        }
                    }
                    else
                    {
                        while (!migrationCompleted)
                            Thread.Sleep(1000);
                        if (migrationSucceeded)
                        {
                            var destinationPath1 = destinationPath; //Bypass Resharper AccessToForEachVariableInClosure
                            ExecutionHelper.DoSomething(
                                () => FsHelper.DeleteDirectory(destinationPath1.Replace(temp, ""))
                                , "DeletePreviousFolder", $"Dir: {destinationPath1.Replace(temp, "")}",
                                env, currentProj, currentWh, logGroup, 3);
                            var path2 = destinationPath; //Bypass Resharper AccessToForEachVariableInClosure
                            ExecutionHelper.DoSomething(() => FsHelper.RenameDirectory(path2, path2.Replace(temp, ""))
                                , "RenameDirectory",
                                $"From: {path2.Replace(temp, "")}\n\t\tTo:{path2}", env, currentProj,
                                currentWh, logGroup, 4);
                            destinationPathLocal = destinationPathLocal.Replace(temp, "");
                            var site = siteNames.Where(x => x.Contains(currentWh.ToString()));
                            ExecutionHelper.DoSomething(
                                () => iis.UpdatePhysicalPathVd(site.ToList(), destinationPathLocal)
                                , "UpdatePhysicalPathVD",
                                $"Site: {site.FirstOrDefault()}\n\t\tDir: {destinationPathLocal}",
                                env, currentProj, currentWh, logGroup, 5);
                        }
                        else
                        {
                            ExecutionHelper.DoSomething(() => FsHelper.DeleteDirectory(destinationPath)
                                , "DeleteTempFolder", $"Dir: {destinationPath}", env, currentProj, currentWh,
                                logGroup, 5);
                        }
                    }

                }
                ExecutionHelper.DoSomething(() => iis.SetApplicationPoolStateForVd(siteNames, ObjectState.Started)
                        , "SetApplicationPoolStateForVD",
                    $"Sites: {siteNamesString}\tTo: {ObjectState.Started.ToString()}", env, currentProj, null, logGroup, 6);
            }
        }


        private static string ReadStream(object streamReader)
        {
            string result = ((StreamReader)streamReader).ReadToEnd();

            return result;
        }

        public static ConcurrentDictionary<string, string> concurrentDictionary = new ConcurrentDictionary<string, string>();

        private static bool PublishDacPac(Environments env, ProjectTypes currentProj, Warehouses currentWh, string destinationPathLocal, string destinationPath, string server, string commandLineArgs = null)
        {
            //todo: clean it
            var timeStamp = DateTime.Now.ToString("yyyyMMddHHmmss");
            if (string.IsNullOrWhiteSpace(commandLineArgs))
            {
                var pd = Parameters.GetAllParmList(env, currentProj, currentWh);
                commandLineArgs = string.Format(pd["MigrationCommand"], destinationPath, timeStamp);
            }
            else
            {
                commandLineArgs = string.Format(commandLineArgs, destinationPath, timeStamp);
            }
            ///action:Publish /SourceFile:"\\10.3.147.4\c$\Usys\DEV\Handlers\BE11CF\DacPacs\Database.Query\Usys.Database.Query.dacpac" /Profile:"\\10.3.147.4\c$\Usys\DEV\Handlers\BE11CF\DacPacs\Database.Query\USYS_USYS2_R_WAREHOUSE.publish.xml"
            ///
            var zoekDit = "SourceFile:";
            var startIdx = commandLineArgs.IndexOf(zoekDit);
            var endIdx = commandLineArgs.IndexOf('"', startIdx + zoekDit.Length + 1);
            var sourceFile = commandLineArgs.Substring(startIdx + zoekDit.Length + 1, endIdx - startIdx - zoekDit.Length - 1);

            var md5HashFromFile = GetMD5HashFromFile(sourceFile);
            var dictionaryKey = $"{env}{currentWh}{md5HashFromFile}";
            if (concurrentDictionary.ContainsKey(dictionaryKey))
            {
                Logger.Log(new LoggingData
                {
                    Env = env,
                    Project = currentProj,
                    Wh = currentWh,
                    Message = $"The following dacpac was already executed:{Environment.NewLine}{env} {currentWh} {sourceFile}"
                });
                return true;
            }

            zoekDit = "Profile:";
            startIdx = commandLineArgs.IndexOf(zoekDit);
            endIdx = commandLineArgs.IndexOf('"', startIdx + zoekDit.Length + 1);
            var profileFile = commandLineArgs.Substring(startIdx + zoekDit.Length + 1, endIdx - startIdx - zoekDit.Length - 1);
            var aNewFile = "";

            switch (currentProj)
            {
                case ProjectTypes.InventoryApi:
                    aNewFile = Path.Combine(destinationPath, "BE11_BF", currentWh + timeStamp + ".xml");
                    break;
                case ProjectTypes.Handlers:
                case ProjectTypes.JobProcessorCF:
                    aNewFile = Path.Combine(destinationPath, currentWh + timeStamp + ".xml");
                    break;
                case ProjectTypes.BfInterfacingDacPac:
                    aNewFile = Path.Combine(destinationPath, "BE11", currentWh + timeStamp + ".xml");
                    break;
                default:
                    aNewFile = Path.Combine(destinationPath, "DacPacs", currentWh + timeStamp + ".xml");
                    break;
            }
            var logfile = aNewFile + ".log";
            File.WriteAllText(aNewFile, File.ReadAllText(profileFile).Replace("{WAREHOUSE}", currentWh.ToString()));
            commandLineArgs = commandLineArgs.Replace(profileFile, aNewFile);

            var psi = new ProcessStartInfo
            {
                FileName = @"C:\Program Files (x86)\Microsoft SQL Server\120\DAC\bin\SqlPackage.exe",
                WorkingDirectory = @"C:\Program Files (x86)\Microsoft SQL Server\120\DAC\bin",
                Arguments = commandLineArgs,
                UseShellExecute = false,
                CreateNoWindow = true,
                RedirectStandardError = true,
                RedirectStandardOutput = true
            };
            var p = new Process { StartInfo = psi };
            var output = "";
            int exitCode;

            try
            {
                Console.WriteLine("Running DacPac for {0} Wh: {1}\nCommand: {2}\nFor server: {3}", currentProj, currentWh, commandLineArgs, server);

                p.Start();
                const int timeout = 1000000;
                using (var processWaiter = Task.Factory.StartNew(() => p.WaitForExit(timeout)))
                using (var outputReader = Task.Factory.StartNew((Func<object, string>)ReadStream, p.StandardOutput))
                using (var errorReader = Task.Factory.StartNew((Func<object, string>)ReadStream, p.StandardError))
                {
                    var waitResult = processWaiter.Result;

                    if (!waitResult)
                        p.Kill();

                    Task.WaitAll(outputReader, errorReader);

                    if (!waitResult)
                        throw new TimeoutException("Process wait timeout expired");

                    exitCode = p.ExitCode;

                    output = outputReader.Result;
                    output += errorReader.Result;
                    File.WriteAllText(logfile, output);
                }
            }
            catch (Exception e)
            {
                Logger.Log(new LoggingData
                {
                    Env = env,
                    Message = "Error publishing Dacpac for " + currentWh,
                    ExtraMessage = "Profile:" + aNewFile + Environment.NewLine + Environment.NewLine + output,
                    Project = currentProj,
                    Wh = currentWh,
                    Group = "",
                    Exception = e
                });


                throw new Exception(output, e);
            }
            if (exitCode == 0)
            {
                concurrentDictionary[dictionaryKey] = $"{env} {currentWh} {sourceFile}";
                return true;
            }
            Logger.Log(new LoggingData
            {
                Env = env,
                Message = "Error running Dacpac for " + currentWh,
                ExtraMessage = "Profile:" + aNewFile + Environment.NewLine + Environment.NewLine + output,
                Project = currentProj,
                Wh = currentWh,
                Group = ""
            });

            throw new Exception(output);
        }

        private static string GetMD5HashFromFile(string filePath)
        {
            byte[] retVal;
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filePath))
                {
                    retVal = md5.ComputeHash(stream);
                }
            }

            var sb = new StringBuilder();
            for (int i = 0; i < retVal.Length; i++)
                sb.Append(retVal[i].ToString("x2"));
            return sb.ToString();
        }

        private static bool RunMigration(Environments env, ProjectTypes currentProj, Warehouses currentWh, string destinationPathLocal, string destinationPath, string server)
        {
            var pd = Parameters.GetAllParmList(env, currentProj, currentWh);
            var timeStamp = DateTime.Now.ToString("yyyyMMddHHmmss");

            var result = DoActualMigration(env, currentProj, currentWh, destinationPathLocal, destinationPath, server, pd["MigrationCommand"], timeStamp);
            if (result && currentProj == ProjectTypes.WCFWarehouse)
                result = DoActualMigration(env, currentProj, currentWh, destinationPathLocal, destinationPath, server, pd["MigrationCommandLogWH"], timeStamp);
            if (result && currentProj == ProjectTypes.WCFWarehouse)
                result = DoActualMigration(env, currentProj, currentWh, destinationPathLocal, destinationPath, server, pd["MigrationCommandLogNonWH"], timeStamp);
            return result;
        }

        private static bool DoActualMigration(Environments env, ProjectTypes currentProj, Warehouses currentWh, string destinationPathLocal, string destinationPath, string server, string migrationCommand, string timeStamp)
        {
            var commandLine = string.Format(migrationCommand, destinationPathLocal, timeStamp);
            var logfile = string.Format(migrationCommand.Split(' ').Last(), destinationPath, timeStamp).Replace("\"", "");
            var p = new ProcessWMI();
            const int timeout = 120000;
            var succeeded = false;
            for (var i = 0; i < 3 && !succeeded; i++)
            {
                try
                {
#if DEBUG
                    //TODO DR: Use other logfiles
                    //if (commandLine.Contains("USys2.Data.Logging.Migrations"))
                    //    Debugger.Break();
#endif
                    Console.WriteLine("Running migrations for {0} Wh: {1}\nCommand: {2}\nOn server: {3}", currentProj,
                        currentWh, commandLine, server);
                    p.ExecuteRemoteProcessWMI(server, commandLine, timeout);
                    succeeded = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Failed running migrations for {0} Wh: {1}\nCommand: {2}\nOn server: {3}\nException: {4}", currentProj,
                        currentWh, commandLine, server, e);

                    var sb = new StringBuilder();

                    var succeededGettingLogFile = false;
                    for (var j = 0; j < 3 && !succeededGettingLogFile; j++)
                    {
                        try
                        {
                            sb.AppendLine("[ERROR] in migrations for " + currentWh);
                            using (var reader = new StreamReader(logfile))
                            {
                                while (reader.Peek() >= 0)
                                {
                                    sb.AppendLine(reader.ReadLine());
                                }
                            }
                            succeededGettingLogFile = true;
                        }
                        catch (Exception exception)
                        {
                            if (j < 2)
                            {
                                Console.WriteLine("Failed getting logfile for migration: {0} {1} time, retrying...\nException: {2}", logfile, j + 1, exception);
                                Thread.Sleep(5000);
                            }
                            else
                            {
                                Logger.Log(new LoggingData
                                {
                                    Env = env,
                                    Message = "Unable to get migration log",
                                    ExtraMessage = "logfile",
                                    Project = currentProj,
                                    Wh = currentWh,
                                    Group = "",
                                    Exception = exception
                                });
                            }
                        }
                    }
                    if (i < 2)
                    {
                        Console.WriteLine("[ERROR] in migrations for " + (i + 1) + " time, let's retry!");
                        sb.AppendLine("[ERROR] in migrations for " + (i + 1) + " time, let's retry!");
                        Thread.Sleep(5000);
                    }
                    else
                    {
                        throw new Exception(sb.ToString(), e);
                    }
                }
            }
            return true;
        }

        public static void PublishRDB(Environments env, Warehouses whs, string prepLocation, ProjectTypes currentProj)
        {
            var servers = new ConfigPropertyManager().GetServers(env, currentProj, whs);
            if (servers.Count == 0)
                throw new Exception($"Cannot find servers for Env: {env}  Project: {currentProj}");


            var logGroup = Guid.NewGuid().ToString();
            var server = servers[0]; //Bypass Resharper AccessToForEachVariableInClosure
                                     //var server1 = server; //Bypass Resharper AccessToForEachVariableInClosure
            Parallel.ForEach(EnumHelper.MaskToList<Warehouses>(whs), currentWh =>
            {
                try
                {
                    if (currentWh == Warehouses.None) return;
                    var migrationSucceeded =
                        ExecutionHelper.DoSomething(
                            () => PublishDacPac(env, currentProj, currentWh, "", prepLocation, server)
                            , "PublishDacPac", $"For: {currentWh.ToString()}", env, currentProj,
                            null, logGroup, 0);
                }
                catch (Exception e)
                {
                    Logger.Log(new LoggingData
                    {
                        Env = env,
                        Exception = e,
                        Message = e.Message + "\n" + e.StackTrace,
                        Project = currentProj,
                        Wh = currentWh
                    });
                }
            });

        }

        public static void PublishBfInterfacingDacpac(Environments env, Warehouses whs, string prepLocation, ProjectTypes currentProj)
        {
            var servers = new ConfigPropertyManager().GetServers(env, currentProj, whs);
            if (servers.Count == 0)
                throw new Exception($"Cannot find servers for Env: {env}  Project: {currentProj}");


            var logGroup = Guid.NewGuid().ToString();
            var server = servers[0]; //Bypass Resharper AccessToForEachVariableInClosure
                                     //var server1 = server; //Bypass Resharper AccessToForEachVariableInClosure
            Parallel.ForEach(EnumHelper.MaskToList<Warehouses>(whs), currentWh =>
            {
                try
                {
                    if (currentWh == Warehouses.None)
                        return;

                    if (currentWh != Warehouses.BE11)
                        return;

                    var publishSucceeded = ExecutionHelper.DoSomething(
                        () => PublishDacPac(env, currentProj, currentWh, "", prepLocation, server), "PublishDacPac", $"For: {currentWh.ToString()}", env, currentProj, null, logGroup, 0);
                }
                catch (Exception e)
                {
                    Logger.Log(new LoggingData
                    {
                        Env = env,
                        Exception = e,
                        Message = e.Message + "\n" + e.StackTrace,
                        Project = currentProj,
                        Wh = currentWh
                    });
                }
            });
        }


        public static void APISites(Environments env, Warehouses whs, string prepLocation, ProjectTypes currentProj)
        {
            //findVersion
            FileVersionInfo fvi = null;
            switch (currentProj)
            {
                case ProjectTypes.CommandApi:
                    fvi = FileVersionInfo.GetVersionInfo(Path.Combine(prepLocation, "bin", "Usys.Server.CommandApi.dll"));
                    break;
                case ProjectTypes.PartyApi:
                    fvi = FileVersionInfo.GetVersionInfo(Path.Combine(prepLocation, "bin", "Usys.Server.PartyWebApi.dll"));
                    break;
                case ProjectTypes.InboundApi:
                    //fvi = FileVersionInfo.GetVersionInfo(Path.Combine(prepLocation, "bin", "Usys.Server.PartyWebApi.dll"));
                    break;
                default:
                    fvi = FileVersionInfo.GetVersionInfo(Path.Combine(prepLocation, "bin", "Usys2.Web.Shell.dll"));
                    break;
            }

            var servers = new ConfigPropertyManager().GetServers(env, currentProj, whs);
            if (servers.Count == 0)
                throw new Exception($"Cannot find servers for Env: {env}  Project: {currentProj}");

            var migrationSucceeded = false;
            var migrationCompleted = false;

            Parallel.ForEach(servers, server =>
            {
                //FileCopy
                var destination = string.Empty;
                var destinationPathLocal = string.Empty;
                switch (currentProj)
                {
                    case ProjectTypes.CommandApi:
                        destination = $@"\\{server}\c$\Usys\{env.ToString()}\WebServices\Command.Api\v{fvi.FileVersion}";
                        destinationPathLocal = $@"c:\Usys\{env.ToString()}\WebServices\Command.Api\v{fvi.FileVersion}";
                        break;
                    //case ProjectTypes.PocService:
                    //    destination = string.Format(@"\\{0}\c$\Usys\{1}\WebServices\Usys.Server.CommandWebApi\v{2}", server, env.ToString(), fvi.FileVersion);
                    //    destinationPathLocal = string.Format(@"c:\Usys\{0}\WebServices\Usys.Server.CommandWebApi\v{1}", env.ToString(), fvi.FileVersion);
                    //    break;
                    //case ProjectTypes.PocApi:
                    //    destination = string.Format(@"\\{0}\c$\Usys\{1}\WebServices\Usys.Server.PartyWebApi\v{2}", server, env.ToString(), fvi.FileVersion);
                    //    destinationPathLocal = string.Format(@"c:\Usys\{0}\WebServices\Usys.Server.PartyWebApi\v{1}", env.ToString(), fvi.FileVersion);
                    //    break;
                    //default:
                    //    destination = string.Format(@"\\{0}\c$\Usys\{1}\Usys2.Webshell\v{2}", server, env.ToString(), fvi.FileVersion);
                    //    destinationPathLocal = string.Format(@"c:\Usys\{0}\Usys2.Webshell\v{1}", env.ToString(), fvi.FileVersion);
                    //    break;
                    default:
                        throw new Exception("Could not determin physical paths for " + currentProj);
                }

                var logGroup = Guid.NewGuid().ToString();

                if (Directory.Exists(destination))
                {
                    Logger.Log(new LoggingData
                    {
                        Env = env,
                        Message = "DirectoryCopy",
                        ExtraMessage = "Not needed, target already exists, " + destination,
                        Project = currentProj,
                        Wh = null,
                        Group = logGroup,
                        OrderNr = 1
                    });
                }
                else
                {
                    var copySucceeded = ExecutionHelper.DoSomething(() =>
                            FsHelper.DirectoryCopy(prepLocation, destination, true)
                            , "DirectoryCopy", $"From: {prepLocation}\n\t\tTo: {destination}", env, currentProj, null, logGroup, 1);

                    if (!copySucceeded) return;
                }

                //Stop AppPool + update Path + Start pool
                var siteNamesString = "";

                var siteNameConfig = new ConfigPropertyManager().GetSimpleConfigProperties(env, currentProj, "SiteName");
                List<string> siteNames = new List<string>();
                Parallel.ForEach(EnumHelper.MaskToList<Warehouses>(whs), currentWh =>
                {

                    if (currentWh == Warehouses.None) return;
                    //Publish DB

                    if (server == servers.First())
                    {
                        migrationSucceeded =
                            ExecutionHelper.DoSomething(
                                () => PublishDacPac(env, currentProj, currentWh, destinationPathLocal, destination, server)
                                , "PublishDacPac", $"For: {currentWh.ToString()}", env, currentProj,
                                null, logGroup, 2);

                        //migrationSucceeded = true;
                        migrationCompleted = true;
                        if (migrationSucceeded)
                        {
                            siteNames.Add(string.Format(siteNameConfig, currentWh, env));


                            //var values = whs.ToString()
                            //       .Split(new[] { ", " }, StringSplitOptions.None)
                            //       .Select(v => (Warehouses)Enum.Parse(typeof(Warehouses), v));
                            //foreach (var w in values)
                            //{
                            //    siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, currentWh, env, currentProj));
                            //}

                            //foreach (var siteName in siteNames)
                            //{
                            //    siteNamesString += siteName + "; ";
                            //}

                            //var iis = new IisHelper(server);
                            //ExecutionHelper.DoSomething(
                            //    () => iis.SetApplicationPoolStateForSite(siteNames, ObjectState.Stopped)
                            //    , "Stop application pools", siteNamesString, env, currentProj, null, logGroup, 2);

                            //ExecutionHelper.DoSomething(
                            //    () => iis.UpdatePhysicalPathSite(siteNames, destinationPathLocal)
                            //         , "UpdatePhysicalPathSite", siteNamesString + "\n\t\tNewPath: " + destinationPathLocal, env, currentProj, null, logGroup, 3);

                            //ExecutionHelper.DoSomething(
                            //    () => iis.SetApplicationPoolStateForSite(siteNames, ObjectState.Started)
                            //         , "Start application pools", siteNamesString, env, currentProj, null, logGroup, 4);
                            //iis.CommitAndClose();
                        }
                        //else
                        //throw new Exception("dacpack trouble");
                    }
                    else
                    {
                        while (!migrationCompleted)
                            Thread.Sleep(1000);
                        if (migrationSucceeded)
                        {
                            siteNames.Add(string.Format(siteNameConfig, currentWh, env));
                            //    //Stop AppPool + update Path + Start pool
                            //    var siteNamesString = "";

                            //    var siteNameConfig = new ConfigPropertyManager().GetSimpleConfigProperties(env, currentProj, "SiteName");
                            //    List<string> siteNames = new List<string>();

                            //    var values = whs.ToString()
                            //           .Split(new[] { ", " }, StringSplitOptions.None)
                            //           .Select(v => (Warehouses)Enum.Parse(typeof(Warehouses), v));
                            //    foreach (var w in values)
                            //    {
                            //        if (w.Equals(Warehouses.None))
                            //            throw new Exception("'GetSiteNames' for not found 1");
                            //        else if (w.Equals(Warehouses.BE11))
                            //            siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.BE11, env, currentProj));

                            //        else if (w.Equals(Warehouses.BG01))
                            //            siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.BG01, env, currentProj));

                            //        else if (w.Equals(Warehouses.FR01))
                            //            siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.FR01, env, currentProj));

                            //        else if (w.Equals(Warehouses.PL01))
                            //            siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PL01, env, currentProj));

                            //        else if (w.Equals(Warehouses.PL02))
                            //            siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PL02, env, currentProj));

                            //        else if (w.Equals(Warehouses.PL03))
                            //            siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PL03, env, currentProj));

                            //        else if (w.Equals(Warehouses.PL04))
                            //            siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PL04, env, currentProj));

                            //        else if (w.Equals(Warehouses.PT01))
                            //            siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PT01, env, currentProj));
                            //    }

                            //    foreach (var siteName in siteNames)
                            //    {
                            //        siteNamesString += siteName + "; ";
                            //    }

                            //    var iis = new IisHelper(server);
                            //    ExecutionHelper.DoSomething(
                            //        () => iis.SetApplicationPoolStateForSite(siteNames, ObjectState.Stopped)
                            //        , "Stop application pools", siteNamesString, env, currentProj, null, logGroup, 2);

                            //    ExecutionHelper.DoSomething(
                            //        () => iis.UpdatePhysicalPathSite(siteNames, destinationPathLocal)
                            //             , "UpdatePhysicalPathSite", siteNamesString + "\n\t\tNewPath: " + destinationPathLocal, env, currentProj, null, logGroup, 3);

                            //    ExecutionHelper.DoSomething(
                            //        () => iis.SetApplicationPoolStateForSite(siteNames, ObjectState.Started)
                            //             , "Start application pools", siteNamesString, env, currentProj, null, logGroup, 4);
                            //    iis.CommitAndClose();
                        }
                        //else
                        //throw new Exception("dacpack trouble");

                    }

                });
                foreach (var siteName in siteNames)
                {
                    siteNamesString += siteName + "; ";
                }

                var iis = IisHelper.GetIisHelper(server);
                ExecutionHelper.DoSomething(
                    () => iis.SetApplicationPoolStateForSite(siteNames, ObjectState.Stopped)
                    , "Stop application pools", siteNamesString, env, currentProj, null, logGroup, 2);

                ExecutionHelper.DoSomething(
                    () => iis.UpdatePhysicalPathSite(siteNames, destinationPathLocal)
                         , "UpdatePhysicalPathSite", siteNamesString + "\n\t\tNewPath: " + destinationPathLocal, env, currentProj, null, logGroup, 3);

                ExecutionHelper.DoSomething(
                    () => iis.SetApplicationPoolStateForSite(siteNames, ObjectState.Started)
                         , "Start application pools", siteNamesString, env, currentProj, null, logGroup, 4);



                //-------------weg
                //    //Stop AppPool + update Path + Start pool
                //    var siteNamesString = "";

                //    var siteNameConfig = new ConfigPropertyManager().GetSimpleConfigProperties(env, currentProj, "SiteName");
                //    List<string> siteNames = new List<string>();

                //    var values = whs.ToString()
                //           .Split(new[] { ", " }, StringSplitOptions.None)
                //           .Select(v => (Warehouses)Enum.Parse(typeof(Warehouses), v));
                //    foreach (var w in values)
                //    {
                //        if (w.Equals(Warehouses.None))
                //            throw new Exception("'GetSiteNames' for not found 1");
                //        else if (w.Equals(Warehouses.BE11))
                //            siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.BE11, env, currentProj));

                //        else if (w.Equals(Warehouses.BG01))
                //            siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.BG01, env, currentProj));

                //        else if (w.Equals(Warehouses.FR01))
                //            siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.FR01, env, currentProj));

                //        else if (w.Equals(Warehouses.PL01))
                //            siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PL01, env, currentProj));

                //        else if (w.Equals(Warehouses.PL02))
                //            siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PL02, env, currentProj));

                //        else if (w.Equals(Warehouses.PL03))
                //            siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PL03, env, currentProj));

                //        else if (w.Equals(Warehouses.PL04))
                //            siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PL04, env, currentProj));

                //        else if (w.Equals(Warehouses.PT01))
                //            siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PT01, env, currentProj));
                //    }

                //    foreach (var siteName in siteNames)
                //    {
                //        siteNamesString += siteName + "; ";
                //    }

                //    var iis = new IisHelper(server);
                //    ExecutionHelper.DoSomething(
                //        () => iis.SetApplicationPoolStateForSite(siteNames, ObjectState.Stopped)
                //        , "Stop application pools", siteNamesString, env, currentProj, null, logGroup, 2);

                //    ExecutionHelper.DoSomething(
                //        () => iis.UpdatePhysicalPathSite(siteNames, destinationPathLocal)
                //             , "UpdatePhysicalPathSite", siteNamesString + "\n\t\tNewPath: " + destinationPathLocal, env, currentProj, null, logGroup, 3);

                //    ExecutionHelper.DoSomething(
                //        () => iis.SetApplicationPoolStateForSite(siteNames, ObjectState.Started)
                //             , "Start application pools", siteNamesString, env, currentProj, null, logGroup, 4);
                //    iis.CommitAndClose();
                //}
                //-------------weg

            });
        }

        public static void MVC(Environments env, Warehouses whs, string prepLocation, ProjectTypes currentProj)
        {
            //TODO: cleanup after 5.5 is in release
            var useJobExecutorWebservice = true;

            //findVersion
            FileVersionInfo fvi = null;
            switch (currentProj)
            {
                case ProjectTypes.CommandApi:
                    fvi = FileVersionInfo.GetVersionInfo(Path.Combine(prepLocation, "bin", "Usys.Server.CommandApi.dll"));
                    break;
                case ProjectTypes.PartyApi:
                    fvi = FileVersionInfo.GetVersionInfo(Path.Combine(prepLocation, "bin", "Usys.Server.PartyWebApi.dll"));
                    break;
                case ProjectTypes.BFApi:
                    fvi = FileVersionInfo.GetVersionInfo(Path.Combine(prepLocation, "bin", "Usys.Server.BenFreshWebApi.dll"));
                    break;
                case ProjectTypes.BFJobWebservice:
                    //TODO: cleanup after 5.5 is in release
                    if (File.Exists(Path.Combine(prepLocation, "bin", "Usys.JobExecutor.Webservice.dll")))
                    {
                        fvi = FileVersionInfo.GetVersionInfo(Path.Combine(prepLocation, "bin", "Usys.JobExecutor.Webservice.dll"));
                        useJobExecutorWebservice = true;
                    }
                    else
                    if (File.Exists(Path.Combine(prepLocation, "bin", "Usys.JobExecutor.WebApi.dll")))
                    {
                        fvi = FileVersionInfo.GetVersionInfo(Path.Combine(prepLocation, "bin", "Usys.JobExecutor.WebApi.dll"));
                        useJobExecutorWebservice = false;
                    }
                    else
                        throw new Exception("Cannot find input for Usys.JobExecutor.Webservice.dll/Usys.JobExecutor.WebApi.dll in Deploy.cs-MVC");
                    break;


                //// BFJobWebservice 5.4.5
                //// fvi = FileVersionInfo.GetVersionInfo(Path.Combine(prepLocation, "bin", "Usys.JobExecutor.Webservice.dll"));
                ////
                //// BFJobWebservice 5.5+
                //fvi = FileVersionInfo.GetVersionInfo(Path.Combine(prepLocation, "bin", "Usys.JobExecutor.WebApi.dll"));
                //break;
                case ProjectTypes.InventoryApi:
                    fvi = FileVersionInfo.GetVersionInfo(Path.Combine(prepLocation, "bin", "Usys.Server.UnivegFilesWebApi.dll"));
                    break;
                case ProjectTypes.ApiWcfApi:
                    fvi = FileVersionInfo.GetVersionInfo(Path.Combine(prepLocation, "bin", "Usys.ApiWcfApi.dll"));
                    break;
                default:
                    fvi = FileVersionInfo.GetVersionInfo(Path.Combine(prepLocation, "bin", "Usys2.Web.Shell.dll"));
                    break;
            }

            var servers = new ConfigPropertyManager().GetServers(env, currentProj, whs);
            if (servers.Count == 0)
                throw new Exception($"Cannot find servers for Env: {env}  Project: {currentProj}");

            Parallel.ForEach(servers, server =>
            {
                //FileCopy
                var destination = string.Empty;
                var destinationPathLocal = string.Empty;
                switch (currentProj)
                {
                    case ProjectTypes.CommandApi:
                        destination = $@"\\{server}\c$\Usys\{env.ToString()}\WebServices\Command.Api\v{fvi.FileVersion}";
                        destinationPathLocal = $@"c:\Usys\{env.ToString()}\WebServices\Command.Api\v{fvi.FileVersion}";
                        break;
                    case ProjectTypes.PartyApi:
                        destination =
                            $@"\\{server}\c$\Usys\{env.ToString()}\WebServices\Usys.Server.PartyWebApi\v{fvi.FileVersion
                                }";
                        destinationPathLocal =
                            $@"c:\Usys\{env.ToString()}\WebServices\Usys.Server.PartyWebApi\v{fvi.FileVersion}";
                        break;
                    case ProjectTypes.BFApi:
                        //TODO: THIS IS NOT CORRECT!
                        destination =
                            $@"\\{server}\c$\Usys\{env.ToString()}\WebServices\Usys.Server.BenFreshWebApi\v{
                                fvi.FileVersion}";
                        destinationPathLocal =
                            $@"c:\Usys\{env.ToString()}\WebServices\Usys.Server.BenFreshWebApi\v{fvi.FileVersion}";
                        break;
                    case ProjectTypes.InventoryApi:
                        //TODO: THIS IS NOT CORRECT!
                        destination =
                            $@"\\{server}\c$\Usys\{env.ToString()}\WebServices\Usys.Server.UnivegFilesWebApi\v{
                                fvi.FileVersion}";
                        destinationPathLocal =
                            $@"c:\Usys\{env.ToString()}\WebServices\Usys.Server.UnivegFilesWebApi\v{fvi.FileVersion}";
                        break;
                    case ProjectTypes.BFJobWebservice:
                        // BFJobWebservice 5.4.5
                        //destination =
                        //    $@"\\{server}\c$\Usys\{env.ToString()}\WebServices\Usys.JobExecutor.Webservice\v{fvi.FileVersion}";
                        //destinationPathLocal =
                        //    $@"c:\Usys\{env.ToString()}\WebServices\Usys.JobExecutor.Webservice\v{fvi.FileVersion}";
                        //
                        // BFJobWebservice 5.5+

                        //TODO: cleanup after 5.5 is in release
                        if (useJobExecutorWebservice)
                        {
                            destination =
                                $@"\\{server}\c$\Usys\{env.ToString()}\WebServices\Usys.JobExecutor.Webservice\v{fvi.FileVersion}";
                            destinationPathLocal =
                                $@"c:\Usys\{env.ToString()}\WebServices\Usys.JobExecutor.Webservice\v{fvi.FileVersion}";
                        }
                        else
                        {
                            destination =
                                $@"\\{server}\c$\Usys\{env.ToString()}\WebServices\Usys.JobExecutor.WebApi\v{fvi.FileVersion}";
                            destinationPathLocal =
                                $@"c:\Usys\{env.ToString()}\WebServices\Usys.JobExecutor.WebApi\v{fvi.FileVersion}";
                        }
                        break;
                    case ProjectTypes.ApiWcfApi:
                        destination =
                            $@"\\{server}\c$\Usys\{env.ToString()}\WebServices\Usys.ApiWcfApi\v{
                                fvi.FileVersion}";
                        destinationPathLocal =
                            $@"c:\Usys\{env.ToString()}\WebServices\Usys.ApiWcfApi\v{fvi.FileVersion}";
                        break;
                    default:
                        destination = $@"\\{server}\c$\Usys\{env.ToString()}\Usys2.Webshell\v{fvi.FileVersion}";
                        destinationPathLocal = $@"c:\Usys\{env.ToString()}\Usys2.Webshell\v{fvi.FileVersion}";
                        break;
                }

                var logGroup = Guid.NewGuid().ToString();

                if (Directory.Exists(destination))
                {
                    Logger.Log(new LoggingData
                    {
                        Env = env,
                        Message = "DirectoryCopy",
                        ExtraMessage = "Not needed, target already exists, " + destination,
                        Project = currentProj,
                        Wh = null,
                        Group = logGroup,
                        OrderNr = 1
                    });
                }
                else
                {
                    var copySucceeded = ExecutionHelper.DoSomething(() =>
                            FsHelper.DirectoryCopy(prepLocation, destination, true)
                            , "DirectoryCopy", $"From: {prepLocation}\n\t\tTo: {destination}", env, currentProj, null, logGroup, 1);

                    if (!copySucceeded) return;
                }
                //Stop AppPool + update Path + Start pool
                var siteNamesString = "";

                var siteNameConfig = new ConfigPropertyManager().GetSimpleConfigProperties(env, currentProj, "SiteName");
                List<string> siteNames = new List<string>();

                var values = whs.ToString()
                       .Split(new[] { ", " }, StringSplitOptions.None)
                       .Select(v => (Warehouses)Enum.Parse(typeof(Warehouses), v));
                foreach (var w in values)
                {
                    if (w.Equals(Warehouses.None))
                        throw new Exception("'GetSiteNames' for not found 1");
                    else if (w.Equals(Warehouses.BE11))
                        siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.BE11, env, currentProj));

                    else if (w.Equals(Warehouses.BG01))
                        siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.BG01, env, currentProj));

                    else if (w.Equals(Warehouses.FR01))
                        siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.FR01, env, currentProj));

                    else if (w.Equals(Warehouses.PL01))
                        siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PL01, env, currentProj));

                    else if (w.Equals(Warehouses.PL02))
                        siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PL02, env, currentProj));

                    else if (w.Equals(Warehouses.PL03))
                        siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PL03, env, currentProj));

                    else if (w.Equals(Warehouses.PL04))
                        siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PL04, env, currentProj));

                    else if (w.Equals(Warehouses.PT01))
                        siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.PT01, env, currentProj));
                }

                foreach (var siteName in siteNames)
                {
                    siteNamesString += siteName + "; ";
                }

                var iis = IisHelper.GetIisHelper(server);
                ExecutionHelper.DoSomething(
                    () => iis.SetApplicationPoolStateForSite(siteNames, ObjectState.Stopped)
                    , "Stop application pools", siteNamesString, env, currentProj, null, logGroup, 2);

                ExecutionHelper.DoSomething(
                    () => iis.UpdatePhysicalPathSite(siteNames, destinationPathLocal)
                         , "UpdatePhysicalPathSite", siteNamesString + "\n\t\tNewPath: " + destinationPathLocal, env, currentProj, null, logGroup, 3);

                ExecutionHelper.DoSomething(
                    () => iis.SetApplicationPoolStateForSite(siteNames, ObjectState.Started)
                         , "Start application pools", siteNamesString, env, currentProj, null, logGroup, 4);
            });
        }


        public static void WCFGeneral(Environments env, Warehouses whs, ProjectTypes currentProj, string prepLocation)
        {
            var servers = new ConfigPropertyManager().GetServers(env, currentProj, whs);
            if (servers.Count == 0)
                throw new Exception($"Cannot find servers for Env: {env}  Project: {currentProj}");

            var migrationSucceeded = false;
            var migrationCompleted = false;
            Parallel.ForEach(servers, server =>
            {
                //FileCopy
                var temp = "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
                var destinationPath = $@"\\{server}\c$\Usys\{env.ToString()}\WebServices\GeneralServices{temp}";
                var destinationPathLocal = $@"c:\Usys\{env.ToString()}\WebServices\GeneralServices{temp}";

                var siteNameConfig = new ConfigPropertyManager().GetSimpleConfigProperties(env, currentProj, "SiteName");
                List<string> siteNames = new List<string>();
                var values = whs.ToString()
                       .Split(new[] { ", " }, StringSplitOptions.None)
                       .Select(v => (Warehouses)Enum.Parse(typeof(Warehouses), v));

                siteNames.Add(SiteNameHelper.GenerateSiteName(siteNameConfig, Warehouses.None, env, currentProj));

                var siteNamesString = siteNames.Aggregate("", (current, siteName) => current + (siteName + "; "));

                //siteNamesString = siteNames.Aggregate(siteNamesString, (current, siteName) => current + (siteName + ";"));
                var iis = IisHelper.GetIisHelper(server);
                var logGroup = Guid.NewGuid().ToString();


                ExecutionHelper.DoSomething(
                    () => iis.SetApplicationPoolStateForVd(siteNames, ObjectState.Stopped)
                    , "Stop application pools", siteNamesString, env, currentProj, null, logGroup, 0);

                var copySucceeded = ExecutionHelper.DoSomething(() => FsHelper.DirectoryCopy(prepLocation, destinationPath, true)
                   , "DirectoryCopy", $"From: {prepLocation}\n\t\tTo: {destinationPath}", env, currentProj, null, logGroup, 0);
                if (!copySucceeded) return;
                //Run Migrations
                if (server == servers.First())
                {
                    var local = destinationPathLocal; //Bypass Resharper AccessToForEachVariableInClosure
                    migrationSucceeded =
                        ExecutionHelper.DoSomething(
                            () => RunMigration(env, currentProj, Warehouses.None, local, destinationPath, server)
                            , "RunMigration General", "", env, currentProj, null, logGroup, 1);
                    migrationCompleted = true;
                    if (migrationSucceeded)
                    {
                        ExecutionHelper.DoSomething(() => FsHelper.DeleteDirectory(destinationPath.Replace(temp, ""))
                            , "DeletePreviousFolder", $"Dir: {destinationPath.Replace(temp, "")}", env,
                            currentProj, null, logGroup, 2);
                        ExecutionHelper.DoSomething(
                            () => FsHelper.RenameDirectory(destinationPath, destinationPath.Replace(temp, ""))
                            , "RenameDirectory",
                            $"From: {destinationPath}\n\t\tTo:{destinationPath.Replace(temp, "")}",
                            env, currentProj, null, logGroup, 3);
                        destinationPathLocal = destinationPathLocal.Replace(temp, "");
                        ExecutionHelper.DoSomething(() => iis.UpdatePhysicalPathVd(siteNames, destinationPathLocal)
                            , "UpdatePhysicalPathVD",
                            $"Site: {siteNames.FirstOrDefault()}\n\t\tDir: {destinationPathLocal}",
                            env, currentProj, null, logGroup, 4);
                    }
                    else
                    {
                        ExecutionHelper.DoSomething(() => FsHelper.DeleteDirectory(destinationPath)
                            , "DeleteTempFolder", $"Dir: {destinationPath}", env, currentProj, null,
                            logGroup, 5);
                    }
                }
                else
                {
                    while (!migrationCompleted)
                        Thread.Sleep(1000);
                    if (migrationSucceeded)
                    {
                        ExecutionHelper.DoSomething(() => FsHelper.DeleteDirectory(destinationPath.Replace(temp, ""))
                            , "DeletePreviousFolder", $"Dir: {destinationPath.Replace(temp, "")}", env,
                            currentProj, null, logGroup, 2);
                        ExecutionHelper.DoSomething(
                            () => FsHelper.RenameDirectory(destinationPath, destinationPath.Replace(temp, ""))
                            , "RenameDirectory",
                            $"From: {destinationPath}\n\t\tTo:{destinationPath.Replace(temp, "")}",
                            env, currentProj, null, logGroup, 3);
                        destinationPathLocal = destinationPathLocal.Replace(temp, "");
                        ExecutionHelper.DoSomething(() => iis.UpdatePhysicalPathVd(siteNames, destinationPathLocal)
                            , "UpdatePhysicalPathVD",
                            $"Site: {siteNames.FirstOrDefault()}\n\t\tDir: {destinationPathLocal}",
                            env, currentProj, null, logGroup, 4);
                    }
                    else
                    {
                        ExecutionHelper.DoSomething(() => FsHelper.DeleteDirectory(destinationPath)
                            , "DeleteTempFolder", $"Dir: {destinationPath}", env, currentProj, null,
                            logGroup, 5);
                    }
                }

                ExecutionHelper.DoSomething(() => iis.SetApplicationPoolStateForVd(siteNames, ObjectState.Started)
                    , "SetApplicationPoolStateForVD", $"Sites: {siteNamesString}\tTo: {ObjectState.Started.ToString()}", env, currentProj, null, logGroup, 6);
            });
        }

        public static void Services(Environments env, Warehouses whs, string prepLocation, ProjectTypes currentProj)
        {
            var servers = new ConfigPropertyManager().GetServers(env, currentProj, whs);
            if (servers.Count == 0)
                throw new Exception($"Cannot find servers for Env: {env}  Project: {currentProj}");

            foreach (var server in servers)
            {
                foreach (var currentWh in EnumHelper.MaskToList<Warehouses>(whs))
                {
                    if (currentWh == Warehouses.None) continue;
                    if (currentWh != Warehouses.BE11
                        && new[] { ProjectTypes.SFTPConnector, ProjectTypes.InboundApi, ProjectTypes.Handlers, ProjectTypes.JobProcessorCF, ProjectTypes.FileChecker }.Contains(currentProj))
                        continue;
                    var c = new ConfigPropertyManager().GetCredentials(env, currentProj, currentWh);
                    if (c == null)
                        throw new Exception(
                            $"'GetCredentials' for not found Env: {env} Project: {currentProj}, Warehouse: {whs}");

                    var serviceNames = string.Format(c.ServiceName, env, currentWh).Split(';');
                    var serviceExeNames = c.ServiceExeName.Split(';');

                    for (var i = 0; i < serviceNames.Length; i++)
                    {
                        var serviceName = serviceNames[i];
                        var serviceExeName = serviceExeNames[i];
                        var userName = c.UserName;
                        var password = c.Password;
                        var stockOwnerCode = string.IsNullOrWhiteSpace(c.StockOwnerCode) ? string.Empty : c.StockOwnerCode;

                        var prepLocationWh = Path.Combine(prepLocation, currentWh.ToString());

                        if (!string.IsNullOrWhiteSpace(stockOwnerCode))
                            prepLocationWh = prepLocationWh + stockOwnerCode;

                        var logGroup = Guid.NewGuid().ToString();
                        //FileCopy
                        var destination = $@"\\{server}\c$\Usys\{env}\{currentProj}\{currentWh}{stockOwnerCode}";
                        var destinationPathLocal = $@"c:\Usys\{env}\{currentProj}\{currentWh}{stockOwnerCode}";

                        if (i == 0) //do this only once
                        {
                            for (var j = 0; j < serviceNames.Length; j++)
                            {
                                var serviceNameToStop = serviceNames[j];
                                var serviceExeNameToStop = serviceExeNames[j];
                                var serviceAlreadyInstalledToStop = WmiService.Instance.IsServiceInstalled(server, serviceNameToStop);
                                var destinationFullPathLocalToStop = Path.Combine(destinationPathLocal, serviceExeName);

                                if (serviceAlreadyInstalledToStop)
                                    StopService(env, currentProj, server, currentWh, serviceNameToStop, logGroup, destinationFullPathLocalToStop);
                            }
                        }

                        if (whs == Warehouses.BE11 && i == 0) //do this only once
                        {
                            if (currentProj == ProjectTypes.Handlers || currentProj == ProjectTypes.JobProcessorCF)
                            {
                                var pd = Parameters.GetAllParmList(env, currentProj, currentWh);
                                var diParent = new DirectoryInfo(prepLocation).Parent;
                                var dbDirsIn = diParent.GetDirectories("Database.*", SearchOption.AllDirectories);
                                foreach (var dbDirIn in dbDirsIn)
                                {
                                    if (!dbDirIn.Name.Contains("Outbound"))
                                    {
                                        if (currentProj == ProjectTypes.Handlers && dbDirIn.Name.Contains("JobScheduler"))
                                            continue;
                                        if (currentProj == ProjectTypes.JobProcessorCF && !dbDirIn.Name.Contains("JobScheduler"))
                                            continue;
                                    }
                                    var destinationDacPac = $"{destination}\\DacPacs\\{dbDirIn.Name}";
                                    var destinationDacPacLocal = $"{destinationPathLocal}\\DacPacs\\{dbDirIn.Name}";
                                    var copyDacPacSucceeded = ExecutionHelper.DoSomething(() =>
                                        FsHelper.DirectoryCopy(dbDirIn.FullName, destinationDacPac, true)
                                        , "DirectoryCopy", $"From: {dbDirIn.FullName}\n\t\tTo: {destinationDacPac}", env, currentProj, currentWh, logGroup, 2);

                                    if (!copyDacPacSucceeded) return;

                                    var parameterKey = $"MigrationCommand{dbDirIn.Name.Replace("Database.", "")}";

                                    PublishDacPac(env, currentProj, currentWh, destinationDacPacLocal, destinationDacPac, server, string.IsNullOrWhiteSpace(parameterKey) ? null : pd[parameterKey]);
                                }
                            }
                        }

                        var destinationFullPathLocal = Path.Combine(destinationPathLocal, serviceExeName);
                        var serviceAlreadyInstalled = WmiService.Instance.IsServiceInstalled(server, serviceName);

                        if (!serviceAlreadyInstalled)
                            Console.WriteLine("Working for {0} {1} on {2}", currentProj, currentWh, server);

                        if (i == 0)
                        {
                            var copySucceeded = ExecutionHelper.DoSomething(() =>
                                FsHelper.DirectoryCopy(prepLocationWh, destination, true)
                                , "DirectoryCopy", $"From: {prepLocationWh}\n\t\tTo: {destination}", env, currentProj, currentWh, logGroup, 2);

                            if (!copySucceeded) return;
                        }

                        ServiceReturnCode ret = 0;
                        if (!serviceAlreadyInstalled)
                        {
                            if (userName == "") userName = null;
                            if (password == "") password = null;
                            ret = ExecutionHelper.DoSomething(() =>
                                 WmiService.Instance.Install(server, serviceName, serviceName, destinationFullPathLocal, ServiceStartMode.Automatic, userName, password, null)
                                 , "Install service", $"Servicename: {serviceName}", env, currentProj, currentWh, logGroup, 3);
                        }
                        if (i == serviceNames.Length - 1) //only do it the last iteration
                        {
                            if ((serviceAlreadyInstalled && ret == ServiceReturnCode.Success) || !serviceAlreadyInstalled)
                            {
                                for (var j = 0; j < serviceNames.Length; j++)
                                {
                                    var serviceNameToStart = serviceNames[j];
                                    StartService(env, currentProj, server, currentWh, serviceNameToStart, logGroup);
                                }
                            }
                            else
                            {
                                Logger.Log(new LoggingData
                                {
                                    Env = env,
                                    Exception = new Exception($"Install service '{serviceName}' FAILED"),
                                    ExtraMessage = $"service return code = {ret}",
                                    Group = logGroup,
                                    Message = $"service return code = {ret}",
                                    OrderNr = 4,
                                    Project = currentProj,
                                    Wh = currentWh
                                });
                            }
                        }
                    }
                };
            };
        }

        private static void StartService(Environments env, ProjectTypes currentProj, string server, Warehouses currentWh, string serviceName, string logGroup)
        {
            var maxRetry = 2;
            var retryCount = 0;
            var isItStarted = false;
            do
            {
                var sh = new ServiceHelper(serviceName, server);
                isItStarted = ExecutionHelper.DoSomething(() => sh.StartTimeout()
                    , "Start service", $"Servicename: {serviceName}", env, currentProj, currentWh, logGroup, 5);
                if (!isItStarted) retryCount++;
            } while (!isItStarted && retryCount < maxRetry);
            if (!isItStarted)
            {
                Logger.Log(new LoggingData
                {
                    Env = env,
                    Exception = new Exception($"Starting service '{serviceName}' on server {server} FAILED"),
                    ExtraMessage = "",
                    Group = logGroup,
                    Message = $"Starting service '{serviceName}' on server {server} FAILED",
                    OrderNr = 4,
                    Project = currentProj,
                    Wh = currentWh
                });
            }
        }

        private static void StopService(Environments env, ProjectTypes currentProj, string server, Warehouses currentWh, string serviceName, string logGroup, string destinationFullPathLocal)
        {
            try
            {
                var pidToWatch = WmiService.Instance.GetPidForService(server, destinationFullPathLocal);
                Console.WriteLine("Working for {0} {1} pid: {2} on {3}", currentProj, currentWh, pidToWatch.ToString(CultureInfo.InvariantCulture), server);
                var sh = new ServiceHelper(serviceName, server);
                ExecutionHelper.DoSomething(() => { if (sh != null) sh.Stop(); }
                    , "Stop service", $"Name: {serviceName}", env, currentProj, currentWh, logGroup, 0);
                sh.Dispose();

                if (pidToWatch != 0)
                {
                    if (!WaitForProcess(pidToWatch, server, TimeSpan.FromSeconds(55)))
                        ExecutionHelper.DoSomething(() => KillRemoteProcess.KillProcess(env, currentProj, pidToWatch, currentWh, server)
                            , "Kill remote process because it still exists", "", env, currentProj, currentWh, logGroup, 1, true);
                }
                //NEEM BACKUP!!!
            }
            catch (Exception e)
            {
                Logger.Log(new LoggingData
                {
                    Env = env,
                    Exception = e,
                    ExtraMessage = "",
                    Group = logGroup,
                    Message = $"Stopping service '{serviceName}' FAILED",
                    OrderNr = 3,
                    Project = currentProj,
                    Wh = currentWh
                });
            }
        }

        public static void ServicesWHIndependent(Environments env, string prepLocation, ProjectTypes currentProj)
        {
            var servers = new ConfigPropertyManager().GetServers(env, currentProj, Warehouses.None);
            if (servers.Count == 0)
                throw new Exception($"Cannot find servers for Env: {env}  Project: {currentProj}");

            foreach (var server in servers)
            {
                var c = new ConfigPropertyManager().GetCredentials(env, currentProj, Warehouses.None);
                if (c == null)
                    throw new Exception(
                        $"'GetCredentials' for not found Env: {env} Project: {currentProj}, Warehouse: {Warehouses.None}");
                //var c = ShouldBeInDb.GetCredentials(env, currentProj, Warehouses.None);
                var serviceName = string.Empty;
                switch (currentProj)
                {
                    case ProjectTypes.BFJobExecutor:
                    case ProjectTypes.uProcessMonitor:
                        serviceName = string.Format(c.ServiceName, env);
                        break;
                }

                var userName = c.UserName;
                var password = c.Password;
                var serviceExeName = c.ServiceExeName;


                var prepLocationWh = prepLocation;
                var logGroup = Guid.NewGuid().ToString();
                //FileCopy
                var destination = $@"\\{server}\c$\Usys\{env}\{currentProj}";
                var destinationPathLocal = $@"c:\Usys\{env}\{currentProj}";

                var destinationFullPathLocal = Path.Combine(destinationPathLocal, serviceExeName);
                var serviceAlreadyInstalled = WmiService.Instance.IsServiceInstalled(server, serviceName);

                if (!serviceAlreadyInstalled)
                    Console.WriteLine("Working for {0} on {1}", currentProj, server);
                else
                {
                    try
                    {
                        var pidToWatch = WmiService.Instance.GetPidForService(server, destinationFullPathLocal);
                        Console.WriteLine("Working for {0} pid: {1} on {2}", currentProj, pidToWatch.ToString(CultureInfo.InvariantCulture), server);
                        var sh = new ServiceHelper(serviceName, server);
                        ExecutionHelper.DoSomething(() => { if (sh != null) sh.Stop(); }
                            , "Stop service", $"Name: {serviceName}", env, currentProj, null, logGroup, 0);
                        sh.Dispose();

                        if (pidToWatch != 0)
                        {
                            if (!WaitForProcess(pidToWatch, server, TimeSpan.FromSeconds(55)))
                                ExecutionHelper.DoSomething(() => KillRemoteProcess.KillProcess(env, currentProj, pidToWatch, Warehouses.None, server)
                                    , "Kill remote process because it still exists", "", env, currentProj, null, logGroup, 1, true);
                        }
                        //NEEM BACKUP!!!
                    }
                    catch (Exception e)
                    {
                        Logger.Log(new LoggingData
                        {
                            Env = env,
                            Exception = e,
                            ExtraMessage = "",
                            Group = logGroup,
                            Message = $"Stopping service '{serviceName}' FAILED",
                            OrderNr = 3,
                            Project = currentProj
                        });
                    }
                }

                var copySucceeded = ExecutionHelper.DoSomething(() =>
                    FsHelper.DirectoryCopy(prepLocationWh, destination, true)
                    , "DirectoryCopy", $"From: {prepLocationWh}\n\t\tTo: {destination}", env, currentProj, null, logGroup, 2);

                if (!copySucceeded) return;

                ServiceReturnCode ret = 0;
                if (!serviceAlreadyInstalled)
                {
                    ret = ExecutionHelper.DoSomething(() =>
                         WmiService.Instance.Install(server, serviceName, serviceName, destinationFullPathLocal, ServiceStartMode.Automatic, userName, password, null)
                         , "Install service", $"Servicename: {serviceName}", env, currentProj, null, logGroup, 3);
                }
                if ((serviceAlreadyInstalled && ret == ServiceReturnCode.Success) || !serviceAlreadyInstalled)
                {
                    var sh = new ServiceHelper(serviceName, server);
                    var isItStarted = ExecutionHelper.DoSomething(() => sh.StartTimeout()
                        , "Start service", $"Servicename: {serviceName}", env, currentProj, null, logGroup, 5);
                    if (!isItStarted)
                    {
                        Logger.Log(new LoggingData
                        {
                            Env = env,
                            Exception = new Exception($"Starting service '{serviceName}' on server {server} FAILED"),
                            ExtraMessage = "",
                            Group = logGroup,
                            Message = $"Starting service '{serviceName}' on server {server} FAILED",
                            OrderNr = 4,
                            Project = currentProj
                        });
                    }
                }
                else
                {
                    Logger.Log(new LoggingData
                    {
                        Env = env,
                        Exception = new Exception($"Install service '{serviceName}' FAILED"),
                        ExtraMessage = $"service return code = {ret}",
                        Group = logGroup,
                        Message = $"service return code = {ret}",
                        OrderNr = 4,
                        Project = currentProj
                    });
                }





                //};
            }
        }


        public class ServiceCredentials
        {
            public string ServiceName { get; set; }
            public string UserName { get; set; }
            public string Password { get; set; }
            public string ServiceExeName { get; set; }
            public string StockOwnerCode { get; set; }
        }

        public static bool WaitForProcess(int pid, string machine, TimeSpan timeout)
        {
            var start = DateTime.Now;
            try
            {
                while (Process.GetProcessById(pid, machine).Id > 0)
                {
                    if (start.Add(timeout).CompareTo(DateTime.Now) <= 0)
                        return false;

                    Thread.Sleep(2000);
                }
                return true;
            }
            catch
            {
                return true;
            }
        }
    }
}
