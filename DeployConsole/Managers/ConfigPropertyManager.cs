﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeployConsole.Config;
using DeployConsole.Data;
using DeployConsole.Data.Model;

namespace DeployConsole.Managers
{
    /// <summary>
    /// Class for retrieving configuration properties
    /// </summary>
    public class ConfigPropertyManager
    {
        private readonly ConfigPropertyRepository _configPropertyRepository;
        private readonly PropertyVariableRepository _propertyVariableRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigPropertyManager"/> class.
        /// </summary>
        public ConfigPropertyManager()
        {
            var dataContext = new DataContext();
            _configPropertyRepository = new ConfigPropertyRepository(dataContext);
            _propertyVariableRepository = new PropertyVariableRepository(dataContext);
        }
        public List<ConfigProperty> GetSimpleConfigProperties(Environments environments, string key = null)
        {
            return _configPropertyRepository.GetConfigProperties(environments, key);
        }

        public List<ConfigProperty> GetSimpleConfigProperties(Environments environments, IEnumerable<string> keys)
        {
            return _configPropertyRepository.GetConfigProperties(environments, keys);
        }

        public List<ConfigProperty> GetSimpleConfigProperties(Environments environments, ProjectTypes projectTypes, Warehouses wh, string key=null)
        {
            return _configPropertyRepository.GetConfigProperties(environments, projectTypes, wh,key);
        }

        public List<ConfigProperty> GetSimpleConfigProperties(Environments environments, Warehouses wh, string key)
        {
            return _configPropertyRepository.GetConfigProperties(environments,wh, key);
        }

        /// <summary>
        /// Gets the enriched configuration properties.
        /// </summary>
        /// <param name="environments">The environments.</param>
        /// <param name="projectTypes">The project types.</param>
        /// <param name="warehouses">The warehouses.</param>
        /// <returns></returns>
        public List<ConfigProperty_ORG> GetEnrichedConfigProperties_org(Environments environments, ProjectTypes projectTypes, Warehouses warehouses)
        {
            var variables = _propertyVariableRepository.GetPropertyVariables(environments, projectTypes, warehouses);
            var properties = _configPropertyRepository.GetConfigProperties_org(environments, projectTypes, warehouses);

            foreach (var p in properties.Where(p => p.HasVariables.GetValueOrDefault(true)))
            {
                var property = p;

                var builder = new StringBuilder(property.PropertyValue);
                foreach (var variable in variables.Where(v => property.Environment == v.Environment && property.ProjectType == v.ProjectType && property.Warehouse == v.Warehouse))
                {
                    builder.Replace('%' + variable.VariableName + '%', variable.VariableValue);
                }
            }

            return properties;
        }

        public  List<string> GetServers(Environments env, ProjectTypes currentProj, Warehouses whs)
        {
            return _configPropertyRepository.GetServers(env, currentProj,whs);            
        }

        public List<string> GetStockOwners(Environments env, ProjectTypes currentProj, Warehouses whs)
        {
            return _configPropertyRepository.GetStockOwners(env, currentProj, whs);
        }

        public Deploy.ServiceCredentials GetCredentials(Environments env, ProjectTypes currentProj, Warehouses currentWh)
        {
            return _configPropertyRepository.GetCredentials(env, currentProj, currentWh);
        }

        public string GetSimpleConfigProperties(Environments env, ProjectTypes currentProj, string propertyName)
        {
            return _configPropertyRepository.GetSimpleConfigProperties(env, currentProj, propertyName);
        }

        /// priority:
        ///  - 1st: combination of env, proj, warehouse
        ///  - 2nd: combination of env, proj
        ///  - 3th: only env
        /// And then replace "{ENV}" and "{WH}"
        public List<ConfigProperty> GetConfigPropertyByPriorityAndEnrich(List<ConfigProperty> configProperties, Environments environments, ProjectTypes currentProj, Warehouses currentWh)
        {
            var configList = GetConfigPropertyByPriority(configProperties, environments, currentProj, currentWh);

            var resultList = new List<ConfigProperty>();

            foreach (var config in configList)
            {
                resultList.Add(new ConfigProperty
                {
                    PropertyName = config.PropertyName,
                    Warehouse = config.Warehouse,
                    Environment = config.Environment,
                    Id = config.Id,
                    ProjectType = config.ProjectType,
                    PropertyValue = config.PropertyValue.Replace("{ENV}", environments.ToString()).Replace("{WH}", currentWh.ToString())
                });
            }

            return resultList;
        }

        /// <summary>
        /// priority:
        ///  - 1st: combination of env, proj, warehouse
        ///  - 2nd: combination of env, proj
        ///  - 3th: only env
        /// </summary>
        /// <param name="configProperties"></param>
        /// <param name="environments"></param>
        /// <param name="currentProj"></param>
        /// <param name="currentWh"></param>
        /// <returns></returns>
        private List<ConfigProperty> GetConfigPropertyByPriority(List<ConfigProperty> configProperties, Environments environments, ProjectTypes currentProj, Warehouses currentWh)
        {
            var resultList = new List<ConfigProperty>();

            var keys = configProperties.Select(c => c.PropertyName).Distinct();
            foreach (var key in keys)
            {
                resultList.AddRange(configProperties
                    .Where(c => environments.HasFlag(c.Environment))
                    .Where(c => c.ProjectType.HasValue && currentProj.HasFlag(c.ProjectType.Value))
                    .Where(c => c.Warehouse.HasValue && currentWh.HasFlag(c.Warehouse.Value))
                    .Where(c => c.PropertyName == key)
                    .ToList());

                if (!resultList.Any(c => c.PropertyName == key))
                {
                    resultList.AddRange(configProperties
                        .Where(c => environments.HasFlag(c.Environment))
                        .Where(c => c.ProjectType.HasValue && currentProj.HasFlag(c.ProjectType.Value))
                        .Where(c => !c.Warehouse.HasValue)
                        .Where(c => c.PropertyName == key)
                        .ToList());
                }

                if (!resultList.Any(c => c.PropertyName == key))
                {
                    resultList.AddRange(configProperties
                        .Where(c => environments.HasFlag(c.Environment))
                        .Where(c => !c.ProjectType.HasValue)
                        .Where(c => !c.Warehouse.HasValue)
                        .Where(c => c.PropertyName == key)
                        .ToList());
                }
            }

            return resultList;
        }
    }
}
